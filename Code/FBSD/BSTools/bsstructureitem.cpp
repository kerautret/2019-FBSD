#include <QtGui>
#include <iostream>
#include "bsstructureitem.h"

using namespace std;


const int BSStructureItem::DISPLAY_FINAL_BLURRED_SEGMENT = 1;
const int BSStructureItem::DISPLAY_FINAL_CONNECTED_COMPONENTS = 2;
const int BSStructureItem::DISPLAY_FINAL_SCANS_AND_FILTER = 3;
const int BSStructureItem::DISPLAY_INITIAL_BLURRED_SEGMENT = 4;
const int BSStructureItem::DISPLAY_INITIAL_CONNECTED_COMPONENTS = 5;
const int BSStructureItem::DISPLAY_INITIAL_SCANS_AND_FILTER = 6;
const int BSStructureItem::DISPLAY_PRELIM_BLURRED_SEGMENT = 7;
const int BSStructureItem::DISPLAY_PRELIM_CONNECTED_COMPONENTS = 8;
const int BSStructureItem::DISPLAY_PRELIM_SCANS_AND_FILTER = 9;
const int BSStructureItem::DISPLAY_MIN = DISPLAY_FINAL_BLURRED_SEGMENT;
const int BSStructureItem::DISPLAY_MAX = DISPLAY_PRELIM_SCANS_AND_FILTER;

const int BSStructureItem::DEFAULT_PEN_WIDTH = 1;
const int BSStructureItem::LEFT_MARGIN = 16;
const int BSStructureItem::TEXT_HEIGHT = 16;

const int BSStructureItem::MAX_ZOOM = 16;



BSStructureItem::BSStructureItem (int width, int height, const QImage *im,
                                  BSDetector *detector)
{
  w = width;
  h = height;
  zoom = 1;
  focx = 0;
  focy = 0;
  det = detector;
  displayItem = DISPLAY_MIN;
  displayScanLines = false;
  displayInput = false;
  this->im = im;
  verbose = true;
  infoPen = QPen (Qt::red, DEFAULT_PEN_WIDTH, Qt::SolidLine,
                  Qt::RoundCap, Qt::RoundJoin);
}


BSStructureItem::~BSStructureItem ()
{
}


void BSStructureItem::zoomIn ()
{
  if (zoom < MAX_ZOOM) zoom *= 2;
}


void BSStructureItem::zoomOut ()
{
  if (zoom > 1)
  {
    int w2 = w / 2;
    int h2 = h / 2;
    zoom /= 2;
    if (focx < w2 / zoom - w2) focx = w2 / zoom - w2;
    if (focx > w2 - w2 / zoom) focx = w2 - w2 / zoom;
    if (focy < h2 / zoom - h2) focy = h2 / zoom - h2;
    if (focy > h2 - h2 / zoom) focy = h2 - h2 / zoom;
  }
}


void BSStructureItem::shift (int dx, int dy)
{
  int w2 = w / 2;
  int h2 = h / 2;
  focx += zoom * dx;
  focy += zoom * dy;
  if (focx < w2 / zoom - w2) focx = w2 / zoom - w2;
  if (focx > w2 - w2 / zoom) focx = w2 - w2 / zoom;
  if (focy < h2 / zoom - h2) focy = h2 / zoom - h2;
  if (focy > h2 - h2 / zoom) focy = h2 - h2 / zoom;
}


QRectF BSStructureItem::boundingRect () const
{
  return QRectF (0, 0, w - 1, h - 1); // ZZZ
}


void BSStructureItem::paint (QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
  Q_UNUSED (option);
  Q_UNUSED (widget);

  if (displayItem == DISPLAY_FINAL_BLURRED_SEGMENT)
    paintBlurredSegment (painter, BSDetector::STEP_FINAL);
  else if (displayItem == DISPLAY_FINAL_CONNECTED_COMPONENTS)
    paintConnectedComponents (painter, BSDetector::STEP_FINAL);
  else if (displayItem == DISPLAY_FINAL_SCANS_AND_FILTER)
    paintScansAndFilter (painter, BSDetector::STEP_FINAL);
  else if (displayItem == DISPLAY_INITIAL_BLURRED_SEGMENT)
    paintBlurredSegment (painter, BSDetector::STEP_INITIAL);
  else if (displayItem == DISPLAY_INITIAL_CONNECTED_COMPONENTS)
    paintConnectedComponents (painter, BSDetector::STEP_INITIAL);
  else if (displayItem == DISPLAY_INITIAL_SCANS_AND_FILTER)
    paintScansAndFilter (painter, BSDetector::STEP_INITIAL);
  else if (displayItem == DISPLAY_PRELIM_BLURRED_SEGMENT)
    paintBlurredSegment (painter, BSDetector::STEP_PRELIM);
  else if (displayItem == DISPLAY_PRELIM_CONNECTED_COMPONENTS)
    paintConnectedComponents (painter, BSDetector::STEP_PRELIM);
  else if (displayItem == DISPLAY_PRELIM_SCANS_AND_FILTER)
    paintScansAndFilter (painter, BSDetector::STEP_PRELIM);
}


void BSStructureItem::paintBlurredSegment (QPainter *painter, int step)
{
  BlurredSegment *bs = det->getBlurredSegment (step);
  if (bs != NULL)
  {
    DigitalStraightSegment *dss = bs->getSegment ();
    if (dss != NULL)
    {
      vector<Pt2i> bnd;
      dss->getBounds (bnd, 0, 0, w, h);
      paintPixels (painter, bnd, Qt::green);
    }
    paintPixels (painter, bs->getAllPoints (), Qt::blue);

    if (verbose)
    {
      initText (painter);
      if (dss != NULL)
        addText (painter, QString ("Segment thickness = ")
                 + QString::number (dss->width ()) + QString ("/")
                 + QString::number (dss->period ()) + QString (" = ")
                 + QString::number (dss->width () / (double) dss->period ()));
      else addText (painter, QString ("Segment thickness = 0"));
      addText (painter, QString ("W : assigned thickness = ")
               + QString::number (det->assignedThickness ()));
      addText (painter, QString ("L : output BS min size = ")
               + QString::number (det->finalSizeMinValue ()));
      addText (painter, QString ("H : pixel lack tolerence = ")
               + QString::number (det->getPixelLackTolerence ()));
      if (step == 0)
      {
        addText (painter, QString ("D : extension limit = ")
                 + QString::number (det->initialDetectionMaxExtent ()));
        addText (painter, QString ("P : Prefiltering ")
                 + (det->isFiltering (step) ?
                    (QString ("on : ")
                     + QString::number (det->prefilteringInputSize ())
                     + QString (" -> ")
                     + QString::number (det->prefilteringOutputSize ())
                     + QString (" pixels")) :
                    QString ("off")));
      }
    }
  }
}


void BSStructureItem::paintConnectedComponents (QPainter *painter, int step)
{
  BlurredSegment *bs = det->getBlurredSegment (step);
  if (bs != NULL)
  {
    QColor cols[] = {Qt::blue, Qt::red, Qt::green};
    int col = 0;
    vector < vector <Pt2i> > cc = bs->getConnectedComponents ();
    vector < vector <Pt2i> >::const_iterator it = cc.begin ();
    while (it != cc.end ())
    {
      paintPixels (painter, *it, cols[col]);
      if (++col == 3) col = 0;
      it++;
    }
  }

  if (verbose)
  {
    initText (painter);
    if (bs != NULL)
    {
      int ccs = det->fragmentSizeMinValue ();
      int bsccp = bs->countOfConnectedPoints (ccs);
      int bssize = bs->getAllPoints().size ();
      bool ccon = (step == BSDetector::STEP_FINAL
                   && det->isFinalFragmentationTestOn ());
      addText (painter, QString ("Fragmentation test ") + (ccon ?
                           QString ("on") : QString ("off")));
      addText (painter, QString::number (bssize) + QString (" points"));
      addText (painter, QString::number (bs->countOfConnectedComponents ())
                        + QString (" fragments"));
      addText (painter, QString::number (bs->countOfConnectedPoints ())
                        + QString (" points in fragments"));
      addText (painter, QString::number (bs->countOfConnectedComponents (ccs))
                        + QString (" fragments with min size ")
                        + QString::number (ccs));
      addText (painter, QString::number (bsccp)
                        + QString (" points in fragments with min size ")
                        + QString::number (ccs));
      if (ccon && bsccp < bssize / 2)
        addText (painter, QString ("BS too fragmented !"));
    }
  }
}


void BSStructureItem::paintScansAndFilter (QPainter *painter, int step)
{
  vector <vector <Pt2i> > scanLines;
  if (step == BSDetector::STEP_FINAL) scanLines = det->getFinalScans ();
  else
  {
    if (step == BSDetector::STEP_PRELIM && ! det->isPreliminary ()) return;
    Pt2i pt1, pt2, ptc (-1, -1);
    int swidth = 0;
    det->getScanInput (step, pt1, pt2, swidth, ptc);
    ScannerProvider sp;
    sp.setSize (w, h);
    DirectionalScanner *ds = NULL;
    if (swidth != 0)
      ds = sp.getScanner (ptc, pt1.vectorTo (pt2), swidth, false);
    else ds = sp.getScanner (pt1, pt2);
    vector<Pt2i> pix;
    ds->first (pix);
    scanLines.push_back (pix);
    bool on = true;
    while (on)
    {
      if (ds->nextOnRight (pix) < 1) on = false;
      else scanLines.push_back (pix);
    }
    on = true;
    while (on)
    {
      if (ds->nextOnLeft (pix) < 1) on = false;
      else scanLines.push_back (pix);
    }
  }
  if (displayScanLines)
  {
    QColor col[] = {Qt::blue, Qt::red, Qt::green};
    int i = 0;
    if (! scanLines.empty ())
    {
      vector <vector <Pt2i> >::const_iterator it = scanLines.begin ();
      paintPixel (painter, it->front (), col[2]);
      paintPixel (painter, it->back (), col[2]);
      it++;  // Displays only the bounds of the central scan
      while (it != scanLines.end ())
      {
        paintPixels (painter, *it, col[i]);
        it++;
        if (++i == 3) i = 0;
      }
    }
  }
  else
  {
    // Displays scan bounds
    if (! scanLines.empty ())
    {
      vector <vector <Pt2i> >::const_iterator it = scanLines.begin ();
      while (it != scanLines.end ())
      {
        paintPixel (painter, it->front (), Qt::blue);
        paintPixel (painter, it->back (), Qt::blue);
        it ++;
      }
    }

    // Display the input selection
    if (displayInput)
    {
      vector<Pt2i> stroke;
      Pt2i ip1, ip2, ipc;
      int iw;
      det->getScanInput (BSDetector::STEP_INITIAL, ip1, ip2, iw, ipc);
      ip1.draw (stroke, ip2);
      paintPixels (painter, stroke, Qt::red);
    }

    // Displays filter output
    if (det->isFiltering (step))
    {
      paintPixels (painter, det->getAccepted (step), Qt::green);
      paintPixels (painter, det->getRejected (step), Qt::red);
      BlurredSegment *bs = det->getBlurredSegment (step);
      if (bs != NULL) paintPixels (painter, bs->getStartPt (), Qt::yellow);
    }

    // Displays the blurred segment
    else
    {
      BlurredSegment *bs = det->getBlurredSegment (step);
      if (bs != NULL) paintPixels (painter, bs->getAllPoints (), Qt::green);
    }
  }

  if (verbose)
  {
    initText (painter);
    addText (painter,
             QString ("S : ") + (det->dynamicScansOn () ?
               QString ("dynamic scans") : QString ("static scans")));
    addText (painter,
             QString ("O : ") + (det->orthoScansOn () ?
               QString ("vert/horiz scans") : QString ("directional scans")));
  }
}


void BSStructureItem::toggleDisplay (bool next)
{
  displayItem += (next ? 1 : -1);
  if (displayItem > DISPLAY_MAX) displayItem = DISPLAY_MIN;
  else if (displayItem < DISPLAY_MIN) displayItem = DISPLAY_MAX;
  det->setFinalScansRecord (displayItem == DISPLAY_FINAL_SCANS_AND_FILTER);
}


void BSStructureItem::paintPixels (QPainter *painter,
                                   const vector<Pt2i> &pix, const QColor col)
{
  QBrush brush (col);
  vector<Pt2i>::const_iterator iter = pix.begin ();
  while (iter != pix.end ())
  {
    Pt2i p = *iter;
    int dx = w / 2 + focx - p.x ();
    int dy = h / 2 - focy - p.y ();
    painter->fillRect (w / 2 - zoom * dx,
                      (h / 2 + zoom * dy),
                       zoom, zoom, brush);
    iter++;
  }
}


void BSStructureItem::paintPixels (QPainter *painter,
                                   const vector<Pt2i> &pix)
{
  vector<Pt2i>::const_iterator iter = pix.begin ();
  while (iter != pix.end ())
  {
    Pt2i p = *iter;
    int dx = w / 2 + focx - p.x ();
    int dy = h / 2 - focy - p.y ();
    QBrush brush (im->pixel (p.x (), im->height () - 1 - p.y ()));
    painter->fillRect (w / 2 - zoom * dx,
                      (h / 2 + zoom * dy),
                       zoom, zoom, brush);
    iter++;
  }
}


void BSStructureItem::paintPixel (QPainter *painter,
                                  const Pt2i &pix, const QColor col)
{
  QBrush brush (col);
  int dx = w / 2 + focx - pix.x ();
  int dy = h / 2 - focy - pix.y ();
  painter->fillRect (w / 2 - zoom * dx, (h / 2 + zoom * dy), // dec 1
                     zoom, zoom, brush);
}


void BSStructureItem::initText (QPainter *painter)
{
  painter->setPen (infoPen);
  textOffset = 0;
}


void BSStructureItem::addText (QPainter *painter, const QString &text)
{
  textOffset += TEXT_HEIGHT;
  painter->drawText (LEFT_MARGIN, textOffset, text);
}
