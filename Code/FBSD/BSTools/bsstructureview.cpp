#include <QtGui>
#include <iostream>
#include "bsstructureview.h"
#include "math.h"

using namespace std;


const int BSStructureView::BACK_BLACK = 0;
const int BSStructureView::BACK_WHITE = 1;
const int BSStructureView::BACK_IMAGE = 2;
const int BSStructureView::BACK_GRAD = 3;


BSStructureView::BSStructureView (QImage *im, BSDetector *sd)
{
  int w = im->width ();
  int h = im->height ();
  background = BACK_IMAGE;
  graylevelImage = im;
  currentImage = im;
  gradImage = NULL;
  blevel = 0;
  det = sd;
  setBackgroundBrush (QBrush (*currentImage));
  setScene (new QGraphicsScene (0, 0, w, h));
  grid = new BSStructureItem (w, h, im, sd);
  scene()->addItem (grid);
  setWindowTitle (grid->itemTitle ());
}


BSStructureView::~BSStructureView ()
{
  scene()->removeItem (grid);
  delete grid;
}


void BSStructureView::setGradientImage (QImage *gim)
{
  gradImage = gim;
}


void BSStructureView::updateBackground ()
{
  if (background == BACK_BLACK)
    setBackgroundBrush (QBrush (Qt::black));
  else if (background == BACK_WHITE)
    setBackgroundBrush (QBrush (Qt::white));
  else
  {
    int w = currentImage->width ();
    int h = currentImage->height ();
    int zf = grid->zoomFactor ();
    image = currentImage->scaled (w * zf, h * zf);

    if (blevel != 0)
    {
      for (int i = 0; i < image.height (); i++)
        for(int j = 0; j < image.width (); j++)
        {
          int col = blevel + (QColor (image.pixel(j,i)).value ()
                              * (255 - blevel)) / 255;
          image.setPixel (j, i, col + col * 256 + col * 256 * 256);
        }
    }

    QBrush qb (image);
    qb.setTransform (QTransform::fromTranslate (
          w / 2 - zf * (w / 2 + grid->focusX ()),
          h / 2 - zf * (h / 2 + grid->focusY ())));
    setBackgroundBrush (qb);
  }
}


void BSStructureView::toggleBackground ()
{
  if (background++ == BACK_GRAD) background = BACK_BLACK;
  if (background == BACK_GRAD) currentImage = gradImage;
  else currentImage = graylevelImage;
}


void BSStructureView::incBlackLevel (int val)
{
  blevel += val * 5;
  if (blevel < 0) blevel = 0;
  if (blevel > 200) blevel = 200;
}


void BSStructureView::paint (QPainter *painter,
                             const QStyleOptionGraphicsItem *option,
                             QWidget *widget)
{
  Q_UNUSED (painter);
  Q_UNUSED (option);
  Q_UNUSED (widget);
}


bool BSStructureView::processKeyEvent (QKeyEvent *event)
{
  bool processed = false;
  switch (event->key ())
  {
    case Qt::Key_I : // Info
      if (event->modifiers () & Qt::ControlModifier)
        grid->switchScanDisplay ();
      else
        grid->toggleDisplay ((event->modifiers () & Qt::ShiftModifier) == 0);
      setWindowTitle (grid->itemTitle ());
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_J : // Input selection display
      if (event->modifiers () & Qt::ControlModifier)
        grid->switchInputDisplay ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_V : // Info display
      grid->switchInfoDisplay ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_B : // Background
      if (event->modifiers () & Qt::ControlModifier)
      {
        toggleBackground ();
        updateBackground ();
        scene()->update ();
        update ();
        processed = false;
      }
      break;

    case Qt::Key_P : // Capture
      // viewport()->grab (
      //   QRect (QPoint (0, 0),
      //          QSize (grid->getWidth(), grid->getHeight()))
      //   ).toImage().save ("structure.png");
      // cout << "Structure shot in structure.png" << endl;
      break;

    case Qt::Key_Y : // Background lightening
      incBlackLevel ((event->modifiers () & Qt::ShiftModifier) ? -1 : 1);
      updateBackground ();
      scene()->update ();
      update ();
      cout << "Background black level = " << getBlackLevel () << endl;
      processed = false;
      break;

    case Qt::Key_Plus : // Zoom in
      grid->zoomIn ();
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_Minus : // Zoom out
      grid->zoomOut ();
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_Left : // Go leftwards
      grid->shift (-1, 0);
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_Right : // Go rightwards
      grid->shift (1, 0);
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_Up : // Go upwards
      grid->shift (0, -1);
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;

    case Qt::Key_Down : // Go downwards
      grid->shift (0, 1);
      updateBackground ();
      scene()->update ();
      update ();
      processed = false;
      break;
  }
  return processed;
}
