#ifndef BS_GROUNDTRUTH_VIEW_H
#define BS_GROUNDTRUTH_VIEW_H

#include <QGraphicsView>
#include <QImage>
#include "bsgroundtruthitem.h"


/** 
 * @class BSGroundtruthView bsgroundtruthview.h
 * \brief A Qt window to compare detected segments with groundtruth lines.
 * \author {P. Even}
 */
class BSGroundtruthView : public QGraphicsView
{

public:

  /**
   * \brief Creates a groundtruth comparator view.
   */
  BSGroundtruthView (QImage *im, BSDetector *sd);

  /**
   * \brief Deletes the groundtruth comparator view.
   */
  ~BSGroundtruthView ();

  /**
   * \brief Redraws the groundtruth comparator view.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Processes key pressed events.
   */
  bool processKeyEvent (QKeyEvent *event);


protected:

private:

  /** Pointer to the blurred segment structure graphics item. */
  BSGroundtruthItem *groundtruth;
  /** Pointer to the displayed image. */
  QImage *image;

};

#endif
