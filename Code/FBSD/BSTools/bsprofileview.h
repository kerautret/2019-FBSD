#ifndef BS_PROFILE_VIEW_H
#define BS_PROFILE_VIEW_H

#include <QGraphicsView>
#include "bsprofileitem.h"


/** 
 * @class BSProfileView bsprofileview.h
 * \brief A Qt window containing informations about scanned profiles.
 * \author {P. Even}
 */
class BSProfileView : public QGraphicsView
{

public:

  /**
   * \brief Creates a profile analysis window.
   */
  BSProfileView ();

  /**
   * \brief Deletes the profile analysis window.
   */
  ~BSProfileView ();

  /**
   * \brief Updates the profile analysis window display.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Declares the image to be analysed.
   */
  void setImage (QImage *image, VMap *idata);

  /**
   * \brief Sets the image scan area from an initial scan.
   * The initial scan is a straight segment from p1 to p2.
   */
  void buildScans (Pt2i p1, Pt2i p2);

  /**
   * \brief Processes key pressed events.
   */
  bool processKeyEvent (QKeyEvent *event);


private:

  /** Profile analysis widget. */
  BSProfileItem *prof;

};

#endif
