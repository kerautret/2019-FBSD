#ifndef BS_IDET_VIEW_H
#define BS_IDET_VIEW_H

#include <QGraphicsView>
#include "bsidetitem.h"


/** 
 * @class BSIdetView bsidetview.h
 * \brief A Qt window containing informations about initial detection.
 * \author {P. Even}
 */
class BSIdetView : public QGraphicsView
{

public:

  /**
   * \brief Creates a initial detection analysis window.
   */
  BSIdetView (BSDetector *detector);

  /**
   * \brief Deletes the initial detection analysis window.
   */
  ~BSIdetView ();

  /**
   * \brief Updates the initial detection analysis window display.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Declares the image to be analysed.
   */
  void setImage (QImage *image, VMap *idata);

  /**
   * \brief Sets the image scan area from an initial scan.
   * The initial scan is a straight segment from p1 to p2.
   */
  void buildScans (Pt2i p1, Pt2i p2);

  /**
   * \brief Processes key pressed events.
   */
  bool processKeyEvent (QKeyEvent *event);

  /**
   * \brief Update the initial detection widget.
   */
  void update ();


private:

  /** Initial detection analysis widget. */
  BSIdetItem *idet;

};

#endif
