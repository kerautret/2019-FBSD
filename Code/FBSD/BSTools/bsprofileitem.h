#ifndef BS_PROFILE_ITEM_H
#define BS_PROFILE_ITEM_H

#include <QGraphicsItem>
#include <QImage>
#include <QKeyEvent>
#include "vmap.h"
#include "scannerprovider.h"


/** 
 * @class BSProfileItem bsprofileitem.h
 * \brief Scan intensity profile view and controller.
 * \author {P. Even}
 */
class BSProfileItem : public QGraphicsItem
{

public:


  /**
   * \brief Creates a profile analysis widget.
   */
  BSProfileItem ();

  /**
   * \brief Declares the image to be analysed.
   */
  void setImage (QImage *image, VMap *idata);

  /**
   * \brief Sets the image scan area from an initial scan.
   * The initial scan is a straight segment from p1 to p2.
   */
  void buildScans (Pt2i p1, Pt2i p2);

  /**
   * \brief Returns the widget size.
   * Nominally the image size.
   */
  QRectF boundingRect () const;

  /**
   * \brief Updates the widget display.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Returns the displayed information title.
   */
  inline QString itemTitle () const
  {
    if (displayItem == DISPLAY_INTENSITY)
      return ("Intensity profiles");
    else if (displayItem == DISPLAY_GRADIENT)
      return ("Gradient profiles");
    else if (displayItem == DISPLAY_CORRELATION_MEAN_1D)
      return ("Correlation profiles (mean 1D)");
    else if (displayItem == DISPLAY_CORRELATION_FULL_2D)
      return ("Correlation profiles (full 2D)");
    else if (displayItem == DISPLAY_CORRELATION_MEAN_2D)
      return ("Correlation profiles (mean 2D)");
    else return ("No profile");
  }

  /**
   * \brief Returns the widget display width.
   */
  inline int getWidth () const { return widWidth; }

  /**
   * \brief Returns the widget display height.
   */
  inline int getHeight () const { return widHeight; }

  /**
   * \brief Increments the current stripe index.
   * @param inc Direction (1 for leftwards, -1 for rightwards)
   */
  void incStripe (int inc);

  /**
   * \brief Increments or decrements the scan correlation width.
   * @param down Direction (1 to increment, -1 to decrement)
   */
  void incCorrelWidth (int down);

  /**
   * \brief Increments or decrements the scan correlation thickness.
   * @param down Direction (1 to increment, -1 to decrement)
   */
  void incCorrelThick (int down);

  /**
   * \brief Increments or decrements the scan correlation ratio.
   * @param down Direction (1 to increment, -1 to decrement)
   */
  void incCorrelRatio (int down);

  /**
   * \brief Toggles the displayed information.
   * @param next Get next information if true, previous on otherwise.
   */
  void toggleDisplay (bool next);


protected:

private:

  /** Available information : intensity profiles. */
  static const int DISPLAY_INTENSITY;
  /** Available information : gradient profiles. */
  static const int DISPLAY_GRADIENT;
  /** Available information : 1D mean correlation profiles. */
  static const int DISPLAY_CORRELATION_MEAN_1D;
  /** Available information : 2D full correlation profiles. */
  static const int DISPLAY_CORRELATION_FULL_2D;
  /** Available information : 1D full correlation profiles. */
  static const int DISPLAY_CORRELATION_MEAN_2D;
  /** Number of the first information. */
  static const int DISPLAY_MIN;
  /** Number of the last information. */
  static const int DISPLAY_MAX;

  /** Analysis widget height. */
  int widHeight;
  /** Analysis widget width. */
  int widWidth;

  /** Profile area width. */
  int profileWidth;
  /** Intensity profile vertical zoom factor. */
  int profileRatio;
  /** Intensity profile lowest visible value. */
  int profileLow;

  /** Gradient profile vertical unzoom factor. */
  int gradientUnRatio;
  /** Gradient profile lowest visible value. */
  int gradientLow;

  /** Correlation profile vertical zoom factor (power of 2). */
  int correlRatio;
  /** Correlation profile width. */
  int correlWidth;
  /** Correlation profile scan thickness. */
  int correlThick;

  /** Stripe area width. */
  int stripeWidth;
  /** Stripe area margin width. */
  int stripeMargin;
  /** Stripe points zoom factor. */
  int stripeResol;

  /** Analysed image. */
  QImage *image;
  /* Analysed image width. */
  int imageWidth;
  /** Analyzed image height. */
  int imageHeight;
  /** Gradient map. */
  VMap *gMap;

  /** Central scan start point. */
  Pt2i pt1;
  /** Central scan start point. */
  Pt2i pt2;
  /** Central and left scans. */
  vector <vector <Pt2i> > leftscan;
  /** Right scans. */
  vector <vector <Pt2i> > rightscan;
  /** Minimum scan length allowed. */
  static const int MIN_SCAN;

  // Just for local minima extraction.
  vector <vector <int> > leftCorrel;
  vector <vector <int> > rightCorrel;
  vector <vector <int> > leftReCorrel;
  vector <vector <int> > rightReCorrel;

  /** Displayed information (intensity, gradient, correlation). */
  int displayItem;
  /** Current stripe index. */
  int stripe;
  /** Min stripe index (right scan size). */
  int minStripe;
  /** Max stripe index (left scan size). */
  int maxStripe;

  /** Scanner provider (that selects the appropriate octant) */
  ScannerProvider scanp;


  /**
   * \brief Correlation calculation (first release).
   * \author {B. Kerautret}
   * Compares portions of scan bars with a profile extracted at
   * the first scan centered on the blurred segment.
   */
  void setCorrelationStripes (Pt2i p1, Pt2i p2, int segwidth);

  /**
   * \brief Correlation calculation (second release).
   * \author {B. Kerautret}
   * Compares portions of scan bars with a mean profile extracted at
   * the center of the first Nth scans (N = correlThick).
   */
  bool setMeanCorrelationStripes ();

  /**
   * \brief Correlation calculation (third release).
   * \author {B. Kerautret}
   * Compares 2D portions of scan bars (N = correlThick) with a 2D profile
   * extracted at the center of the first Nth scans (N = correlThick).
   */
  bool setFull2dCorrelationStripes ();

  /**
   * \brief Correlation calculation (fourth release).
   * \author {B. Kerautret}
   * Compares mean portions of scan bars (N = correlThick) with a mean
   * profile extracted at the center of the first Nth scans (N = correlThick).
   */
  bool setMean2dCorrelationStripes ();

  /**
   * \brief Extract local minima from given signal.
   * @param indices List of indices of the signal local minima.
   * @param signal Input signal.
   */
  void getLocalMinimaIndices (vector<int> &indices,
                              const vector<int> &signal) const;

  /**
   * \brief Draws the scan strip.
   */
  void paintStripes (QPainter *painter);

  /**
   * \brief Draws the intensity profile.
   */
  void paintIntensityProfile (QPainter *painter);

  /**
   * \brief Draws the gradient profile.
   */
  void paintGradientProfile (QPainter *painter);

  /**
   * \brief Draws the correlation profile.
   */
  void paintCorrelationProfile (QPainter *painter);
};

#endif
