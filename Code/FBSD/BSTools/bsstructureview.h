#ifndef BS_STRUCTURE_VIEW_H
#define BS_STRUCTURE_VIEW_H

#include <QGraphicsView>
#include <QImage>
#include "bsstructureitem.h"


/** 
 * @class BSStructureView bsstructureview.h
 * \brief A Qt window containing informations about detection stages.
 * \author {P. Even}
 */
class BSStructureView : public QGraphicsView
{

public:

  /**
   * \brief Creates a pixel analyzer.
   */
  BSStructureView (QImage *im, BSDetector *sd);

  /**
   * \brief Deletes the pixel analyzer.
   */
  ~BSStructureView ();

  /**
   * Sets the reference to the gradient image.
   * @param gim Reference to the used gradient image.
   */
  void setGradientImage (QImage *gim);

  /**
   * \brief Redraws the pixel analyzer.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Processes key pressed events.
   */
  bool processKeyEvent (QKeyEvent *event);


protected:

private:

  /** Background status : uniform black. */
  static const int BACK_BLACK;
  /** Background status : uniform white. */
  static const int BACK_WHITE;
  /** Background status : intensity image displayed. */
  static const int BACK_IMAGE;
  /** Background status : gradient image displayed. */
  static const int BACK_GRAD;

  /** Pointer to the blurred segment detector. */
  BSDetector *det;
  /** Pointer to the blurred segment structure graphics item. */
  BSStructureItem *grid;
  /** Background type : BACK_BLACK, BACK_WHITE, BACK_IMAGE or BACK_GRAD. */
  int background;
  /** Pointer to the background image. */
  QImage *currentImage;
  /** Pointer to the raw image. */
  QImage *graylevelImage;
  /** Pointer to the gradient image. */
  QImage *gradImage;
  /** Effective background image. */
  QImage image;
  /** Black level used to lighten background images. */
  int blevel;

  /**
   * Updates the background image.
   */
  void updateBackground ();

  /**
   * Toggles the window background between the current image or plain white.
   */
  void toggleBackground ();

  /**
   * \brief Returns the background black level.
   */
  inline int getBlackLevel () const { return (blevel); }

  /**
   * Increments the background intensity.
   * @param val Increment value.
   */
  void incBlackLevel (int val);

};

#endif
