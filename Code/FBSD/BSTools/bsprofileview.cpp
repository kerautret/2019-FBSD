#include <QtGui>
#include <iostream>
#include <cstdlib>
#include "bsprofileview.h"

using namespace std;



BSProfileView::BSProfileView ()
{
  // CAUTION : don't activate antialiasing here !!!
  setBackgroundBrush (QBrush (Qt::white));
  setScene (new QGraphicsScene (0, 0, 610, 610));
  prof = new BSProfileItem ();
  scene()->addItem (prof);
  setWindowTitle (prof->itemTitle ());
  resize (QSize (616, 616));
}


BSProfileView::~BSProfileView ()
{
  scene()->removeItem (prof);
  delete prof;
}


void BSProfileView::paint (QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
  Q_UNUSED (option);
  Q_UNUSED (widget);
  Q_UNUSED (painter);
}


void BSProfileView::setImage (QImage *image, VMap *idata)
{
  prof->setImage (image, idata);
}


void BSProfileView::buildScans (Pt2i p1, Pt2i p2)
{
  prof->buildScans (p1, p2);
}


bool BSProfileView::processKeyEvent (QKeyEvent *event)
{
  switch (event->key ())
  {
    case Qt::Key_W : // Width of correlation measure
      prof->incCorrelWidth (event->modifiers () & Qt::ShiftModifier);
      prof->update ();
      break;

    case Qt::Key_T : // Thickness of correlation measure
      prof->incCorrelThick (event->modifiers () & Qt::ShiftModifier);
      prof->update ();
      break;

    case Qt::Key_R : // Ratio of correlation measure
      prof->incCorrelRatio (event->modifiers () & Qt::ShiftModifier);
      prof->update ();
      break;

    case Qt::Key_I :
      prof->toggleDisplay ((event->modifiers () & Qt::ShiftModifier) == 0);
      setWindowTitle (prof->itemTitle ());
      prof->update ();
      break;

    case Qt::Key_P : // Capture
      // viewport()->grab (
      //   QRect (QPoint (0, 0),
      //          QSize (prof->getWidth(), prof->getHeight()))
      //   ).toImage().save ("profiles.png");
      // cout << "Profiles shot in capture.png" << endl;
      break;

    case Qt::Key_Up :
      prof->incStripe (1);
      prof->update ();
      break;

    case Qt::Key_Down :
      prof->incStripe (-1);
      prof->update ();
      break;
  }
  return false;
}
