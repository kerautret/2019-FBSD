#ifndef BS_STRUCTURE_ITEM_H
#define BS_STRUCTURE_ITEM_H

#include <QGraphicsItem>
#include <vector>
#include "bsdetector.h"


/** 
 * @class BSStructureItem bsstructureitem.h
 * \brief Detection stage analysis view and controller.
 * \author {P. Even}
 */
class BSStructureItem : public QGraphicsItem
{
public:

  /**
   * \brief Creates a pixel analysis grid.
   */
  BSStructureItem (int width, int height,
                   const QImage *im, BSDetector *detector);

  /**
   * \brief Deletes the pixel analysis grid.
   */
  ~BSStructureItem ();

  /**
   * \brief Return the width.
   */
  inline int getWidth () const { return w; }

  /**
   * \brief Return the height.
   */
  inline int getHeight () const { return h; }

  /**
   * \brief Return the zoom factor.
   */
  inline int zoomFactor () const { return zoom; }

  /**
   * \brief Zooms the grid in.
   */
  void zoomIn ();

  /**
   * \brief Zooms the grid out.
   */
  void zoomOut ();

  /**
   * \brief Return the focus point abscissae.
   */
  inline int focusX () const { return focx; }

  /**
   * \brief Return the focus point ordinate.
   */
  inline int focusY () const { return focy; }

  /**
   * \brief Shifts the grid.
   * @param dx shift X value.
   * @param dy shift Y value.
   */
  void shift (int dx, int dy);

  /**
   * \brief Toggles the displayed information.
   * @param next Get next information if true, previous on otherwise.
   */
  void toggleDisplay (bool next);

  /**
   * \brief Switches on or off the information text display modality.
   */
  inline void switchInfoDisplay () { verbose = ! verbose; }

  /**
   * \brief Switches on or off the scan line display modality.
   */
  inline void switchScanDisplay () { displayScanLines = ! displayScanLines; }

  /**
   * \brief Switches on or off the input selection display modality.
   */
  inline void switchInputDisplay () { displayInput = ! displayInput; }

  /**
   * \brief Returns the displayed information title.
   */
  inline QString itemTitle () const {
    if (displayItem == DISPLAY_FINAL_BLURRED_SEGMENT)
      return ("Final blurred segment");
    else if (displayItem == DISPLAY_FINAL_CONNECTED_COMPONENTS)
      return ("Final connected components");
    else if (displayItem == DISPLAY_FINAL_SCANS_AND_FILTER)
      return ("Final scans and filter");
    else if (displayItem == DISPLAY_INITIAL_BLURRED_SEGMENT)
      return ("Initial blurred segment");
    else if (displayItem == DISPLAY_INITIAL_CONNECTED_COMPONENTS)
      return ("Initial connected components");
    else if (displayItem == DISPLAY_INITIAL_SCANS_AND_FILTER)
      return ("Initial scans and filter");
    else if (displayItem == DISPLAY_PRELIM_BLURRED_SEGMENT)
      return ("Preliminary blurred segment");
    else if (displayItem == DISPLAY_PRELIM_CONNECTED_COMPONENTS)
      return ("Preliminary connected components");
    else if (displayItem == DISPLAY_PRELIM_SCANS_AND_FILTER)
      return ("Preliminary scans and filter");
    else return ("No info");
  }

  /**
   * \brief Returns the grid area.
   */
  QRectF boundingRect () const;

  /**
   * \brief Redraws the pixel analysis grid.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);


private:

  /** Available information : final blurred segment pixels and bounds. */
  static const int DISPLAY_FINAL_BLURRED_SEGMENT;
  /** Available information : final connected components. */
  static const int DISPLAY_FINAL_CONNECTED_COMPONENTS;
  /** Available information : final scans and filter output. */
  static const int DISPLAY_FINAL_SCANS_AND_FILTER;
  /** Available information : initial blurred segment points and bounds. */
  static const int DISPLAY_INITIAL_BLURRED_SEGMENT;
  /** Available information : initial connected components. */
  static const int DISPLAY_INITIAL_CONNECTED_COMPONENTS;
  /** Available information : initial scans and filter output. */
  static const int DISPLAY_INITIAL_SCANS_AND_FILTER;
  /** Available information : preliminary blurred segment points and bounds. */
  static const int DISPLAY_PRELIM_BLURRED_SEGMENT;
  /** Available information : preliminary connected components. */
  static const int DISPLAY_PRELIM_CONNECTED_COMPONENTS;
  /** Available information : preliminary scans and filter output. */
  static const int DISPLAY_PRELIM_SCANS_AND_FILTER;
  /** Number of the first information. */
  static const int DISPLAY_MIN;
  /** Number of the last information. */
  static const int DISPLAY_MAX;

  /** Default value for pen width. */
  static const int DEFAULT_PEN_WIDTH;
  /** Left margin for information text. */
  static const int LEFT_MARGIN;
  /** Information text height. */
  static const int TEXT_HEIGHT;

  const QImage *im;
  /** Grid width. */
  int w;
  /** Grid height. */
  int h;
  /** Zoom factor. */
  static const int MAX_ZOOM;
  /** Zoom factor. */
  int zoom;
  /** Focus point abscissae. */
  int focx;
  /** Focus point ordinate. */
  int focy;
  /** Segment detector. */
  BSDetector *det;
  /** Displayed information. */
  int displayItem;
  /** Scan display modality. */
  bool displayScanLines;
  /** Input selection display modality. */
  bool displayInput;
  /** Information text modality. */
  bool verbose;
  /** Information text style. */
  QPen infoPen;
  /** Information text vertical offset. */
  int textOffset;


  /**
   * \brief Draws blurred segment structure information for the given step.
   * @param painter : Painter to be decorated.
   * @param step Initial step addressed if set to 0, final step otherwise.
   */
  void paintBlurredSegment (QPainter *painter, int step);

  /**
   * \brief Draws connected components information for the given step.
   * @param painter : Painter to be decorated.
   * @param step Initial step addressed if set to 0, final step otherwise.
   */
  void paintConnectedComponents (QPainter *painter, int step);

  /**
   * \brief Draws scans and filter information for the given step.
   * @param painter : Painter to be decorated.
   * @param step Initial step addressed if set to 0, final step otherwise.
   */
  void paintScansAndFilter (QPainter *painter, int step);

  /**
   * \brief Draws a vector of pixels with given color.
   * @param painter : Painter to be decorated.
   * @param pix : Pixels to display.
   * @param col : Pixels color.
   */
  void paintPixels (QPainter *painter,
                    const vector<Pt2i> &pix, const QColor col);

  /**
   * \brief Draws a list of image pixels.
   * @param painter : Painter to be decorated.
   * @param pix : List of pixels to display.
   * @param col : Pixels color.
   */
  void paintPixels (QPainter *painter, const vector<Pt2i> &pix);

  /**
   * \brief Draws a pixel with given color.
   * @param painter : Painter to be decorated.
   * @param pix : Pixel to display.
   * @param col : Color to apply.
   */
  void paintPixel (QPainter *painter, const Pt2i &pix, const QColor col);

  /**
   * \brief Initializes the text display (color and position).
   * @param painter : Painter to be decorated.
   */
  void initText (QPainter *painter);

  /**
   * \brief Paints a new text in the graphics item (updates text position).
   * @param painter : Painter to be decorated.
   * @param text : Text to be displayed.
   */
  void addText (QPainter *painter, const QString &text);
};

#endif
