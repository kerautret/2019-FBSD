#include <cstdlib>
#include <iostream>
#include <QtGui>
#include "bsidetitem.h"
#include "directionalscanner.h"

using namespace std;



const int BSIdetItem::DISPLAY_INTENSITY = 1;
const int BSIdetItem::DISPLAY_MIN = DISPLAY_INTENSITY;
const int BSIdetItem::DISPLAY_MAX = DISPLAY_INTENSITY;

const int BSIdetItem::MIN_SCAN = 8;



BSIdetItem::BSIdetItem (BSDetector *detector)
{
  margin = 5;
  resol = 4;
  width = 800;
  height = 200;

  image = NULL;
  imageWidth = 0;
  imageHeight = 0;
  gMap = NULL;

  displayItem = DISPLAY_INTENSITY;
  det = detector;
  ds = NULL;

  offx = 0;
  offy = 0;
}


QRectF BSIdetItem::boundingRect () const
{
  return QRectF (0, 0, width + 40, height + 40);
}


void BSIdetItem::paint (QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
  Q_UNUSED (option);
  Q_UNUSED (widget);

  if (det->getBlurredSegments().empty () || det->getMaxDetections () > 0)
  {
//    buildScans ();
    paintStripes (painter);
  }
}


void BSIdetItem::setImage (QImage *image, VMap *idata)
{
  this->image = image;
  this->gMap = idata;
  this->imageWidth = image->width ();
  this->imageHeight = image->height ();
  scanp.setSize (image->width (), image->height ());
}


void BSIdetItem::buildScans ()
{
  if ((! det->getBlurredSegments().empty ()) && det->getMaxDetections () == 0)
    return;

  if (ds != NULL)
  {
    delete ds;
    ds = NULL;
  }

  // Updates the central scan end points for parallel display
  Pt2i ptc;
  int scanw = 0;
  int step = (det->isPreliminary () ?
              BSDetector::STEP_PRELIM : BSDetector::STEP_INITIAL);
  det->getScanInput (step, pt1, pt2, scanw, ptc);

  // Resets the idets
  rightscan.clear ();
  leftscan.clear ();
  offx = 0;
  offy = 0;

  // Gets a scan iterator
  if (scanw != 0)
    ds = scanp.getScanner (ptc, pt1.vectorTo (pt2), scanw, false);
  else ds = scanp.getScanner (pt1, pt2);

  // Extracts the left scan (with central one)
  vector<Pt2i> pix;
  if (ds->first (pix) < MIN_SCAN) { delete ds; ds = NULL; return;}
  leftscan.push_back (pix);
  int sw = pix.size ();

  bool leftScanOn = true;
  maxStripe = 0;
  while (leftScanOn)
  {
    vector<Pt2i> scan;
    if (ds->nextOnLeft (scan) < sw - 1) leftScanOn = false;
    else
    {
      leftscan.push_back (scan);
      maxStripe ++;
    }
  }

  // Extracts the right scans
  bool rightScanOn = true;
  minStripe = 1;
  while (rightScanOn)
  {
    vector<Pt2i> scan;
    if (ds->nextOnRight (scan) < sw - 1) rightScanOn = false;
    else
    {
      rightscan.push_back (scan);
      minStripe --;
    }
  }
}


void BSIdetItem::incX (int inc)
{
  offx += inc;
}


void BSIdetItem::incY (int inc)
{
  offy += inc;
  if (offy > 0) offy = 0;
}


void BSIdetItem::toggleDisplay (bool next)
{
  displayItem += (next ? 1 : -1);
  if (displayItem > DISPLAY_MAX) displayItem = DISPLAY_MIN;
  else if (displayItem < DISPLAY_MIN) displayItem = DISPLAY_MAX;
}




void BSIdetItem::paintStripes (QPainter *painter)
{
if (pt1.equals (pt2)) return;
  int lowpos = height - margin - resol * (offy + 1);
  int cpos = width / 2 - resol / 2 + resol * offx;
  int py = lowpos, px = cpos;
  vector <vector <Pt2i> >::iterator bigit;

  // Central scan (in yellow)
  if (leftscan.size ())
  {
    bigit = leftscan.begin ();
    vector<Pt2i> scan = *bigit; 
    vector<Pt2i>::iterator it = scan.begin (); 
    while (py >= margin + resol && it != scan.end ())
    {
      int col = qRed (image->pixel ((*it).x (), imageHeight - 1 - (*it).y ()));
      painter->fillRect (px, py, resol, resol,
                         QBrush (qRgb (col, col, 0)));
      it ++;
      py -= resol;
    }

    // Left side
    px -= resol;
    bigit ++;
    while (px >= resol + 1 && bigit != leftscan.end ())
    {
      py = lowpos;
      scan = *bigit;
      it = scan.begin ();
      while (py >= margin + resol && it != scan.end ())
      {
        painter->fillRect (px, py, resol, resol,
          QBrush (image->pixel ((*it).x (), imageHeight - 1 - (*it).y ())));
        it ++;
        py -= resol;
      }
      bigit ++;
      px -= resol;
    }
  }

  // Right side
  if (rightscan.size ())
  {
    px = cpos + resol;
    bigit = rightscan.begin ();
    while (px <= width - margin - resol
           && bigit != rightscan.end ())
    {
      py = lowpos;
      vector<Pt2i> scan = *bigit;
      vector<Pt2i>::iterator it = scan.begin ();
      while (py >= margin + resol && it != scan.end ())
      {
        painter->fillRect (px, py, resol, resol,
          QBrush (image->pixel ((*it).x (), imageHeight - 1 - (*it).y ())));
        it ++;
        py -= resol;
      }
      bigit ++;
      px += resol;
    }
  }

  BlurredSegment *bs = det->getBlurredSegment (
    det->isPreliminary () ? BSDetector::STEP_PRELIM
                          : BSDetector::STEP_INITIAL);
  if (bs != NULL)
  {
//    const vector<Pt2i> *pts = bs->getLeftPoints ();
//    vector<Pt2i>::const_iterator it = pts->begin ();
//    while (it != pts->end ())
    vector<Pt2i> pts = bs->getAllPoints ();
    vector<Pt2i>::iterator it = pts.begin ();
    while (it != pts.end ())
    {
      Pt2i pt = ds->locate (*it);

/*
cout << "CC PT (" << pt.x () << ", " << pt.y () << ")" << endl;
      if (pt.y () < 0) cout << "Y NEGATIF" << endl;
      if (pt.x () <= 0)
      {
        if (-pt.x () >= (int) (leftscan.size ())) cout << "DEP LEFT" << endl;
        else
        {
          if ((int) (leftscan.at(-pt.x ()).size ()) <= pt.y ())
          cout << "Y TOO FAR" << endl;
          else
          {
            Pt2i pini = leftscan.at(-pt.x ()).at(pt.y ());
            if (it->x () != pini.x () || it->y () != pini.y ())
              cout << "IT (" << it->x () << ", " << it->y () << ") -> ("
                   << pini.x () << ", " << pini.y () << ")" << endl;
          }
        }
      }
      else
      {
        if (pt.x () - 1 >= (int) (rightscan.size ()))
          cout << "DEP RIGHT" << endl;
        else
        {
          if ((int) (rightscan.at(pt.x ()-1).size ()) <= pt.y ())
            cout << "Y TOO FAR" << endl;
          else
          {
            Pt2i pini = rightscan.at(pt.x () - 1).at(pt.y ());
            if (it->x () != pini.x () || it->y () != pini.y ())
              cout << "IT (" << it->x () << ", " << it->y () << ") -> ("
                   << pini.x () << ", " << pini.y () << ")" << endl;
          }
        }
      }
*/

      int posx = cpos + resol * pt.x ();
      int posy = lowpos - resol * pt.y ();
      painter->fillRect (posx, posy, resol, resol, QBrush (Qt::blue));
      it++;
    }
  }
}
