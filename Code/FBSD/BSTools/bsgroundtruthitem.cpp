#include <QtGui>
#include <iostream>
#include <fstream>
#include "bsgroundtruthitem.h"
#include "pt2i.h"

using namespace std;


const int BSGroundtruthItem::GT_ALONE = 0;
const int BSGroundtruthItem::LSD_ALONE = 1;
const int BSGroundtruthItem::DILATED_LSD_ALONE = 2;
const int BSGroundtruthItem::GT_OVER_LSD = 3;
const int BSGroundtruthItem::ED_ALONE = 4;
const int BSGroundtruthItem::DILATED_ED_ALONE = 5;
const int BSGroundtruthItem::GT_OVER_ED = 6;
const int BSGroundtruthItem::CANNY_ALONE = 7;
const int BSGroundtruthItem::DILATED_CANNY_ALONE = 8;
const int BSGroundtruthItem::GT_OVER_CANNY = 9;
const int BSGroundtruthItem::FBSD_LINES = 10;
const int BSGroundtruthItem::FBSD_ALONE = 11;
const int BSGroundtruthItem::GT_OVER_BS = 12;
const int BSGroundtruthItem::GT_OVER_DSS = 13;
const int BSGroundtruthItem::GT_WITH_CANNY = 14;
const int BSGroundtruthItem::GT_WITH_DSS = 15;
const int BSGroundtruthItem::NO_INFO = 16;
const QColor BSGroundtruthItem::ALONE_COLOR = Qt::black;
const QColor BSGroundtruthItem::OVER_COLOR = Qt::lightGray;
const QColor BSGroundtruthItem::UNDER_COLOR = Qt::yellow;
const QColor BSGroundtruthItem::BOTH_COLOR = Qt::black;
const int BSGroundtruthItem::DEFAULT_PEN_WIDTH = 1;


BSGroundtruthItem::BSGroundtruthItem (int width, int height,
                                      const QImage *im, BSDetector *detector)
{
  w = width;
  h = height;
  int size = w * h;
  this->im = im;
  bsmap = new bool[size];
  clmap = new bool[size];
  det = detector;
  type = GT_ALONE;
  dilationType = 0;
  gts = new ExtLines ("Data/yorklines.txt", w, h);
  cannys = new ExtLines ("Data/cannylines.txt", w, h, 5);
  lsds = new ExtLines ("Data/lsdlines.txt", w, h, 7);
  eds = new ExtLines ("Data/edlines.txt", w, h, 4);
  fbsds = new ExtLines ("Data/naivelines.txt", w, h, 5);
}


BSGroundtruthItem::~BSGroundtruthItem ()
{
  delete [] bsmap;
}


QRectF BSGroundtruthItem::boundingRect () const
{
  return QRectF (0, 0, w - 1, h - 1); // ZZZ
}


void BSGroundtruthItem::paint (QPainter *painter,
                               const QStyleOptionGraphicsItem *option,
                               QWidget *widget)
{
  Q_UNUSED (option);
  Q_UNUSED (widget);

  for (int i = 0; i < w * h; i++) bsmap[i] = false;
  if (type == GT_ALONE) drawGroundTruth (painter, true);
  else if (type == ED_ALONE) drawEdLines (painter, true);
  else if (type == DILATED_ED_ALONE) drawDilatedEdLines (painter);
  else if (type == GT_OVER_ED)
  {
    drawEdLines (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == LSD_ALONE) drawLsdLines (painter, true);
  else if (type == DILATED_LSD_ALONE) drawDilatedLsdLines (painter);
  else if (type == GT_OVER_LSD)
  {
    drawLsdLines (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == CANNY_ALONE) drawCannyLines (painter, true);
  else if (type == DILATED_CANNY_ALONE) drawDilatedCannyLines (painter);
  else if (type == GT_OVER_CANNY)
  {
    drawCannyLines (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == FBSD_LINES) drawFbsdLines (painter, true);
  else if (type == FBSD_ALONE) drawDSS (painter, true);
  else if (type == GT_OVER_BS)
  {
    drawBS (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == GT_OVER_DSS)
  {
    drawDSS (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == GT_WITH_CANNY)
  {
    drawCannyLines (painter, false);
    drawGroundTruth (painter, false);
  }
  else if (type == GT_WITH_DSS)
  {
    drawDSS (painter, false, false);
    drawGroundTruth (painter, false);
  }
  else drawGroundTruth (painter, true);
  int nboth = 0, ncl = 0, nbs = 0;
  for (int i = 0; i < w * h; i++)
    if (clmap[i])
      if (bsmap[i]) nboth++;
      else ncl++;
    else if (bsmap[i]) nbs++;
  cout << nboth << " both, " << ncl << " york, " << nbs << " bs" << endl;
}


void BSGroundtruthItem::drawBS (QPainter *painter, bool alone)
{
  painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                         DEFAULT_PEN_WIDTH, Qt::SolidLine,
                         Qt::RoundCap, Qt::RoundJoin));
  vector<BlurredSegment *> bss = det->getBlurredSegments ();
  if (! bss.empty ())
  {
    vector<BlurredSegment *>::const_iterator it = bss.begin ();
    while (it != bss.end ())
    {
      if (*it != NULL) drawPoints (painter, (*it)->getAllPoints ());
      it++;
    }
  }
}


void BSGroundtruthItem::drawDSS (QPainter *painter, bool alone, bool disp)
{
  painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                         DEFAULT_PEN_WIDTH, Qt::SolidLine,
                         Qt::RoundCap, Qt::RoundJoin));
  vector<BlurredSegment *> bss = det->getBlurredSegments ();
  if (! bss.empty ())
  {
    vector<BlurredSegment *>::const_iterator it = bss.begin ();
    while (it != bss.end ())
    {
      if ((*it) != NULL)
      {
        DigitalStraightSegment *dss = (*it)->getSegment ();
        if (dss != NULL)
        {
          vector<Pt2i> pts;
          dss->getPoints (pts);
          drawPoints (painter, pts, disp);
        }  
      }
      it++;
    }
  }
}


void BSGroundtruthItem::drawPoints (QPainter *painter,
                                    vector<Pt2i> pts, bool disp)
{
  vector<Pt2i>::iterator iter = pts.begin ();
  while (iter != pts.end ())
  {
    Pt2i p = *iter++;
    if (p.x() < w && p.y() < h && p.x() >= 0 && p.y() >= 0)
    {
      bsmap[p.y()*w+p.x()] = true;
      if (disp) painter->drawPoint (QPoint (p.x(), h - 1 - p.y()));  // dec 1
    }
  }
}


void BSGroundtruthItem::drawGroundTruth (QPainter *painter, bool alone)
{
  if (gts->areLinesLoaded ())
  {
    int n = 0;
    Pt2i *pts = NULL;
    int nb = gts->countOfLines ();
    for (int i = 0; i < nb; i++)
    {
      pts = Pt2i (gts->xStart (i), gts->yStart (i)).drawing (
            Pt2i (gts->xEnd (i), gts->yEnd (i)), &n);
      for (int i = 0; i < n; i++)
      {
        if (pts[i].x () >= 0 && pts[i].x () < w
            && pts[i].y () >= 0 && pts[i].y () < h)
        {
          painter->setPen (QPen (alone || bsmap[pts[i].y()*w+pts[i].x()] ?
                                 BOTH_COLOR : OVER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pts[i].x (),
                                      h - 1 - pts[i].y ()));  // dec 1
          clmap[pts[i].y()*w+pts[i].x()] = true;
        }
      }
      delete [] pts;
    }
  }
}


void BSGroundtruthItem::drawLsdLines (QPainter *painter, bool alone, bool disp) {
  int n = 0;
  Pt2i *pts = NULL;
  int nb = lsds->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    pts = Pt2i (lsds->xStart (i), lsds->yStart (i)).drawing (
          Pt2i (lsds->xEnd (i), lsds->yEnd (i)), &n);
    for (int i = 0; i < n; i++)
    {
      if (pts[i].x () >= 0 && pts[i].x () < w
          && pts[i].y () >= 0 && pts[i].y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pts[i].x (),
                                      h - 1 - pts[i].y ()));  // dec 1
        }
        bsmap[pts[i].y()*w+pts[i].x()] = true;
      }
    }
    delete [] pts;
  }
}


void BSGroundtruthItem::drawDilatedLsdLines (QPainter *painter, bool disp)
{
  int nb = lsds->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    int xs = lsds->xStart (i);
    int ys = lsds->yStart (i);
    int xe = lsds->xEnd (i);
    int ye = lsds->yEnd (i);
    int minx = (xe < xs ? xe : xs);
    int miny = (ye < ys ? ye : ys);
    int maxx = (xe < xs ? xs : xe);
    int maxy = (ye < ys ? ys : ye);
    DigitalStraightSegment *dss = new DigitalStraightSegment (
      Pt2i (xs, ys), Pt2i (xe, ye),
      DigitalStraightLine::DSL_NAIVE, minx, miny, maxx, maxy);
    vector<Pt2i> points;
    if (dilationType > 0)
    {
      int dil = 0;
      if (dilationType == 1) dil = dss->period ();
      else if (dilationType == 2) dil = dss->standard ();
      else if (dilationType == 3) dil = 2 * dss->period ();
cout << "Dilation of " << dil << endl;
      dss->dilate (dil);
    }
    dss->getPoints (points);
    vector<Pt2i>::iterator pit = points.begin ();
    while (pit != points.end ())
    {
      if (pit->x () >= 0 && pit->x () < w && pit->y () >= 0 && pit->y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pit->x (), h - 1 - pit->y ()));
        }
        bsmap[pit->y()*w+pit->x()] = true;
      }
      pit ++;
    }
    delete dss;
  }
}


void BSGroundtruthItem::drawEdLines (QPainter *painter, bool alone, bool disp)
{
  int n = 0;
  Pt2i *pts = NULL;
  int nb = eds->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    pts = Pt2i (eds->xStart (i), eds->yStart (i)).drawing (
          Pt2i (eds->xEnd (i), eds->yEnd (i)), &n);
    for (int i = 0; i < n; i++)
    {
      if (pts[i].x () >= 0 && pts[i].x () < w
          && pts[i].y () >= 0 && pts[i].y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pts[i].x (),
                                      h - 1 - pts[i].y ()));  // dec 1
        }
        bsmap[pts[i].y()*w+pts[i].x()] = true;
      }
    }
    delete [] pts;
  }
}


void BSGroundtruthItem::drawDilatedEdLines (QPainter *painter, bool disp)
{
  int nb = eds->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    int xs = eds->xStart (i);
    int ys = eds->yStart (i);
    int xe = eds->xEnd (i);
    int ye = eds->yEnd (i);
    int minx = (xe < xs ? xe : xs);
    int miny = (ye < ys ? ye : ys);
    int maxx = (xe < xs ? xs : xe);
    int maxy = (ye < ys ? ys : ye);
    DigitalStraightSegment *dss = new DigitalStraightSegment (
      Pt2i (xs, ys), Pt2i (xe, ye),
      DigitalStraightLine::DSL_NAIVE, minx, miny, maxx, maxy);
    vector<Pt2i> points;
    if (dilationType > 0)
    {
      int dil = 0;
      if (dilationType == 1) dil = dss->period ();
      else if (dilationType == 2) dil = dss->standard ();
      else if (dilationType == 3) dil = 2 * dss->period ();
cout << "Dilation of " << dil << endl;
      dss->dilate (dil);
    }
    dss->getPoints (points);
    vector<Pt2i>::iterator pit = points.begin ();
    while (pit != points.end ())
    {
      if (pit->x () >= 0 && pit->x () < w && pit->y () >= 0 && pit->y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pit->x (), h - 1 - pit->y ()));
        }
        bsmap[pit->y()*w+pit->x()] = true;
      }
      pit ++;
    }
    delete dss;
  }
}


void BSGroundtruthItem::drawCannyLines (QPainter *painter,
                                        bool alone, bool disp)
{
  int n = 0;
  Pt2i *pts = NULL;
  int nb = cannys->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    pts = Pt2i (cannys->xStart (i), cannys->yStart (i)).drawing (
          Pt2i (cannys->xEnd (i), cannys->yEnd (i)), &n);
    for (int i = 0; i < n; i++)
    {
      if (pts[i].x () >= 0 && pts[i].x () < w
          && pts[i].y () >= 0 && pts[i].y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pts[i].x (),
                                      h - 1 - pts[i].y ()));  // dec 1
        }
        bsmap[pts[i].y()*w+pts[i].x()] = true;
      }
    }
    delete [] pts;
  }
}


void BSGroundtruthItem::drawDilatedCannyLines (QPainter *painter, bool disp)
{
  int nb = cannys->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    int xs = cannys->xStart (i);
    int ys = cannys->yStart (i);
    int xe = cannys->xEnd (i);
    int ye = cannys->yEnd (i);
    int minx = (xe < xs ? xe : xs);
    int miny = (ye < ys ? ye : ys);
    int maxx = (xe < xs ? xs : xe);
    int maxy = (ye < ys ? ys : ye);
    DigitalStraightSegment *dss = new DigitalStraightSegment (
      Pt2i (xs, ys), Pt2i (xe, ye),
      DigitalStraightLine::DSL_NAIVE, minx, miny, maxx, maxy);
    vector<Pt2i> points;
    if (dilationType > 0)
    {
      int dil = 0;
      if (dilationType == 1) dil = dss->period ();
      else if (dilationType == 2) dil = dss->standard ();
      else if (dilationType == 3) dil = 2 * dss->period ();
      dss->dilate (dil);
    }
    dss->getPoints (points);
    vector<Pt2i>::iterator pit = points.begin ();
    while (pit != points.end ())
    {
      if (pit->x () >= 0 && pit->x () < w && pit->y () >= 0 && pit->y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pit->x (), h - 1 - pit->y ()));
        }
        bsmap[pit->y()*w+pit->x()] = true;
      }
      pit ++;
    }
    delete dss;
  }
}


void BSGroundtruthItem::drawFbsdLines (QPainter *painter, bool alone, bool disp)
{
  int n = 0;
  Pt2i *pts = NULL;
  int nb = fbsds->countOfLines ();
  for (int i = 0; i < nb; i++)
  {
    pts = Pt2i (fbsds->xStart (i), fbsds->yStart (i)).drawing (
          Pt2i (fbsds->xEnd (i), fbsds->yEnd (i)), &n);
    for (int i = 0; i < n; i++)
    {
      if (pts[i].x () >= 0 && pts[i].x () < w
          && pts[i].y () >= 0 && pts[i].y () < h)
      {
        if (disp)
        {
          painter->setPen (QPen (alone ? ALONE_COLOR : UNDER_COLOR,
                             DEFAULT_PEN_WIDTH, Qt::SolidLine,
                             Qt::RoundCap, Qt::RoundJoin));
          painter->drawPoint (QPoint (pts[i].x (),
                                      h - 1 - pts[i].y ()));  // dec 1
        }
        bsmap[pts[i].y()*w+pts[i].x()] = true;
      }
    }
    delete [] pts;
  }
}


bool BSGroundtruthItem::toggleDilationType ()
{
  if (++dilationType == 4) dilationType = 0;
  return (type == DILATED_CANNY_ALONE
          || type == DILATED_ED_ALONE || type == DILATED_LSD_ALONE);
}
