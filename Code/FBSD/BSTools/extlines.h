#ifndef EXT_LINES_H
#define EXT_LINES_H


#include <QtGui>
#include <vector>
#include "blurredsegment.h"

using namespace std;


/** 
 * @class ExtLines extlines.h
 * \brief Comparable lines set from an external tool.
 */
class ExtLines
{
public:

  /**
   * \brief Creates an external lines set and runs statistics.
   * @param groundTruth Indicates if testing with a lines groundtruth.
   * @param extdets Indicates if testing with external detectors.
   */
  ExtLines (bool groundTruth, bool extdets);

  /**
   * \brief Creates an external lines set.
   */
  ExtLines (const char *name, int width, int height, int nbinfos = 4);

  /**
   * \brief Deletes the external lines set.
   */
  ~ExtLines ();

  /**
   * \brief Returns the count of lines in the set.
   */
  inline int countOfLines () const { return ((int) exts.size ()); }

  /**
   * \brief Returns the start point X coordinate of a line.
   */
  inline int xStart (int index) const {
    return ((int) (exts[index].sx + 0.5)); }

  /**
   * \brief Returns the start point Y coordinate of a line.
   */
  inline int yStart (int index) const {
    return ((int) (exts[index].sy + 0.5)); }

  /**
   * \brief Returns the end point X coordinate of a line.
   */
  inline int xEnd (int index) const {
    return ((int) (exts[index].ex + 0.5)); }

  /**
   * \brief Returns the end point Y coordinate of a line.
   */
  inline int yEnd (int index) const {
    return ((int) (exts[index].ey + 0.5)); }

  /**
   * \brief Indicates if lines are available.
   */
  inline bool areLinesLoaded () const { return (loaded); }

  /**
   * \brief Returns the covering ratio of given blurred segments.
   */
  double covering (const vector<BlurredSegment *> &segs) const;

  /**
   * \brief Returns the covering ratio of external lines.
   * @param name External lines file name.
   * @param nbinfo count of data per line.
   * @param nb Extracted number of lines.
   * @param lb Extracted total length of lines.
   * @param minl Minimal length of extarcted lines.
   */
  double covering (const char *name, int nbinfo,
                   int &nb, double &lg, double minl) const;

  /**
   * \brief Returns the count and cumulated length of external lines.
   * @param name External lines file name.
   * @param nbinfo count of data per line.
   * @param lb Extracted total length of lines.
   * @param minl Minimal length of extarcted lines.
   */
  int count (const char *name, int nbinfo, double &lg, double minl) const;


private:

  /** Minimal length of considered lines. */
  static const double MIN_LENGTH;

 /** Aggregation of segment extraction results with initial conditions. */
  struct ExtLine
  {
    /** External line start point X coordinate. */
    double sx;
    /** External line start point Y coordinate. */
    double sy;
    /** External line end point X coordinate. */
    double ex;
    /** External line end point Y coordinate. */
    double ey;
  };

  /** Indicates if a lines are available. */
  bool loaded;
  /** Set of recorded external lines. */
  vector<ExtLine> exts;

  /** Image width */
  int width;
  /** Image height */
  int height;


  /** Extracts covering stats on the detector.
   * @param im Test image.
   * @param alt Alternate version tested.
   */
  void stats (const QImage &im, bool alt);

  /** Extracts covering stats on an external detector.
   * @param lname Input line file from external detector.
   * @param nbinfo Count of values per line in the file.
   * @param covname Output file of covering value.
   * @param lgname Output file of lines count and length.
   */
  void stats (const char *lname, int nbinfo,
              const char *covname, const char *lgname);

};

#endif
