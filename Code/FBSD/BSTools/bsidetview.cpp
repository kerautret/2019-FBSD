#include <QtGui>
#include <iostream>
#include <cstdlib>
#include "bsidetview.h"

using namespace std;



BSIdetView::BSIdetView (BSDetector *detector)
{
  // CAUTION : don't activate antialiasing here !!!
  setBackgroundBrush (QBrush (Qt::white));
  setScene (new QGraphicsScene (0, 0, 800, 200));
  idet = new BSIdetItem (detector);
  scene()->addItem (idet);
  setWindowTitle (idet->itemTitle ());
  resize (QSize (816, 216));
}


BSIdetView::~BSIdetView ()
{
  scene()->removeItem (idet);
  delete idet;
}


void BSIdetView::paint (QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget)
{
  Q_UNUSED (option);
  Q_UNUSED (widget);
  Q_UNUSED (painter);
}


void BSIdetView::setImage (QImage *image, VMap *idata)
{
  idet->setImage (image, idata);
}


bool BSIdetView::processKeyEvent (QKeyEvent *event)
{
  switch (event->key ())
  {
    case Qt::Key_I :
      idet->toggleDisplay ((event->modifiers () & Qt::ShiftModifier) == 0);
      setWindowTitle (idet->itemTitle ());
      scene()->update ();
      break;

    case Qt::Key_P : // Capture
      // viewport()->grab (
      //   QRect (QPoint (0, 0),
      //          QSize (idet->getWidth(), idet->getHeight()))
      //   ).toImage().save ("firstDetection.png");
      // cout << "First detection window shot in firstDetection.png" << endl;
      break;

    case Qt::Key_Left :
      idet->incX (-1);
      scene()->update ();
      break;

    case Qt::Key_Right :
      idet->incX (1);
      scene()->update ();
      break;

    case Qt::Key_Up :
      idet->incY (1);
      scene()->update ();
      break;

    case Qt::Key_Down :
      idet->incY (-1);
      scene()->update ();
      break;
  }
  return false;
}


void BSIdetView::update ()
{
  idet->buildScans ();
  scene()->update ();
}
