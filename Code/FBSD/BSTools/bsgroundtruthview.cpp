#include <QtGui>
#include <iostream>
#include "bsgroundtruthview.h"
#include "math.h"

using namespace std;


BSGroundtruthView::BSGroundtruthView (QImage *im, BSDetector *sd)
{
  int w = im->width ();
  int h = im->height ();
  image = im;
  setScene (new QGraphicsScene (0, 0, w, h));
  groundtruth = new BSGroundtruthItem (w, h, image, sd);
  //setBackgroundBrush (QBrush (*image));
  scene()->addItem (groundtruth);
  setWindowTitle (groundtruth->itemTitle ());
}


BSGroundtruthView::~BSGroundtruthView ()
{
  scene()->removeItem (groundtruth);
  delete groundtruth;
}


void BSGroundtruthView::paint (QPainter *painter,
                               const QStyleOptionGraphicsItem *option,
                               QWidget *widget)
{
  Q_UNUSED (painter);
  Q_UNUSED (option);
  Q_UNUSED (widget);
}


bool BSGroundtruthView::processKeyEvent (QKeyEvent *event)
{
  bool processed = false;
  switch (event->key ())
  {
    case Qt::Key_I : // Info
      groundtruth->toggleType (event->modifiers () & Qt::ShiftModifier);
      scene()->update ();
      update ();
      setWindowTitle (groundtruth->itemTitle ());
      break;

    case Qt::Key_D : // Canny dilation
      if (groundtruth->toggleDilationType ())
      {
        scene()->update ();
        update ();
      }
      break;

    case Qt::Key_P : // Capture
      viewport()->grab (
        QRect (QPoint (0, 0),
               QSize (groundtruth->getWidth(), groundtruth->getHeight()))
        ).toImage().save ("external.png");
      cout << "External detectors view shot in external.png" << endl;
      break;
  }
  return processed;
}
