#include <iostream>
#include <fstream>
#include <cmath>
#include "extlines.h"
#include "bsdetector.h"

using namespace std;


const double ExtLines::MIN_LENGTH = 10.;


ExtLines::ExtLines (bool groundTruth, bool extdets)
{
  QImage im;
  im.load ("Data/york.jpg");
  width = im.width ();
  height = im.height ();
  loaded = false;

  if (groundTruth)
  {
    double val[4];
    int i = 0, nb = 0;
    ifstream input ("Data/yorklines.txt", ios::in);
    if (input.is_open ())
    {
      bool reading = true;
      while (reading)
      {
        input >> val[i];
        if (input.eof ()) reading = false;
        if (++i == 4)
        {
          ExtLine cl;
          cl.sx = val[0];
          cl.sy = height - 1 - val[1];
          cl.ex = val[2];
          cl.ey = height - 1 - val[3];
          if (cl.sx == cl.ex && cl.sy == cl.ey)
            cout << "LIGNE NULLE !!!" << endl;
          else
          {
            exts.push_back (cl);
            nb++;
          }
          i = 0;
        }
      }
      loaded = true;
    }
  }
  stats (im, false);
  if (extdets)
  {
    stats ("Data/cannylines.txt", 5, "Data/cannycov.txt", "Data/cannyln.txt");
    stats ("Data/edlines.txt", 4, "Data/edcov.txt", "Data/edln.txt");
    stats ("Data/lsdlines.txt", 7, "Data/lsdcov.txt", "Data/lsdln.txt");
    stats ("Data/naivelines.txt", 5, "Data/naivecov.txt", "Data/naiveln.txt");
  }
  else stats (im, true);
}


ExtLines::ExtLines (const char *name, int width, int height, int nbinfo)
{
  this->width = width;
  this->height = height;

  double val[nbinfo];
  int i = 0, nb = 0;
  ifstream input (name, ios::in);
  if (input.is_open ())
  {
    bool reading = true;
    while (reading)
    {
      input >> val[i];
      if (input.eof ()) reading = false;
      if (++i == nbinfo)
      {
        ExtLine cl;
        cl.sx = val[0];
        cl.sy = height - 1 - val[1];
        cl.ex = val[2];
        cl.ey = height - 1 - val[3];
        if (cl.sx == cl.ex && cl.sy == cl.ey)
          cout << "LIGNE NULLE !!!" << endl;
        else
        {
          exts.push_back (cl);
          nb++;
        }
        i = 0;
      }
    }
    loaded = true;
  }
  else loaded = false;
}


ExtLines::~ExtLines ()
{
}


double ExtLines::covering (const vector<BlurredSegment *> &segs) const
{
  bool cmap[width * height];
  for (int i = 0; i < width * height; i++) cmap[i] = false;
  vector<BlurredSegment *>::const_iterator it = segs.begin ();
  while (it != segs.end ())
  {
    DigitalStraightSegment *dss1 = (*it)->getSegment ();
    if (dss1 != NULL)
    {
      DigitalStraightSegment *dss = dss1->dilation (dss1->standard ());
      vector<Pt2i> points;
      dss->getPoints (points);
      vector<Pt2i>::iterator pit = points.begin ();
      while (pit != points.end ())
      {
        if (pit->x () >= 0 && pit->x () < width
            && pit->y () >= 0 && pit->y () < height)
          cmap[pit->x() + pit->y() * width] = true;
        pit++;
      }
      delete (dss);
    }
    it++;
  }

  int nbext = 0;
  int nbextin = 0;
  int nbextout = 0;
  int n = 0;
  Pt2i *pts = NULL;
  vector<ExtLine>::const_iterator yit = exts.begin ();
  while (yit != exts.end ())
  {
    n = 0;
    pts = Pt2i ((int) (yit->sx + 0.5), (int) (yit->sy + 0.5)).drawing (
          Pt2i ((int) (yit->ex + 0.5), (int) (yit->ey + 0.5)), &n);
    for (int i = 0; i < n; i++)
      if (cmap[pts[i].y () * width + pts[i].x ()]) nbextin ++;
      else nbextout ++;
    nbext += n;
    yit++;
    delete [] pts;
  }
  return (nbextin / (double) nbext);
}


double ExtLines::covering (const char *name, int nbinfo,
                           int &nb, double &lg, double minl) const
{
  // 1. Reading external lines file
  vector<ExtLine> cannys;
  double val[nbinfo];
  double clLength = 0.;
  int i = 0;
  nb = 0;
  lg = 0.;
  ifstream input (name, ios::in);
  if (input)
  {
    bool reading = true;
    while (reading)
    {
      input >> val[i];
      if (input.eof ()) reading = false;
      if (++i == nbinfo)
      {
        ExtLine cl;
        cl.sx = val[0];
        cl.sy = height - 1 - val[1];
        cl.ex = val[2];
        cl.ey = height - 1 - val[3];
        clLength = sqrt ((cl.sx - cl.ex) * (cl.sx - cl.ex)
                         + (cl.sy - cl.ey) * (cl.sy - cl.ey));
        if (clLength >= minl)
        {
          cannys.push_back (cl);
          lg += clLength;
          nb++;
        }
        i = 0;
      }
    }
  }

  // 2. Spreading external lines
  bool cmap[width * height];
  for (int i = 0; i < width * height; i++) cmap[i] = false;
  vector<ExtLine>::iterator it = cannys.begin ();
  while (it != cannys.end ())
  {
    int xs = (int) (it->sx + 0.5);
    int ys = (int) (it->sy + 0.5);
    int xe = (int) (it->ex + 0.5);
    int ye = (int) (it->ey + 0.5);
    int minx = (xe < xs ? xe : xs);
    int miny = (ye < ys ? ye : ys);
    int maxx = (xe < xs ? xs : xe);
    int maxy = (ye < ys ? ys : ye);
    DigitalStraightSegment *dss = new DigitalStraightSegment (
      Pt2i (xs, ys), Pt2i (xe, ye),
      DigitalStraightLine::DSL_NAIVE, minx, miny, maxx, maxy);
    dss->dilate (dss->standard ());
    vector<Pt2i> points;
    dss->getPoints (points);
    vector<Pt2i>::iterator pit = points.begin ();
    while (pit != points.end ())
    {
      if (pit->x () >= 0 && pit->x () < width
          && pit->y () >= 0 && pit->y () < height)
        cmap[pit->x() + pit->y() * width] = true;
      pit++;
    }
    delete (dss);
    it++;
  }

  // 3. Covering external lines
  int nbext = 0;
  int nbextin = 0;
  int nbextout = 0;
  int n = 0;
  Pt2i *pts = NULL;
  vector<ExtLine>::const_iterator yit = exts.begin ();
  while (yit != exts.end ())
  {
    n = 0;
    pts = Pt2i ((int) (yit->sx + 0.5), (int) (yit->sy + 0.5)).drawing (
          Pt2i ((int) (yit->ex + 0.5), (int) (yit->ey + 0.5)), &n);
    for (int i = 0; i < n; i++)
      if (cmap[pts[i].y () * width + pts[i].x ()]) nbextin ++;
      else nbextout ++;
    nbext += n;
    yit++;
    delete [] pts;
  }
  return (nbextin / (double) nbext);
}


int ExtLines::count (const char *name, int nbinfo,
                     double &lg, double minl) const
{
cout << "COUNTING" << endl;
  vector<ExtLine> cannys;
  double val[nbinfo];
  double clLength = 0.;
  int i = 0;
  int nb = 0;
  lg = 0.;
  ifstream input (name, ios::in);
  bool reading = true;
  if (input)
  {
    while (reading)
    {
      input >> val[i];
      if (input.eof ()) reading = false;
      if (++i == nbinfo)
      {
        ExtLine cl;
        cl.sx = val[0];
        cl.sy = height - 1 - val[1];
        cl.ex = val[2];
        cl.ey = height - 1 - val[3];
        cannys.push_back (cl);
        clLength = sqrt ((cl.sx - cl.ex) * (cl.sx - cl.ex)
                         + (cl.sy - cl.ey) * (cl.sy - cl.ey));
        if (clLength > minl)
        {
          lg += clLength;
          nb++;
        }
        i = 0;
      }
    }
  }
  return (nb);
}


void ExtLines::stats (const QImage &im, bool alt)
{
  BSDetector detector;
  if (alt) detector.setStaticDetector (true);
  int **tabImage = new int*[height];
  for (int i = 0; i < height; i++)
  {
    tabImage[i] = new int[width];
    for(int j = 0; j < width; j++)
    {
      QColor c = QColor (im.pixel (j, height - i - 1));
      tabImage[i][j] = c.value ();
    }
  }
  detector.setGradientMap (new VMap (width, height, tabImage,
                                     VMap::TYPE_SOBEL_5X5));
  if (MIN_LENGTH != 0.)
  {
    if (! detector.isFinalSizeTestOn ()) detector.switchFinalSizeTest ();
    detector.setFinalSizeMinValue ((int) (MIN_LENGTH + 0.5));
  }
  else
    if (detector.isFinalSizeTestOn ()) detector.switchFinalSizeTest ();
  detector.detectAll ();
  vector<BlurredSegment *> bss = detector.getBlurredSegments ();
  if (loaded)
  {
    double covFBSD = covering (bss);
    if (alt)
    {
      ofstream outf ("Data/altcov.txt", ios::out);
      outf << covFBSD << endl;
      outf.close ();
    }
    else
    {
      ofstream outf ("Data/fbsdcov.txt", ios::out);
      outf << covFBSD << endl;
      outf.close ();
    }
  }

  double mylength, mylg = 0., myth = 0.;
  int mynb = 0;
  // int nbs = (int) bss.size ();
  vector<BlurredSegment *>::iterator it = bss.begin ();
  while (it != bss.end ())
  {
    Pt2i lastleft = (*it)->getLastLeft ();
    Pt2i lastright = (*it)->getLastRight ();
    mylength = sqrt ((lastleft.x () - lastright.x ())
                     * (lastleft.x () - lastright.x ())
                     + (lastleft.y () - lastright.y ())
                     * (lastleft.y () - lastright.y ()));
    if (mylength >= MIN_LENGTH)
    {
      DigitalStraightSegment *dss = (*it)->getSegment ();
      if (dss != NULL)
      {
        AbsRat eth2 = dss->squaredEuclideanThickness ();
        double eth = sqrt (eth2.num () / (double) (eth2.den ()));
        myth += eth * mylength;
      }
      mylg += mylength;
      mynb++;
    }
    it ++;
  }
  if (alt)
  {
    ofstream outl ("Data/altln.txt", ios::out);
    outl << mynb << " " << mylg << " " << (mylg / mynb) << endl;
    outl.close ();
    ofstream outth ("Data/altth.txt", ios::out);
    outth << myth << " " << (myth / mylg) << endl;
    outth.close ();
  }
  else
  {
    ofstream outl ("Data/fbsdln.txt", ios::out);
    outl << mynb << " " << mylg << " " << (mylg / mynb) << endl;
    outl.close ();
    ofstream outth ("Data/fbsdth.txt", ios::out);
    outth << myth << " " << (myth / mylg) << endl;
    outth.close ();
  }

  for (int i = 0; i < height; i++) delete [] tabImage[i];
  delete [] tabImage;
}


void ExtLines::stats (const char *lname, int nbinfo,
                      const char *covname, const char *lgname)
{
  int nel = 0;
  double lel = 0.;
  if (loaded)
  {
    double covel = covering (lname, nbinfo, nel, lel, MIN_LENGTH);
    ofstream outf (covname, ios::out);
    outf << covel << endl;
    outf.close ();
  }
  else nel = count (lname, nbinfo, lel, MIN_LENGTH);
  ofstream outl (lgname, ios::out);
  outl << nel << " " << lel << " " << (lel / nel) << " " << endl;
  outl.close ();
}
