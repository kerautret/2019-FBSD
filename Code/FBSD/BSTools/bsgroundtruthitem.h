#ifndef BS_GROUNDTRUTH_ITEM_H
#define BS_GROUNDTRUTH_ITEM_H

#include <QGraphicsItem>
#include "bsdetector.h"
#include "extlines.h"

using namespace std;


/** 
 * @class BSGroundtruthItem bsgroundtruthitem.h
 * \brief Blurred segment comparator with groundtruth lines.
 * \author {P. Even}
 */
class BSGroundtruthItem : public QGraphicsItem
{
public:

  /**
   * \brief Creates a groundtruth comparator.
   */
  BSGroundtruthItem (int width, int height,
                     const QImage *im, BSDetector *detector);

  /**
   * \brief Deletes the groundtruth comparator.
   */
  ~BSGroundtruthItem ();

  /**
   * \brief Returns the grid area.
   */
  QRectF boundingRect () const;

  /**
   * \brief Redraws the groundtruth comparator grid.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Toggles display modality.
   */
  inline void toggleType (bool back) {
    if (back) { if (--type < 0) type = NO_INFO - 1; }
    else if (++type == NO_INFO) type = 0; }

  /**
   * \brief Toggles dilation type for external lines.
   * Returns if the display should be updated.
   */
  bool toggleDilationType ();

  /**
   * \brief Returns the external lines view width.
   */
  inline int getWidth () const { return w; }

  /**
   * \brief Returns the external lines view height.
   */
  inline int getHeight () const { return h; }

  /**
   * \brief Returns the displayed information title.
   */
  inline QString itemTitle () const {
    if (type == GT_ALONE)
      return ("Groundtruth lines alone");
    else if (type == DILATED_LSD_ALONE)
      return ("Dilated_LSD lines");
    else if (type == LSD_ALONE)
      return ("LSD lines");
    else if (type == GT_OVER_LSD)
      return ("Groundtruth over LSD lines");
    else if (type == DILATED_ED_ALONE)
      return ("Dilated_ED lines");
    else if (type == ED_ALONE)
      return ("ED lines");
    else if (type == GT_OVER_ED)
      return ("Groundtruth over ED lines");
    else if (type == DILATED_CANNY_ALONE)
      return ("Dilated_Canny lines");
    else if (type == CANNY_ALONE)
      return ("Canny lines");
    else if (type == GT_OVER_CANNY)
      return ("Groundtruth over Canny lines");
    else if (type == FBSD_LINES)
      return ("FBSD output lines");
    else if (type == FBSD_ALONE)
      return ("FBSD detection");
    else if (type == GT_OVER_BS)
      return ("Groundtruth over blurred segments points");
    else if (type == GT_OVER_DSS)
      return ("Groundtruth over digital straight segments");
    else if (type == GT_WITH_CANNY)
      return ("Groundtruth blend with Canny lines");
    else if (type == GT_WITH_DSS)
      return ("Groundtruth blend with digital straight lines");
    else return ("No info");
  }



private:

  /** Groundtruth alone display modality. */
  static const int GT_ALONE;
  /** Lsd lines alone display modality. */
  static const int LSD_ALONE;
  /** Dilated Lsd lines alone display modality. */
  static const int DILATED_LSD_ALONE;
  /** Groundtruth lines over Lsd display modality. */
  static const int GT_OVER_LSD;
  /** ED lines alone display modality. */
  static const int ED_ALONE;
  /** Dilated ED lines alone display modality. */
  static const int DILATED_ED_ALONE;
  /** Groundtruth lines over ED display modality. */
  static const int GT_OVER_ED;
  /** Canny lines alone display modality. */
  static const int CANNY_ALONE;
  /** Dilated Canny lines alone display modality. */
  static const int DILATED_CANNY_ALONE;
  /** Groundtruth lines over Canny display modality. */
  static const int GT_OVER_CANNY;
  /** FBSD output lines alone display modality. */
  static const int FBSD_LINES;
  /** FBSD detected lines alone display modality. */
  static const int FBSD_ALONE;
  /** Groundtruth lines over BS display modality. */
  static const int GT_OVER_BS;
  /** Groundtruth lines over DSS display modality. */
  static const int GT_OVER_DSS;
  /** Groundtruth lines with Canny display modality. */
  static const int GT_WITH_CANNY;
  /** Groundtruth lines with DSS display modality. */
  static const int GT_WITH_DSS;
  /** No info display modality. */
  static const int NO_INFO;
  /** Default color for alone lines. */
  static const QColor ALONE_COLOR;
  /** Default color for first drawn lines. */
  static const QColor UNDER_COLOR;
  /** Default color for further drawn lines. */
  static const QColor OVER_COLOR;
  /** Default color for both tools. */
  static const QColor BOTH_COLOR;
  /** Default value for pen width. */
  static const int DEFAULT_PEN_WIDTH;

  /** Set of groundtruth lines. */
  ExtLines *gts;
  /** Set of Lsd lines. */
  ExtLines *lsds;
  /** Set of ED lines. */
  ExtLines *eds;
  /** Set of Canny lines. */
  ExtLines *cannys;
  /** Set of Fbsd lines. */
  ExtLines *fbsds;

  /** Background image. */
  const QImage *im;
  /** Grid width. */
  int w;
  /** Grid height. */
  int h;
  /** Segment detector. */
  BSDetector *det;
  /** Blurred segments map */
  bool *bsmap;
  /** Groundtruth lines map */
  bool *clmap;
  /** Display modality */
  int type;
  /** Type of dilation for external lines */
  int dilationType;

  /** Displays blurred segments pixels */
  void drawBS (QPainter *painter, bool alone);

  /** Displays blurred segments DSS */
  void drawDSS (QPainter *painter, bool alone, bool disp = true);

  /** Displays a set of points */
  void drawPoints (QPainter *painter, vector<Pt2i> pts, bool disp = true);

  /** Displays groundtruth lines */
  void drawGroundTruth (QPainter *painter, bool alone);

  /** Displays Lsd lines */
  void drawLsdLines (QPainter *painter, bool alone, bool disp = true);

  /** Displays dilated Lsd lines */
  void drawDilatedLsdLines (QPainter *painter, bool disp = true);

  /** Displays ED lines */
  void drawEdLines (QPainter *painter, bool alone, bool disp = true);

  /** Displays dilated ED lines */
  void drawDilatedEdLines (QPainter *painter, bool disp = true);

  /** Displays Canny lines */
  void drawCannyLines (QPainter *painter, bool alone, bool disp = true);

  /** Displays dilated Canny lines */
  void drawDilatedCannyLines (QPainter *painter, bool disp = true);

  /** Displays Fbsd lines */
  void drawFbsdLines (QPainter *painter, bool alone, bool disp = true);

};

#endif
