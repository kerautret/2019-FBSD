#ifndef BS_IDET_ITEM_H
#define BS_IDET_ITEM_H

#include <QGraphicsItem>
#include <QImage>
#include <QKeyEvent>
#include "vmap.h"
#include "scannerprovider.h"
#include "bsdetector.h"


/** 
 * @class BSIdetItem bsidetitem.h
 * \brief Initial detection view and controller.
 * \author {P. Even}
 */
class BSIdetItem : public QGraphicsItem
{

public:


  /**
   * \brief Creates an initial detection analysis widget.
   */
  BSIdetItem (BSDetector *detector);

  /**
   * \brief Declares the image to be analysed.
   */
  void setImage (QImage *image, VMap *idata);

  /**
   * \brief Returns the widget size.
   * Nominally the image size.
   */
  QRectF boundingRect () const;

  /**
   * \brief Updates the widget display.
   */
  void paint (QPainter *painter,
              const QStyleOptionGraphicsItem *option, QWidget *widget);

  /**
   * \brief Returns the displayed information title.
   */
  inline QString itemTitle () const
  {
    if (displayItem == DISPLAY_INTENSITY)
      return ("Initial detection");
    else return ("No info");
  }

  /**
   * \brief Returns the detection view width.
   */
  inline int getWidth () const { return width; }

  /**
   * \brief Returns the detection view height.
   */
  inline int getHeight () const { return height; }

  /**
   * \brief Increments the current stripe index.
   * @param inc Direction (1 for rightwards, -1 for leftwards)
   */
  void incX (int inc);

  /**
   * \brief Increments the current stripe index.
   * @param inc Direction (1 for upwards, -1 for downwards)
   */
  void incY (int inc);

  /**
   * \brief Toggles the displayed information.
   * @param next Get next information if true, previous on otherwise.
   */
  void toggleDisplay (bool next);

  /**
   * \brief Sets the image scan area from the detection initial scan.
   */
  void buildScans ();


private:

  /** Available information : intensity idets. */
  static const int DISPLAY_INTENSITY;
  /** Number of the first information. */
  static const int DISPLAY_MIN;
  /** Number of the last information. */
  static const int DISPLAY_MAX;

  /** Analysis widget height. */
  int height;
  /** Analysis widget width. */
  int width;
  /** Stripe area margin width. */
  int margin;
  /** Stripe points zoom factor. */
  int resol;
  /** Stripe area horizontal offset. */
  int offx;
  /** Stripe area vertical offset. */
  int offy;

  /** Analysed image. */
  QImage *image;
  /* Analysed image width. */
  int imageWidth;
  /** Analyzed image height. */
  int imageHeight;
  /** Gradient map. */
  VMap *gMap;

  /** Central scan start point. */
  Pt2i pt1;
  /** Central scan start point. */
  Pt2i pt2;
  /** Directional scanner to analyse. */
  DirectionalScanner *ds;
  /** Central and left scans. */
  vector <vector <Pt2i> > leftscan;
  /** Right scans. */
  vector <vector <Pt2i> > rightscan;
  /** Minimum scan length allowed. */
  static const int MIN_SCAN;

  /** Displayed information (intensity, gradient, correlation). */
  int displayItem;
  /** Current stripe index. */
  int stripe;
  /** Min stripe index (right scan size). */
  int minStripe;
  /** Max stripe index (left scan size). */
  int maxStripe;

  /** Scanner provider. */
  ScannerProvider scanp;
  /** Reference to the associated detector. */
  BSDetector *det;


  /**
   * \brief Draws the scan strip.
   */
  void paintStripes (QPainter *painter);

};

#endif
