#include <QApplication>
#include <string>
#include <cmath>
#include "bswindow.h"
#include "bsrandomtester.h"
// #include "scanwindow.h"  // DEV


int main (int argc, char *argv[])
{
  int val = 0;
  int imageName = 0;
  bool random = false, testing = false;
  bool xt = false, yt = false;    // DEV
  bool out = false;
  QApplication app (argc, argv);

// DEV IN
/*
  if (argc == 2 && string (argv[1]) == string ("scans"))
  {
    ScanWindow sw (&val);
    sw.setWindowTitle ("Scans");
    sw.show ();
    return app.exec ();
  }
*/
// DEV OUT

  BSWindow window (&val);   // val : necessary argument !
  for (int i = 1; i < argc; i++)
  {
    if (string(argv[i]).at(0) == '-')
    {
      if (string(argv[i]) == string ("-random")) random = true;
      else if (string(argv[i]) == string ("-test")) testing = true;
// DEV IN
      else if (string(argv[i]) == string ("-own"))
      {
        new ExtLines (false, true);
        return (EXIT_SUCCESS);
      }
      else if (string(argv[i]) == string ("-york"))
      {
        new ExtLines (true, true);
        return (EXIT_SUCCESS);
      }
      else if (string(argv[i]) == string ("-altyork"))
      {
        new ExtLines (true, false);
        return (EXIT_SUCCESS);
      }
      else if (string(argv[i]) == string ("-xt")) { xt = true; yt = true; }
      else if (string(argv[i]) == string ("-yt")) yt = true;
      else if (string(argv[i]) == string ("-profile"))
        window.toggleProfWindow ();
      // else if (string(argv[i]) == string ("-acc"))
      //   window.toggleAccuWindow ();
      else if (string(argv[i]) == string ("-seg"))
        window.toggleSegWindow ();
      else if (string(argv[i]) == string ("-idet"))
        window.toggleIdetWindow ();
      // Test command : time ./Seg -test ../Images/couloir.jpg
      else if (string(argv[i]) == string ("-oud")) { out = true; xt = true; }
// DEV OUT
      else if (string(argv[i]) == string ("-out")) out = true;
      else if (string(argv[i]) == string ("-sobel3x3"))
        window.useGradient (VMap::TYPE_SOBEL_3X3);
      else if (string(argv[i]) == string ("-sobel5x5"))
        window.useGradient (VMap::TYPE_SOBEL_5X5);
      else if (string(argv[i]) == string ("-tophat"))
        window.useGradient (VMap::TYPE_TOP_HAT);
      else if (string(argv[i]) == string ("-blackhat"))
        window.useGradient (VMap::TYPE_BLACK_HAT);
      else if (string(argv[i]) == string ("-morpho"))
        window.useGradient (VMap::TYPE_MORPHO);
      else if (string(argv[i]) == string ("-fulltophat"))
        window.useGradient (VMap::TYPE_FULL_TOP_HAT);
      else if (string(argv[i]) == string ("-fullblackhat"))
        window.useGradient (VMap::TYPE_FULL_BLACK_HAT);
      else if (string(argv[i]) == string ("-fullmorpho"))
        window.useGradient (VMap::TYPE_FULL_MORPHO);
// DEV IN
      else
      {
        int l = string (argv[i]).length ();
        for (int j = 1; j < l; j++)
        {
          char carac = string(argv[i]).at(j);
          if (carac == 'p') window.toggleProfWindow ();
          // else if (carac == 'a') window.toggleAccuWindow ();
          else if (carac == 's') window.toggleSegWindow ();
          else if (carac == 'i') window.toggleIdetWindow ();
        }
      }
// DEV OUT
    }
    else imageName = i;
  }
  if (random)
  {
    BSRandomTester *tester = new BSRandomTester ();
    tester->randomTest ();
    delete tester;
    return (EXIT_SUCCESS);
  }
  else if (out)
  {
    QImage im;
    if (imageName != 0) im.load (argv[imageName]);
    else im.load ("Images/couloir.gif");
    int width = im.width ();
    int height = im.height ();
    int **tabImage = new int*[height];
    for (int i = 0; i < height; i++)
    { 
      tabImage[i] = new int[width];
      for(int j = 0; j < width; j++)
      {
        QColor c = QColor (im.pixel (j, height - i - 1));
        tabImage[i][j] = c.value ();
      }
    }
    BSDetector detector;
    AbsRat x1, y1, x2, y2;
    VMap *gMap = NULL;
    if (gMap != NULL) delete gMap;
    gMap = new VMap (width, height, tabImage, VMap::TYPE_SOBEL_5X5);
    detector.setGradientMap (gMap);
    if (xt) detector.setStaticDetector (true);     // DEV
    // buildGradientImage (0);
    detector.detectAll ();
    ofstream outf ("naivelines.txt", ios::out);
    vector<BlurredSegment *> bss = detector.getBlurredSegments ();
    vector<BlurredSegment *>::iterator it = bss.begin ();
    while (it != bss.end ())
    {
      if (*it != NULL)
      {
        DigitalStraightSegment *dss = (*it)->getSegment ();
        if (dss != NULL)
        {
          dss->naiveLine (x1, y1, x2, y2);
          AbsRat th = dss->squaredEuclideanThickness ();
          outf << (x1.num () / (double) x1.den ()) << " "
               << (height - 1 - y1.num () / (double) y1.den ()) << " "
               << (x2.num () / (double) x2.den ()) << " "
               << (height - 1 - y2.num () / (double) y2.den ()) << " "
               << sqrt (th.num () / (double) th.den ()) << endl;
        }
      }
      it ++;
    }
    outf.close ();
    return (EXIT_SUCCESS);
  }
// DEV IN
  else if (yt)
  {
    int repetitions = 100;
    QImage im;
    im.load ("test.jpg");
    cout << "Time measure for " << repetitions
         << " FBSD lines extractions" << endl;
    int width = im.width ();
    int height = im.height ();
    int **tabImage = new int*[height];
    for (int i = 0; i < height; i++)
    { 
      tabImage[i] = new int[width];
      for(int j = 0; j < width; j++)
      {
        QColor c = QColor (im.pixel (j, height - i - 1));
        tabImage[i][j] = c.value ();
      }
    }
    BSDetector detector;
    clock_t start = clock ();
    VMap *gMap = NULL;
    for (int i = 0; i < repetitions; i++)
    {
      if (gMap != NULL) delete gMap;
      gMap = new VMap (width, height, tabImage, VMap::TYPE_SOBEL_5X5);
      detector.setGradientMap (gMap);
      if (xt) detector.setStaticDetector (true);
      // buildGradientImage (0);
      detector.detectAll ();
    }
    double diff = (clock () - start) / (double) CLOCKS_PER_SEC;
    ofstream outf ("fbsdperf.txt", ios::out);
    outf << diff << endl;
    outf.close ();
    return (EXIT_SUCCESS);
  }
// DEV OUT
  else
  {
    if (imageName != 0) window.setFile (QString (argv[imageName]));
    else window.setFile (QString ("Images/couloir.gif"));
    if (testing)
    {
      window.runTest ();
      return (EXIT_SUCCESS);
    }
    window.runOptions (); 
    window.show ();
    return app.exec ();
  }
}
