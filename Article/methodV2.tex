\section{The detection method}

\label{sec:method}

In this line detection method, only the gradient information is processed
as it provides a good information on the image dynamics, and hence the
presence of edges.
Trials to use the intensity signal were also made through costly correlation
techniques, but they were mostly successful for detecting shapes with a
stable appearance such as metallic tubular objects \cite{AubryAl17}.
Contrarily to most detectors, no edge map is built here, but gradient
magnitude and orientation are examined in privileged directions to track
edge traces.
In particular, we use a Sobel operator with a 5x5 pixels mask
to get high quality gradient information \cite{KekreGharge10}.

\subsection{Previous work}

In a former paper \cite{KerautretEven09}, an efficient tool to detect
blurred segments of fixed thickness in gray-level images was already
introduced.
It was based on a first rough detection in a local image area
defined by the user. At that stage, the goal was to disclose the presence
of a straight edge. Therefore as simple a test as the gradient maximal value
was performed.
In case of success, refinement steps were then run through an exploration of
the image in the direction of the detected edge.
In order to prevent local disturbances such as the presence of a sharper
edge nearby, all the local gradient maxima were successively tested
until a correct candidate with an acceptable gradient orientation was found.

Despite of a good behavior reported, several drawbacks remained.
First, the blurred segment thickness was not measured but initially set by the
user according to application requirements. The produced information
on edge quality was rather poor, and especially when the edge is thin,
the risk to incorporate outlier points was quite high, thus producing a
biased estimation of the edge orientation.
Then, two refinement steps were systematically performed.
On the one hand, this is useless when the first detection is successfull.
On the other hand, there is no guarantee that this approach is able to
process larger images.
The search direction relies on the support vector of the blurred segment
detected at former step.
Because the numerization rounding fixes a limit on this estimated orientation
accuracy, more steps are inevitably required to process larger images.
In the following, we present the improvements in the new detector to
overcome these limitations.

\subsection{Workflow of the new detection process}

The workflow of the detection process is summerized in the following figure.

\begin{figure}[h]
\center
  \input{Fig_method/workflow}
  \caption{The main workflow of the detection process.}
  \label{fig:workflow}
\end{figure}

The initial detection consists in building and extending a blurred segment
$\mathcal{B}$ of assigned thickness $\varepsilon_0$,
based on points with highest gradient magnitude found in each
scan of a static directional scan defined by an input segment $AB$.
The extension is stopped after five point addition failures on each side.
Notice that the gradient direction is not used in this step.

Validity tests are then applied to decide of the detection pursuit.
They aim at rejecting too small segments (less than 4 points)
or too sparse ones (more than 50\% of point addition failures)
or also those with a close orientation to $AB$ (less than $\pi/6$).

In the fine tracking step, another blurred segment $\mathcal{B}'$ is built
and extended with points that correspond to local maxima of the
image gradient, ranked by magnitude order, and with gradient direction
close to start point gradient direction (less than $\pi/6$).
At this refinement step, a {\it control of assigned thickness} is applied
and an {\it adaptive directional scan} based on found position $C$ and
direction $\vec{D}$ is used in order to extend the segment in
appropriate direction. These two notions are described in
following sections \ref{subsec:ads} and \ref{subsec:caw}.

Output segment $\mathcal{B}'$ is finally accepted based on application criteria.
Final length and sparsity thresholds can be set accordingly.
They are the only parameters of this local detector, together with the input
assigned thickness $\varepsilon_0$.
%Too short, too sparse or too fragmented segments
%can be rejected. Length, sparsity or fragmentation thresholds are
%intuitive parameters left at the end user disposal.
%None of these tests are activated for the experimental stage in order
%to put forward achievable performance.

\subsection{Adaptive directional scan}
\label{subsec:ads}

The blurred segment is searched within a directional scan with position
and orientation approximately drawn by the user, or blindly defined
in unsupervised mode.
In most cases, the detection stops where the segment escapes sideways
from the scan strip (\RefFig{fig:escape} a).
A second search is then run using another directional scan aligned
on the detected segment (\RefFig{fig:escape} b).
In the given example, an outlier added to the initial segment leads to a
wrong orientation value.
But even in case of a correct detection, this estimated orientation is
subject to the numerization rounding, and the longer the real segment is,
the higher the probability gets to fail again on an escape from the scan strip.

\begin{figure}[h]
\center
  \begin{tabular}{c@{\hspace{0.2cm}}c@{\hspace{0.2cm}}c}
    \includegraphics[width=0.24\textwidth]{Fig_notions/escapeLightFirst_half.png} &
    \includegraphics[width=0.24\textwidth]{Fig_notions/escapeLightSecond_half.png} &
    \includegraphics[width=0.48\textwidth]{Fig_notions/escapeLightThird_zoom.png}
    \begin{picture}(1,1)(0,0)
      {\color{dwhite}{
        \put(-307,4.5){\circle*{8}}
        \put(-216,4.5){\circle*{8}}
        \put(-127,4.5){\circle*{8}}
      }}
      \put(-309.5,2){a}
      \put(-219,1.5){b}
      \put(-129.5,2){c}
    \end{picture}
  \end{tabular}
  \caption{Aborted detections on side escapes of static directional scans
           and successful detection using an adaptive directional scan.
           The last points added to the left of the blurred segment during
           initial detection (a) lead to a bad estimation of its
           orientation, and thus to an incomplete fine tracking with a
           classical directional scan (b). An adaptive directional scan
           instead of the static one allows to continue the segment
           expansion as far as necessary (c).
           Input selection is drawn in red color, scan strip bounds
           in blue and detected blurred segments in green.}
  \label{fig:escape}
\end{figure}

To overcome this issue, in the former work, an additional refinement step was
run in the direction estimated from this longer segment.
It was enough to completely detect most of the tested edges, but certainly
not all, especially if big size images with much longer edges were processed.
As a solution, this operation could be iterated as long as the blurred segment
escapes from the directional scan using as any fine detection steps as
necessary.
But at each iteration, already tested points are processed again,
thus producing a useless computational cost.

Here the proposed solution is to dynamically align the scan direction on
the blurred segment all along the expansion stage.
At each iteration $i$ of the expansion, the scan strip is aligned on the
direction of the blurred segment $\mathcal{B}_{i-1}$ computed at previous
iteration $i-1$.
More formally, an {\it adaptive directional scan} $ADS$ is defined by:
\begin{equation}
ADS = \left\{
S_i = \mathcal{D}_i \cap \mathcal{N}_i \cap \mathcal{I}
\left| \begin{array}{l}
\vec{V}(\mathcal{N}_i) \cdot \vec{V}(\mathcal{D}_0) = 0 \\
\wedge~ h(\mathcal{N}_i) = h(\mathcal{N}_{i-1}) + p(\mathcal{D}_0) \\
%\wedge~ \mathcal{D}_{i} = \mathcal{D} (C_{i-1}, \vec{D}_{i-1}, w_{i-1}),
\wedge~ \mathcal{D}_{i} = \mathcal{D}^{C_{i-1}, \vec{D}_{i-1}, \mu_{i-1}},
i > \lambda
\end{array} \right. \right\}
\end{equation}
where $C_{i}$, $\vec{D}_{i}$ and $\mu_{i}$ are respectively a position,
a director vector and a thickness observed at iteration $i$,
used to update the scan strip and lines in accordance to \RefEq{eq:dsdef2}.
%In the scope of the present detector,
The last clause expresses the update of the scan bounds at iteration $i$:
$C_{i-1}$, $\vec{D}_{i-1}$ and $\mu_{i-1}$ are respectively the intersection
of the input selection and the central line of $\mathcal{B}_{i-1}$,
the director vector of the optimal line of $\mathcal{B}_{i-1}$,
and the thickness of $\mathcal{B}_{i-1}$.
$\lambda$ is a delay which is set to 20 iterations to avoid direction instabilities
when too few points are inserted.
Compared to static directional scans where the scan strip remains fixed to
the initial line $\mathcal{D}_0$, here the scan strip moves while
scan lines remain fixed.
This behavior ensures a complete detection of the blurred segment even when
the orientation of $\mathcal{D}_0$ is wrongly estimated (\RefFig{fig:escape} c).

\subsection{Control of assigned thickness}
\label{subsec:caw}

The assigned thickess $\varepsilon$ to the blurred segment recognition
algorithm is initially set to a large value $\varepsilon_0$ in order to
allow the detection of large blurred segments.
Then, when no more augmentation of the blurred segment thickness is observed
after $\tau$ iterations ($\mu_{i+\tau} = \mu_i$), it is set to a much
stricter value able to circumscribe the possible interpretations of the
segment, that take into account the digitization margins:
\begin{equation}
\varepsilon = \mu_{i+\tau} + \frac{\textstyle 1}{\textstyle 2}
\end{equation}
This strategy aims at preventing the incorporation of spurious outliers in
further parts of the segment.
Setting the observation distance to a constant value $\tau = 20$ seems
appropriate in most experimented situations.

\subsection{Supervised blurred segments detection}

In supervised context, the user draws an input stroke across the specific
edge that he wants to extract from the image.
The detection method previously described is continuously run during mouse
dragging and the output blurred segment is displayed on-the-fly.

%The method is quite sensitive to the local conditions of the initial detection
%so that the output blurred segment may be quite unstable.
%In order to temper this undesirable behavior for interactive applications,
%the initial detection can be optionally run twice, the second fast scan being
%aligned on the first detection output.
%This strategy provides a first quick analysis of the local context before
%extracting the segment and contributes to notably stabilize the overall
%process.
%
%When selecting candidates for the fine detection stage, an option, called
%{\it edge selection mode}, is left to also filter the points according to
%their gradient direction.
%In {\it main edge selection mode}, only the points with a gradient vector
%in the same direction as the start point gradient vector are added to the
%blurred segment.
%In {\it opposite edge selection mode}, only the points with an opposite
%gradient vector direction are kept.
%In {\it line selection mode} this direction-based filter is not applied,
%and all the candidate points are aggregated into a same blurred segment,
%whatever the direction of their gradient vector.
%As illustrated on \RefFig{fig:edgeDir}, this mode allows the detection of
%the two opposite edges of a thin straight object.
%
%\begin{figure}[h]
%\center
%  \begin{tabular}{c@{\hspace{0.2cm}}c}
%    \includegraphics[width=0.4\textwidth]{Fig_method/selectLine_zoom.png} &
%    \includegraphics[width=0.4\textwidth]{Fig_method/selectEdges_zoom.png}
%  \end{tabular}
%  \begin{picture}(1,1)(0,0)
%    {\color{dwhite}{
%      \put(-220,-14.5){\circle*{8}}
%      \put(-74,-14.5){\circle*{8}}
%    }}
%    \put(-222.5,-17){a}
%    \put(-76.5,-17){b}
%  \end{picture}
%  \caption{Blurred segments obtained in \textit{line} or \textit{edge
%           selection mode} as a result of the gradient direction filtering
%           when adding points.
%           In \textit{line selection mode} (a), a thick blurred segment is
%           built and extended all along the brick join.
%           In \textit{edge selection mode} (b), a thin blurred segment is
%           built along one of the two join edges.
%           Both join edges are detected with the \textit{multi-selection}
%           option.
%           On that very textured image, they are much shorter than the whole
%           join detected in line selection mode.
%           Blurred segment points are drawn in black color, and the enclosing
%           straight segments in blue.}
%  \label{fig:edgeDir}
%\end{figure}

%\subsection{Multiple blurred segments detection}

An option, called {\it multi-detection} (Algorithm 1), allows the
detection of all the segments crossed by the input stroke $AB$.
In order to avoid multiple detections of the same edge, an occupancy mask,
initially empty, collects the dilated points of all the blurred segments,
so that these points can not be used any more.
\input{Fig_method/algoMulti}

First the positions $M_j$ of the prominent local maxima of the gradient
magnitude found under the stroke are sorted from the highest to the lowest.
For each of them the main detection process is run with three modifications:
\begin{enumerate}
\item the initial detection takes $M_j$ and the orthogonal direction
$\vec{AB}_\perp$ to the stroke as input to build a static scan of fixed
thickness $2\cdot\varepsilon_0$, and $M_j$ is used as start point of the
blurred segment;
\item the occupancy mask is filled in with the points of the dilated blurred
segments $\mathcal{B}_j'$ at the end of each successful detection
(a $5 \times 5$ octogonal neighborhood region of 21 pixels is used);
\item points marked as occupied are rejected when selecting candidates for the
blurred segment extension in the fine tracking step.
\end{enumerate}

%In edge selection mode (\RefFig{fig:edgeDir} b), the multi-detection
%algorithm is executed twice, first in main edge selection mode, then
%in opposite edge selection mode.

\subsection{Automatic blurred segment detection}

An unsupervised mode is also proposed to automatically detect all the
straight lines in the image. A stroke that crosses the whole image, is
swept in both directions, vertical then horizontal, from the center to
the borders. At each position, the multi-detection algorithm is run
to collect all the segments found under the stroke.
Then small blurred segments are rejected in order to avoid the formation
of misaligned segments when the sweeping stroke crosses an image edge
near one of its ends.
In such situation, any nearby disturbing gradient is likely to deviate
the blurred segment direction, and its expansion is quickly stopped.
The stroke sweeping step is an additional parameter for automatic detections,
that could be set in relation to the final length threshold parameter.

The automatic detection of blurred segments in a whole image is available
for testing from the online demonstration
and from a \textit{GitHub} source code repository: \\
\href{https://github.com/evenp/FBSD}{
\small{\url{https://github.com/evenp/FBSD}}}

%\input{Fig_method/algoAuto}

%The behavior of the unsupervised detection is depicted through the two
%examples of \RefFig{fig:auto}.
%The example on the left shows the detection of thin straight objects on a
%circle with variable width.
%On the left half of the circumference, the distance between both edges
%exceeds the initial assigned width and a thick blurred segment is build
%for each of them. Of course, on a curve, a continuous thickenning is
%observed untill the blurred segment minimal width reaches the initial
%assigned width.
%On the right half, both edges are encompassed in a common blurred segment,
%and at the extreme right part of the circle, the few distant residual points
%are grouped to form a thick segment.
%
%The example on the right shows the limits of the edge detector on a picture
%with quite dense repartition of gradient.
%All the salient edges are well detected but they are surrounded by a lot
%of false detections, that rely on the presence of many local maxima of
%the gradient magnitude with similar orientations.
%
%\begin{figure}[h]
%\center
%  \begin{tabular}{c@{\hspace{0.2cm}}c}
%    \includegraphics[width=0.37\textwidth]{Fig_method/vcercleAuto.png} &
%    \includegraphics[width=0.58\textwidth]{Fig_method/plafondAuto.png}
%  \end{tabular}
%  \caption{Automatic detection of blurred segments.}
%  \label{fig:auto}
%\end{figure}
