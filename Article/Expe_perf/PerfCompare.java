import java.io.*;

public class PerfCompare
{
  private static String[] names = {
    "perf_iut_amphi1.txt",
    "perf_iut_amphi2.txt",
    "perf_iut_corridor1.txt",
    "perf_iut_corridor2.txt",
    "perf_iut_entrance.txt",
    "perf_iut_labo1.txt",
    "perf_iut_labo2.txt",
    "perf_iut_library1.txt",
    "perf_iut_mul1.txt",
    "perf_iut_mul2.txt",
    "perf_iut_office1.txt",
    "perf_iut_office2.txt",
    "perf_iut_office4.txt",
    "perf_iut_outdoor1.txt",
    "perf_iut_outdoor2.txt",
    "perf_iut_outdoor3.txt",
    "perf_iut_scc1.txt",
    "perf_iut_scc2.txt",
    "perf_iut_scc3.txt",
    "perf_loria_indoor1.txt",
    "perf_loria_indoor2.txt",
    "perf_loria_indoor3.txt",
    "perf_loria_office1.txt",
    "perf_loria_outdoor1.txt",
    "perf_vosges_castle1.txt",
    "perf_vosges_castle2.txt",
    "perf_vosges_corridor1.txt",
    "perf_vosges_detail1.txt",
    "perf_vosges_detail2.txt",
    "perf_vosges_detail3.txt"};


  private double timeRatio = 0.;
  private double countRatio = 0.;
  private double longRatio = 0.;
  private double lengthRatio = 0.;
  private double widthRatio = 0.;

  public static void main (String[] args) throws IOException
  {
    PerfCompare[] perf = new PerfCompare[names.length];
    double meanTime = 0.;
    double meanCount = 0.;
    double meanLong = 0.;
    double meanLength = 0.;
    double meanWidth = 0.;

    for (int i = 0; i < names.length; i++)
    {
      perf[i] = new PerfCompare (names[i]);
      meanTime += perf[i].timeRatio;
      meanCount += perf[i].countRatio;
      meanLong += perf[i].longRatio;
      meanLength += perf[i].lengthRatio;
      meanWidth += perf[i].widthRatio;
    }
    meanTime /= names.length;
    meanCount /= names.length;
    meanLong /= names.length;
    meanLength /= names.length;
    meanWidth /= names.length;

    double sdTime = 0.;
    double sdCount = 0.;
    double sdLong = 0.;
    double sdLength = 0.;
    double sdWidth = 0.;
    for (int i = 0; i < names.length; i++)
    {
      sdTime += (perf[i].timeRatio - meanTime)
                * (perf[i].timeRatio - meanTime);
      sdCount += (perf[i].countRatio - meanCount)
                 * (perf[i].countRatio - meanCount);
      sdLong += (perf[i].longRatio - meanLong)
                * (perf[i].longRatio - meanLong);
      sdLength += (perf[i].lengthRatio - meanLength)
                  * (perf[i].lengthRatio - meanLength);
      sdWidth += (perf[i].widthRatio - meanWidth)
                 * (perf[i].widthRatio - meanWidth);
    }
    sdTime = Math.sqrt (sdTime / names.length);
    sdCount = Math.sqrt (sdCount / names.length);
    sdLong = Math.sqrt (sdLong / names.length);
    sdLength = Math.sqrt (sdLength / names.length);
    sdWidth = Math.sqrt (sdWidth / names.length);

    System.out.println ("Time    : " + meanTime + " (" + sdTime + ")");
    System.out.println ("Count   : " + meanCount + " (" + sdCount + ")");
    System.out.println ("Long    : " + meanLong + " (" + sdLong + ")");
    System.out.println ("Lendth  : " + meanLength + " (" + sdLength + ")");
    System.out.println ("Width   : " + meanWidth + " (" + sdWidth + ")");
  }

  public PerfCompare (String name) throws IOException
  {
    BufferedReader flot = new BufferedReader (new FileReader (name));
    String val = flot.readLine ();  // nb runs
    val = flot.readLine ();  // width
    val = flot.readLine ();  // height
    val = flot.readLine ();   // time
    double t1 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // trials
    val = flot.readLine ();   // count of segments
    int ns1 = (new Integer (val)).intValue ();
    val = flot.readLine ();   // count of long segments
    int nl1 = (new Integer (val)).intValue ();
    val = flot.readLine ();   // mean length
    double l1 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // mean width
    double w1 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // mean width of long segments
    val = flot.readLine ();   // time
    double t2 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // trials
    val = flot.readLine ();   // count of segments
    int ns2 = (new Integer (val)).intValue ();
    val = flot.readLine ();   // count of long segments
    int nl2 = (new Integer (val)).intValue ();
    val = flot.readLine ();   // mean length
    double l2 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // mean width
    double w2 = (new Double (val)).doubleValue ();
    val = flot.readLine ();   // mean width of long segments
    flot.close ();

    timeRatio = t1 / t2;
    countRatio = ns1 / (double) ns2;
    longRatio = nl1 / (double) nl2;
    lengthRatio = l1 / l2;
    widthRatio = w1 / w2;
  }
}
