\section{Introduction}

\label{sec:intro}

\subsection{Motivations}

Straight line detection is a preliminary step of many image analysis
processes.
Therefore, it is always an active research topic centered on
the quest of still faster, more accurate or more robust-to-noise methods
\cite{AkinlarTopal12,GioiAl10,LuAl15,MatasAl00}.
However, they seldom provide an exploitable measure of the output line
quality, based on intrinsic properties such as sharpness, connectivity
or scattering.
%Some information may sometimes be drawn from their specific context,
%for example through an analysis of the peak in a Hough transform accumulator.

Digital geometry is a research field where new mathematical definitions
of quite classical geometric objects, such as lines or circles, are introduced
to better fit to the discrete nature of most of todays data to process.
In particular, the notion of blurred segment \cite{Buzer07,DebledAl05} was
introduced to cope with the image noise or other sources of imperfections
from the real world by the mean of a width parameter.
Efficient algorithms have already been designed to recognize
these digital objects in binary images \cite{DebledAl06}.
Straight edges are rich visual features for 3D scene reconstruction from 2D
images.
A blurred segment seems well suited to reflect the required edge quality
information.
Its preimage,
i.e. the space of geometric entities which numerization matches this
blurred segment, may be used to compute some confidence level in the delivered
3D interpretations, as a promising extension of former works
on discrete epipolar geometry \cite{NatsumiAl08}.

The present work aims at designing a flexible tool to detect blurred segments
with optimal width and orientation in gray-level images for as well
supervised as unsupervised contexts.
User-friendly solutions are sought, with ideally no parameter to set,
or at least quite few values with intuitive meaning.

\subsection{Previous work}

In a former paper \cite{KerautretEven09}, an efficient tool to detect
blurred segments of fixed width in gray-level images was already introduced.
It is based on a first rough detection in a local image area
defined by the user. The goal is to disclose the presence of a straight edge.
Therefore as simple a test as the gradient maximal value is performed.
In case of success, refinement steps are run through an exploration of
the image in the direction of the detected edge.
In order to prevent local disturbances such as the presence of a sharper
edge nearby, all the local gradient maxima are successively tested
untill a correct candidate with an acceptable gradient orientation is found.

Only the gradient information is processed as it provides a good information
on the image dynamics, and hence the presence of edges.
Trials to also use the intensity signal were made through costly correlation
techniques, but they were mostly successful for detecting shapes with a
stable appearance such as metallic tubular objects \cite{AubryAl17}.

Despite of good performances achieved, several drawbacks remain.
First, the blurred segment width is not measured but initially set by the
user according to the application requirements. The produced information
on the edge quality is rather poor, and especially when the edge is thin,
the risk to incorporate outlier points is quite high, thus producing a
biased estimation of the edge orientation.
Then, two refinement steps are systematically performed.
On the one hand, this is useless when the first detection is successfull.
On the other hand, there is no guarantee that this approach is able to
process larger images.
The search direction relies on the support vector of the blurred segment
detected at the former step.
Because the numerization rounding fixes a limit on this estimated orientation
accuracy, more steps are inevitably necessary to process larger images.

\subsection{Main contributions}

The present work aims at solving both former mentioned
drawbacks through two main contributions:
(i) the concept of {\bf adaptive directional scan} designed to get some
compliance to the unpredictable orientation problem;
(ii) the {\bf control of the assigned width} to the blurred segment
recognition algorithm, intended to derive more reliable information on the
edge orientation and quality.
As a side effect, these two major evolutions also led to a noticeable
improvement of the time performance of the detector.
They are also put forward within a global line extraction algorithm
which can be evaluated through an online demonstration at :
\href{http://ipol-geometry.loria.fr/~kerautre/ipol_demo/FBSD_IPOLDemo}{
\small{\url{http://ipol-geometry.loria.fr/~kerautre/ipol_demo/FBSD_IPOLDemo}}}

In the next section, the main theoretical notions are introduced.
The new detector workflow, the adaptive directional scan, the control
of the assigned width and their integration into both supervised and
unsupervised contexts are then presented in \RefSec{sec:method}.
Experiments led to assess the expected increase of performance are decribed
in \RefSec{sec:expe}.
Finally, achieved results are summarized in \RefSec{sec:conclusion},
followed by some open perspectives for future works.
