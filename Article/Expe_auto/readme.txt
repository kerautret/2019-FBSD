Seg ../buro.png
20 y pour obtenir un niveau de fond d'ecran a 100
m pour une detection m
! pour un affichage sombre
m pour une detection multiple avec le nouveau detecteur
p pour recuperer la capture (capture.png -> coloredNew.png)
6 pour une detection multiple avec l'ancien detecteur
p pour recuperer la capture (capture.png -> coloredOld.png)
= pour remplacer les couleurs aleatoires par du noir
p pour recuperer la capture (capture.png -> bsOld.png)
6 pour une detection multiple avec le nouveau detecteur
p pour recuperer la capture (capture.png -> bsNew.png)
Ctrl y - Ctrl u pour afficher les segments englobants
p pour recuperer la capture (capture.png -> dssNew.png)
6 pour une detection multiple avec l'ancien detecteur
p pour recuperer la capture (capture.png -> dssOld.png)
8 pour un test de performance (perf.txt -> perf_buro.txt)
cp dssNew.png dssDetailNew.png
Selectionner la zone (544, 512) (742, 448)
cp dssOld.png dssDetailOld.png
Selectionner la zone (544, 512) (742, 448)

Contenu de perf_buro.txt
- Nombre de detections multiples
- Largeur d'image
- Hauteur d'image
puis avec le nouveau detecteur
- Temps d'execution
- Nombre de minimum locaux testes
- Nombre de segments flous detectes
- Nombre de segments d'extension > 40 pixels
- Extension moyenne des segments flous detectes
- Largeur moyenne des segments flous detectes
- Largeur moyenne des segments flous longs (> 40 pixels) detectes
puis pareil avec l'ancien detecteur

