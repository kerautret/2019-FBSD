#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <ctime>

#include "CannyLine.h"

using namespace std;
using namespace cv;
int main(int argc, char *argv[])
{
    
    string fileCur = "test.jpg";
    string outname = "cannylines.txt";
    if (argc >= 2) fileCur = string(argv[1]);
    cv::Mat img = imread( fileCur, 0 );

    CannyLine detector;
    std::vector<std::vector<float> > lines;

    detector.cannyLine( img, lines );

    ofstream outf (outname, ios::out);
    std::vector<std::vector<float> >::iterator it = lines.begin ();
    while (it != lines.end ())
    {
      std::vector<float>::iterator flit = it->begin ();
      while (flit != it->end ()) outf << "  " << *flit++;
      outf << endl;
      it++;
    }
    outf.close ();
    
    // show
/*
    cv::Mat imgShow( img.rows, img.cols, CV_8UC3, cv::Scalar( 255, 255, 255 ) );
    for ( int m=0; m<lines.size(); ++m )
    {
        cv::line( imgShow, cv::Point( lines[m][0], lines[m][1] ), cv::Point( lines[m][2], lines[m][3] ), cv::Scalar(0,0,0), 1, 1 );
    }
    imshow("Canny dejection",imgShow);
    imwrite("result.jpg", imgShow);
    
    cv::waitKey(0);
*/

    
    return 0;
}
