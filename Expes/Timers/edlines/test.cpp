#include "EDLib.h"
#include <iostream>
#include <fstream>
#include <ctime>

using namespace cv;
using namespace std;

int main(int argc, char ** argv)
{
  int repetitions = 100;
  if (argc > 1)
  {
    string ch (argv[1]);
    istringstream entree (ch);
    entree >> repetitions;
  }
  cout << "Time measure for " << repetitions
       << " ED-lines extractions" << endl;

  //***************************** ED Edge Segment Detection *****************************
  //Detection of edge segments from an input image	
  Mat testImg = imread("test.jpg", 0);

  //Call ED constructor
  EDLines testEDLines;
  ED testED;
  clock_t start = clock ();
  for (int i = 0; i < repetitions; i++)
  {
    testED = ED(testImg, SOBEL_OPERATOR, 36, 8, 1, 10, 1.0, true); // apply ED algorithm
	
    //Show resulting edge image
    Mat edgeImg = testED.getEdgeImage();
	
    //Get edges in segment form (getSortedSegments() gives segments sorted w.r.t. legnths) 
    //std::vector< std::vector<Point> > segments = testED.getSegments();
       
    //***************************** EDLINES Line Segment Detection *****************************
    //Detection of line segments from the same image
/* Philippe */
    testEDLines = EDLines(testImg);
  }
  double diff = (clock () - start) / (double) CLOCKS_PER_SEC;
  FILE *pf = NULL;
  if ((pf = fopen ("edperf.txt", "w")) == NULL)
  {
    fprintf (stderr, "Impossible d'ecrire dans edperf.txt\n");
    return (0);
  }
  fprintf (pf, "%lf\n", diff);
  fclose (pf);
/* Philippe */

  //Mat lineImg = testEDLines.getLineImage();	//draws on an empty image
  //imshow("Line Image 1 - PRESS ANY KEY TO CONTINUE", lineImg);

  //Detection of lines segments from edge segments instead of input image
  //Therefore, redundant detection of edge segmens can be avoided
//  testEDLines = EDLines(testED);
  //lineImg = testEDLines.drawOnImage();	//draws on the input image
  //imshow("Line Image 2  - PRESS ANY KEY TO CONTINUE", lineImg);
  
  //Acquiring line information, i.e. start & end points
//  vector<LS> lines = testEDLines.getLines();
  //int noLines = testEDLines.getLinesNo();
  //std::cout << "Number of line segments: " << noLines << std::endl;
  
  //waitKey();
  return 0;
}
