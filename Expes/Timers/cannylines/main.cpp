#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <stdio.h>
#include <stdlib.h>
#include <ctime>

#include "CannyLine.h"

using namespace std;
using namespace cv;
int main(int argc, char *argv[])
{
    int repetitions = 100;
    if (argc == 2)
    {
      string ch (argv[1]);
      istringstream entree (ch);
      entree >> repetitions;
    }
    cout << "Time measure for " << repetitions
         << " Canny lines extractions" << endl;
    cv::Mat img = imread( "test.jpg", 0 );
    std::vector<std::vector<float> > lines;
    clock_t start = clock ();
    for (int i = 0; i < repetitions; i++)       // REPETITIONS
    {
      CannyLine detector;
      detector.cannyLine (img, lines);
      lines.empty ();
    }
    double diff = (clock () - start) / (double) CLOCKS_PER_SEC;
    ofstream outf ("cannyperf.txt", ios::out);
    outf << diff << endl;
    outf.close ();
    return (0);
}
