// Compilation : cc -o meanlength meanlength.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define OUTFILE "meanlength.txt"
#define IMAGES "images.txt"
#define MIN_LENGTH 10
#define NBMAX 200
#define LSD_NAME "lsd"
#define ED_NAME "ed"
#define CANNY_NAME "canny"
#define NAIVE_NAME "naive"
#define NB_LSD 7
#define NB_ED 4
#define NB_CANNY 5
#define NB_NAIVE 5


int main (int argc, char *argv[])
{
  int w, h;
  double sx, sy, ex, ey, cmp, longueur;
  int lsdnb[NBMAX];
  double lsdml[NBMAX];
  double lsdcuml[NBMAX];
  int ednb[NBMAX];
  double edml[NBMAX];
  double edcuml[NBMAX];
  int cannynb[NBMAX];
  double cannyml[NBMAX];
  double cannycuml[NBMAX];
  int naivenb[NBMAX];
  double naiveml[NBMAX];
  double naivecuml[NBMAX];
  double lsdmcuml, lsdmnb = 0., lsdmml = 0.;
  double lsdscuml = 0., lsdsnb = 0., lsdsml = 0.;
  double lsdmin = 50.;
  double edmcuml = 0., edmnb = 0., edmml = 0.;
  double edscuml = 0., edsnb = 0., edsml = 0.;
  double edmin = 50.;
  double cannymcuml = 0., cannymnb = 0., cannymml = 0.;
  double cannyscuml = 0., cannysnb = 0., cannysml = 0.;
  double cannymin = 50.;
  double naivemcuml = 0., naivemnb = 0., naivemml = 0.;
  double naivescuml = 0., naivesnb = 0., naivesml = 0.;
  int naivesmall = 0;
  int nbim = 0;
  char val[NBMAX];
  char *inames[NBMAX];
  int nb[4];
  double cuml[4];
  FILE *pf = NULL;

  if ((pf = fopen (IMAGES, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", IMAGES);
    return (0);
  }
  fscanf (pf, "%d", &w);
  fscanf (pf, "%d", &h);
  while (fscanf (pf, "%s", val) != -1)
  {
    inames[nbim] = (char *) malloc (strlen (val) * sizeof (char) + 1);
    strcpy (inames[nbim++], val);
  }
  fclose (pf);

  for (int i = 0; i < nbim; i++)
  {
    strcpy (val, LSD_NAME);
    strcat (val, "lines/");
    strcat (val, LSD_NAME);
    strcat (val, "lines");
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    lsdnb[i] = 0;
    lsdcuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_LSD; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        lsdcuml[i] += longueur;
        lsdnb[i] ++;
      }
      if (longueur < lsdmin) lsdmin = longueur;
    }
    fclose (pf);
    lsdml[i] = lsdcuml[i] / lsdnb[i];

    strcpy (val, ED_NAME);
    strcat (val, "lines/");
    strcat (val, ED_NAME);
    strcat (val, "lines");
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    ednb[i] = 0;
    edcuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_ED; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        edcuml[i] += longueur;
        ednb[i] ++;
      }
      if (longueur < edmin) edmin = longueur;
    }
    fclose (pf);
    edml[i] = edcuml[i] / ednb[i];

    strcpy (val, CANNY_NAME);
    strcat (val, "lines/");
    strcat (val, CANNY_NAME);
    strcat (val, "lines");
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    cannynb[i] = 0;
    cannycuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_CANNY; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        cannycuml[i] += longueur;
        cannynb[i] ++;
      }
      if (longueur < cannymin) cannymin = longueur;
    }
    fclose (pf);
    cannyml[i] = cannycuml[i] / cannynb[i];

    strcpy (val, NAIVE_NAME);
    strcat (val, "lines/");
    strcat (val, NAIVE_NAME);
    strcat (val, "lines");
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    naivenb[i] = 0;
    naivecuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_NAIVE; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        naivecuml[i] += longueur;
        naivenb[i] ++;
      }
      else
      {
        naivesmall ++;
fprintf (stdout, "Segments FBSD de longueur %lf\n", longueur);
      }
    }
    fclose (pf);
    naiveml[i] = naivecuml[i] / naivenb[i];
  }
fprintf (stdout, "%d segments FBSD < 10 pixels\n", naivesmall);
fprintf (stdout, "Min lsd = %lf \n", lsdmin);
fprintf (stdout, "Min ed = %lf \n", edmin);
fprintf (stdout, "Min canny = %lf \n", cannymin);

  // Computes means
  for (int i = 0; i < nbim; i ++)
  {
    lsdmcuml += lsdcuml[i];
    lsdmnb += (double) lsdnb[i];
    lsdmml += lsdml[i];
    edmcuml += edcuml[i];
    edmnb += (double) ednb[i];
    edmml += edml[i];
    cannymcuml += cannycuml[i];
    cannymnb += (double) cannynb[i];
    cannymml += cannyml[i];
    naivemcuml += naivecuml[i];
    naivemnb += (double) naivenb[i];
    naivemml += naiveml[i];
  }
  lsdmcuml /= nbim;
  lsdmnb /= nbim;
  lsdmml /= nbim;
  edmcuml /= nbim;
  edmnb /= nbim;
  edmml /= nbim;
  cannymcuml /= nbim;
  cannymnb /= nbim;
  cannymml /= nbim;
  naivemcuml /= nbim;
  naivemnb /= nbim;
  naivemml /= nbim;

  // Computes standard deviations
  for (int i = 0; i < nbim; i ++)
  {
    lsdsnb += (lsdnb[i] - lsdmnb) * (lsdnb[i] - lsdmnb);
    lsdscuml += (lsdcuml[i] - lsdmcuml) * (lsdcuml[i] - lsdmcuml);
    lsdsml += (lsdml[i] - lsdmml) * (lsdml[i] - lsdmml);
    edsnb += (ednb[i] - edmnb) * (ednb[i] - edmnb);
    edscuml += (edcuml[i] - edmcuml) * (edcuml[i] - edmcuml);
    edsml += (edml[i] - edmml) * (edml[i] - edmml);
    cannysnb += (cannynb[i] - cannymnb) * (cannynb[i] - cannymnb);
    cannyscuml += (cannycuml[i] - cannymcuml) * (cannycuml[i] - cannymcuml);
    cannysml += (cannyml[i] - cannymml) * (cannyml[i] - cannymml);
    naivesnb += (naivenb[i] - naivemnb) * (naivenb[i] - naivemnb);
    naivescuml += (naivecuml[i] - naivemcuml) * (naivecuml[i] - naivemcuml);
    naivesml += (naiveml[i] - naivemml) * (naiveml[i] - naivemml);
  }
  lsdsnb = sqrt (lsdsnb / (nbim));
  lsdscuml = sqrt (lsdscuml / (nbim));
  lsdsml = sqrt (lsdsml / (nbim));
  edsnb = sqrt (edsnb / (nbim));
  edscuml = sqrt (edscuml / (nbim));
  edsml = sqrt (edsml / (nbim));
  cannysnb = sqrt (cannysnb / (nbim));
  cannyscuml = sqrt (cannyscuml / (nbim));
  cannysml = sqrt (cannysml / (nbim));
  naivesnb = sqrt (naivesnb / (nbim));
  naivescuml = sqrt (naivescuml / (nbim));
  naivesml = sqrt (naivesml / (nbim));

  if ((pf = fopen (OUTFILE, "w")) == NULL)
  {
    fprintf (stderr, "Pas de creation de %s\n", OUTFILE);
    return (0);
  }
  fprintf (pf, "         RESOLUTION %d x %d\n", w, h);
  fprintf (pf, "LSD :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", lsdmnb, lsdsnb);
  fprintf (pf, "  L   = %lf (%lf)\n", lsdmcuml, lsdscuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", lsdmml, lsdsml);
  fprintf (pf, "ED :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", edmnb, edsnb);
  fprintf (pf, "  L   = %lf (%lf)\n", edmcuml, edscuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", edmml, edsml);
  fprintf (pf, "Canny :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", cannymnb, cannysnb);
  fprintf (pf, "  L   = %lf (%lf)\n", cannymcuml, cannyscuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", cannymml, cannysml);
  fprintf (pf, "FBSD  :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", naivemnb, naivesnb);
  fprintf (pf, "  L   = %lf (%lf)\n", naivemcuml, naivescuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", naivemml, naivesml);
  fclose (pf);
  return (1);
}
