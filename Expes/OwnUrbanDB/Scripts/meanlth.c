// Compilation : cc -o meanlth meanlth.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define OUTFILE "meanlth.txt"
#define MIN_LENGTH 10
#define OLD_FILE "naiveold.txt"
#define NEW_FILE "naivenew.txt"


int main (int argc, char *argv[])
{
  double sx, sy, ex, ey, th, longueur;
  int newnb = 0, oldnb = 0;
  double newcuml = 0., newcumth = 0., newmeanl, newmeanth;
  double oldcuml = 0., oldcumth = 0., oldmeanl, oldmeanth;
  FILE *pf = NULL;

  if ((pf = fopen (NEW_FILE, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", NEW_FILE);
    return (0);
  }
  while (fscanf (pf, "%lf", &sx) != -1)
  {
    fscanf (pf, "%lf", &sy);
    fscanf (pf, "%lf", &ex);
    fscanf (pf, "%lf", &ey);
    fscanf (pf, "%lf", &th);
    longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
    if (longueur >= MIN_LENGTH)
    {
      newcuml += longueur;
      newcumth += th * longueur;
      newnb ++;
    }
  }
  fclose (pf);
  newmeanl = newcuml / newnb;
  newmeanth = newcumth / newcuml;

  if ((pf = fopen (OLD_FILE, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", OLD_FILE);
    return (0);
  }
  while (fscanf (pf, "%lf", &sx) != -1)
  {
    fscanf (pf, "%lf", &sy);
    fscanf (pf, "%lf", &ex);
    fscanf (pf, "%lf", &ey);
    fscanf (pf, "%lf", &th);
    longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
    if (longueur >= MIN_LENGTH)
    {
      oldcuml += longueur;
      oldcumth += th * longueur;
      oldnb ++;
    }
  }
  fclose (pf);
  oldmeanl = oldcuml / oldnb;
  oldmeanth = oldcumth / oldcuml;

  if ((pf = fopen (OUTFILE, "w")) == NULL)
  {
    fprintf (stderr, "Pas de creation de %s\n", OUTFILE);
    return (0);
  }
  fprintf (pf, "         MIN LENGTH CONSIDERED %d\n", MIN_LENGTH);
  fprintf (pf, "OLD :\n");
  fprintf (pf, "  N   = %d\n", oldnb);
  fprintf (pf, "  L/N = %lf\n", oldmeanl);
  fprintf (pf, "  W/L = %lf\n", oldmeanth);
  fprintf (pf, "NEW :\n");
  fprintf (pf, "  N   = %d\n", newnb);
  fprintf (pf, "  L/N = %lf\n", newmeanl);
  fprintf (pf, "  W/L = %lf\n", newmeanth);
  fclose (pf);
  return (1);
}
