// Compilation : cc -o mtimes mtimes.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define NB_MES 20
#define FICHIER_CANNY "cannytimes.txt"
#define FICHIER_FBSD "fbsdtimes.txt"
#define FICHIER_LSD "lsdtimes.txt"
#define FICHIER_ED "edtimes.txt"


int main (int argc, char *argv[])
{
  double vcanny[NB_MES];
  double vfbsd[NB_MES];
  double vlsd[NB_MES];
  double ved[NB_MES];
  double mcanny = 0.;
  double mfbsd = 0.;
  double mlsd = 0.;
  double med = 0.;
  double sigcanny = 0.;
  double sigfbsd = 0.;
  double siglsd = 0.;
  double siged = 0.;
  double *val;
  int nbcanny = 0;
  int nbfbsd = 0;
  int nblsd = 0;
  int nbed = 0;
  FILE *pfcanny = NULL;
  FILE *pffbsd = NULL;
  FILE *pflsd = NULL;
  FILE *pfed = NULL;

  if ((pfcanny = fopen (FICHIER_CANNY, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_CANNY);
    return (0);
  }
  val = vcanny;
  while (fscanf (pfcanny, "%lf", val) != -1)
  {
    mcanny += *val++;
    nbcanny ++;
  }
  fclose (pfcanny);

  if ((pffbsd = fopen (FICHIER_FBSD, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_FBSD);
    return (0);
  }
  val = vfbsd;
  while (fscanf (pffbsd, "%lf", val) != -1)
  {
    mfbsd += *val++;
    nbfbsd ++;
  }
  fclose (pffbsd);

  if (nbfbsd != nbcanny)
  {
    fprintf (stderr, "Different counts of measures\n");
    return (0);
  }

  if ((pflsd = fopen (FICHIER_LSD, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_LSD);
    return (0);
  }
  val = vlsd;
  while (fscanf (pflsd, "%lf", val) != -1)
  {
    mlsd += *val++;
    nblsd ++;
  }
  fclose (pflsd);

  if ((pfed = fopen (FICHIER_ED, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_ED);
    return (0);
  }
  val = ved;
  while (fscanf (pfed, "%lf", val) != -1)
  {
    med += *val++;
    nbed ++;
  }
  fclose (pfed);

  if (nbfbsd != NB_MES || nbcanny != NB_MES
      || nblsd != NB_MES || nbed != NB_MES)
  {
    fprintf (stderr, "Bad counts of measures\n");
    return (0);
  }

  // Computes means
  mcanny /= nbcanny;
  mfbsd /= nbfbsd;
  mlsd /= nblsd;
  med /= nbed;

  // Computes standard deviations
  for (int i = 0; i < NB_MES; i ++)
  {
    sigcanny += (vcanny[i] - mcanny) * (vcanny[i] - mcanny);
    sigfbsd += (vfbsd[i] - mfbsd) * (vfbsd[i] - mfbsd);
    siglsd += (vlsd[i] - mlsd) * (vlsd[i] - mlsd);
    siged += (ved[i] - med) * (ved[i] - med);
  }
  sigcanny = sqrt (sigcanny / (nbcanny - 1));
  sigfbsd = sqrt (sigfbsd / (nbfbsd - 1));
  siglsd = sqrt (siglsd / (nblsd - 1));
  siged = sqrt (siged / (nbed - 1));

  fprintf (stdout, "Canny : %lf ms (%lf)\n", mcanny * 10, sigcanny * 10);
  fprintf (stdout, "FBSD  : %lf ms (%lf)\n", mfbsd * 10, sigfbsd * 10);
  fprintf (stdout, "LSD   : %lf ms (%lf)\n", mlsd * 10, siglsd * 10);
  fprintf (stdout, "ED    : %lf ms (%lf)\n", med * 10, siged * 10);
  return (1);
}
