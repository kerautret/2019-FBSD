// Compilation : cc -o epaisseurs epaisseurs.c -lm
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define MIN_DTHETA 8.0
#define MIN_SHIFT 3.0
#define LSD_DIR "/home/even/Expes/YorkUrbanDB/lines/lsdlines/output/"
#define NAIVE_DIR "/home/even/Expes/YorkUrbanDB/lines/naivelines/output/"
#define LSD_NB 7
#define NAIVE_NB 5


int main (int argc, char* argv[])
{
  struct seg
  {
    double sx;
    double sy;
    double ex;
    double ey;
    double th;
  };

  struct seg segs1[20000];
  struct seg segs2[20000];
  int match[20000];
  double lmatch[20000];
  char nom[200];

  int nsegs1 = 0, nsegs2 = 0;
  int nblect;
  double val;

  // 1. Reading first file
  strcpy (nom, LSD_DIR);
  strcat (nom, argv[1]);
  strcat (nom, ".txt");
  FILE *pf = NULL;
  if ((pf = fopen (nom, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", nom);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % LSD_NB == 0) segs1[nblect/LSD_NB].sx = val;
    else if (nblect % LSD_NB == 1) segs1[nblect/LSD_NB].sy = val;
    else if (nblect % LSD_NB == 2) segs1[nblect/LSD_NB].ex = val;
    else if (nblect % LSD_NB == 3) segs1[nblect/LSD_NB].ey = val;
    else if (nblect % LSD_NB == 4) segs1[nblect/LSD_NB].th = val;
    if (nblect % LSD_NB == LSD_NB - 1) nsegs1 ++;
    nblect ++;
  }
  fclose (pf);

  // 2. Reading second file
  strcpy (nom, NAIVE_DIR);
  strcat (nom, argv[1]);
  strcat (nom, ".txt");
  pf = NULL;
  if ((pf = fopen (nom, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", nom);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % NAIVE_NB == 0) segs2[nblect/NAIVE_NB].sx = val;
    else if (nblect % NAIVE_NB == 1) segs2[nblect/NAIVE_NB].sy = val;
    else if (nblect % NAIVE_NB == 2) segs2[nblect/NAIVE_NB].ex = val;
    else if (nblect % NAIVE_NB == 3) segs2[nblect/NAIVE_NB].ey = val;
    else if (nblect % NAIVE_NB == 4) segs2[nblect/NAIVE_NB].th = val;
    if (nblect % NAIVE_NB == NAIVE_NB - 1) nsegs2 ++;
    nblect ++;
  }
  fclose (pf);
  
  // 3. Matching
  int nbor = 0, nbec = 0, nbin = 0;
  int opposite = 0;
  double totl = 0., totdtheta = 0.;
  double cosang = 0., dtheta = 0., shift = 0., cx = 0., cy = 0.;
  double px = 0., py = 0., qx = 0., qy = 0., lgth = 0.;
  for (int i = 0; i < nsegs1; i++)
  {
    double vrefx = segs1[i].ex - segs1[i].sx;
    double vrefy = segs1[i].ey - segs1[i].sy;
    double vrefn = sqrt (vrefx * vrefx + vrefy * vrefy);
    double aref = segs1[i].ey - segs1[i].sy;
    double bref = segs1[i].sx - segs1[i].ex;
    double cref = aref * segs1[i].sx + bref * segs1[i].sy;
    double dref = sqrt (aref * aref + bref * bref);
    lmatch[i] = 0.;
    match[i] = -1;

    for (int j = 0; j < nsegs2; j++)
    {
      opposite = 1;
      double vtestx = segs2[j].ex - segs2[j].sx;
      double vtesty = segs2[j].ey - segs2[j].sy;
      double vtestn = sqrt (vtestx * vtestx + vtesty * vtesty);
      cosang = (vtestx * vrefx + vtesty * vrefy) / (vrefn * vtestn);
      if (cosang < 0.)
      {
        opposite = 1;
        cosang = - cosang;
      }
      dtheta = (180. / M_PI) * acos (cosang);
      if (dtheta < MIN_DTHETA)
      {
        nbor ++;
        cx = (segs2[j].sx + segs2[j].ex) / 2;
        cy = (segs2[j].sy + segs2[j].ey) / 2;
        shift = (cref - aref * cx - bref * cy) / dref;
        if (shift < 0) shift = - shift;
        if (shift < MIN_SHIFT)
        {
          nbec ++;
          double vecacx = cx - segs1[i].sx;
          double vecacy = cy - segs1[i].sy;
          double vecbcx = cx - segs1[i].ex;
          double vecbcy = cy - segs1[i].ey;
          if (vrefx * vecacx + vrefy * vecacy >= 0.
              && vrefx * vecbcx + vrefy * vecbcy <= 0.)
          {
            nbin ++;
            if (opposite == 1)
            {
              px = segs2[j].ex;
              py = segs2[j].ey;
              qx = segs2[j].sx;
              qy = segs2[j].sy;
            }
            else
            {
              px = segs2[j].sx;
              py = segs2[j].sy;
              qx = segs2[j].ex;
              qy = segs2[j].ey;
            }
            double vecapx = px - segs1[i].sx;
            double vecapy = py - segs1[i].sy;
            if (vrefx * vecapx + vrefy * vecapy < 0.)
            {
              px = segs1[i].sx;
              py = segs1[i].sy;
            }
            double vecqbx = segs1[i].ex - qx;
            double vecqby = segs1[i].ey - qy;
            if (vrefx * vecqbx + vrefy * vecqby < 0.)
            {
              qx = segs1[i].ex;
              qy = segs1[i].ey;
            }
            lgth = sqrt ((qx - px) * (qx - px) + (qy - py) * (qy - py));
            if (lgth > lmatch[i])
            {
              lmatch[i] = lgth;
              match[i] = j;
            }
            totl += lgth;
            totdtheta += dtheta * lgth;
          }
        }
      }
    }
  }
fprintf (stdout, "%d orientations communes, %d alignements, %d confusions\n",
         nbor, nbec, nbin);
  strcpy (nom, "thicks");
  strcat (nom, argv[1]);
  strcat (nom, ".txt");
  pf = NULL;
  if ((pf = fopen (nom, "w")) == NULL)
  {
    fprintf (stderr, "Unbale to create %s\n", nom);
    return (0);
  }
  for (int i = 0; i < nsegs1; i++)
    if (match[i] != -1)
      fprintf (pf, "LSD (%lf) <-> FBSD (%lf) : %lf long\n",
               segs1[i].th, segs2[match[i]].th, lmatch[i]);
  fclose (pf);
}
