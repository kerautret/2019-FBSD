// Compilation : cc -o compepais compepais.c -lm
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define MIN_LENGTH 20.
#define MIN_DTHETA 8.0
#define MIN_SHIFT 3.0
#define YORK_DIR "/home/even/Expes/YorkUrbanDB/lines/yorklines/"
#define DET_DIR "/home/even/Expes/YorkUrbanDB/lines/"
#define LINES_DIR "lines/output/"
#define OUT_FILE "tmp.txt"
#define YORK_NB 4
#define LSD_NB 7
#define ED_NB 4
#define CANNY_NB 5
#define NAIVE_NB 5


int main (int argc, char* argv[])
{
  struct seg
  {
    double sx;
    double sy;
    double ex;
    double ey;
  };

  struct seg segs1[20000];
  struct seg segs2[20000];
  int match[20000];
  double lmatch[20000];
  double ldtheta[20000];
  char nom[200];

  int nsegs1 = 0, nsegs2 = 0;
  int nblect;
  double val;

  // 1. Reading first file
  strcpy (nom, YORK_DIR);
  strcat (nom, argv[2]);
  strcat (nom, "Lines.txt");
  FILE *pf = NULL;
  if ((pf = fopen (nom, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", nom);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % YORK_NB == 0) segs1[nblect/YORK_NB].sx = val;
    else if (nblect % YORK_NB == 1) segs1[nblect/YORK_NB].sy = val;
    else if (nblect % YORK_NB == 2) segs1[nblect/YORK_NB].ex = val;
    else if (nblect % YORK_NB == 3) segs1[nblect/YORK_NB].ey = val;
    if (nblect % YORK_NB == YORK_NB - 1) nsegs1 ++;
    nblect ++;
  }
  fclose (pf);

  // 2. Reading second file
  strcpy (nom, DET_DIR);
  strcat (nom, argv[1]);
  strcat (nom, LINES_DIR);
  strcat (nom, argv[2]);
  strcat (nom, ".txt");
  pf = NULL;
  if ((pf = fopen (nom, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", nom);
    return (0);
  }
  int detnb = 0;
  if (strcmp (argv[1], "lsd") == 0) detnb = LSD_NB;
  else if (strcmp (argv[1], "ed") == 0) detnb = ED_NB;
  else if (strcmp (argv[1], "canny") == 0) detnb = CANNY_NB;
  else if (strcmp (argv[1], "naive") == 0) detnb = NAIVE_NB;
  if (detnb == 0)
  {
    fprintf (stdout, "Unrecognized detector %s\n", argv[1]);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % detnb == 0) segs2[nblect/detnb].sx = val;
    else if (nblect % detnb == 1) segs2[nblect/detnb].sy = val;
    else if (nblect % detnb == 2) segs2[nblect/detnb].ex = val;
    else if (nblect % detnb == 3) segs2[nblect/detnb].ey = val;
    if (nblect % detnb == detnb - 1) nsegs2 ++;
    nblect ++;
  }
  fclose (pf);
  
  // 3. Matching
  int nbor = 0, nbec = 0, nbin = 0;
  int opposite = 0;
  double cosang = 0., dtheta = 0., shift = 0., cx = 0., cy = 0.;
  double px = 0., py = 0., qx = 0., qy = 0., lgth = 0.;
  for (int i = 0; i < nsegs1; i++)
  {
    double vrefx = segs1[i].ex - segs1[i].sx;
    double vrefy = segs1[i].ey - segs1[i].sy;
    double vrefn = sqrt (vrefx * vrefx + vrefy * vrefy);
    double aref = segs1[i].ey - segs1[i].sy;
    double bref = segs1[i].sx - segs1[i].ex;
    double cref = aref * segs1[i].sx + bref * segs1[i].sy;
    double dref = sqrt (aref * aref + bref * bref);
    lmatch[i] = 0.;
    ldtheta[i] = 0.;
    match[i] = -1;

    for (int j = 0; j < nsegs2; j++)
    {
      opposite = 1;
      double vtestx = segs2[j].ex - segs2[j].sx;
      double vtesty = segs2[j].ey - segs2[j].sy;
      double vtestn = sqrt (vtestx * vtestx + vtesty * vtesty);
      cosang = (vtestx * vrefx + vtesty * vrefy) / (vrefn * vtestn);
      if (cosang < 0.)
      {
        opposite = 1;
        cosang = - cosang;
      }
      dtheta = (180. / M_PI) * acos (cosang);
      if (dtheta < MIN_DTHETA)
      {
        nbor ++;
        cx = (segs2[j].sx + segs2[j].ex) / 2;
        cy = (segs2[j].sy + segs2[j].ey) / 2;
        shift = (cref - aref * cx - bref * cy) / dref;
        if (shift < 0) shift = - shift;
        if (shift < MIN_SHIFT)
        {
          nbec ++;
          double vecacx = cx - segs1[i].sx;
          double vecacy = cy - segs1[i].sy;
          double vecbcx = cx - segs1[i].ex;
          double vecbcy = cy - segs1[i].ey;
          if (vrefx * vecacx + vrefy * vecacy >= 0.
              && vrefx * vecbcx + vrefy * vecbcy <= 0.)
          {
            nbin ++;
            if (opposite == 1)
            {
              px = segs2[j].ex;
              py = segs2[j].ey;
              qx = segs2[j].sx;
              qy = segs2[j].sy;
            }
            else
            {
              px = segs2[j].sx;
              py = segs2[j].sy;
              qx = segs2[j].ex;
              qy = segs2[j].ey;
            }
            double vecapx = px - segs1[i].sx;
            double vecapy = py - segs1[i].sy;
            if (vrefx * vecapx + vrefy * vecapy < 0.)
            {
              px = segs1[i].sx;
              py = segs1[i].sy;
            }
            double vecqbx = segs1[i].ex - qx;
            double vecqby = segs1[i].ey - qy;
            if (vrefx * vecqbx + vrefy * vecqby < 0.)
            {
              qx = segs1[i].ex;
              qy = segs1[i].ey;
            }
            lgth = sqrt ((qx - px) * (qx - px) + (qy - py) * (qy - py));
            if (lgth > lmatch[i])
            {
              lmatch[i] = lgth;
              ldtheta[i] = dtheta * lgth;
              match[i] = j;
            }
          }
        }
      }
    }
  }

  // 4. Angle cumulation
  double totl = 0.;
  double totdtheta = 0.;
  for (int i = 0; i < nsegs1; i++)
    if (match[i] != -1)
    {
      if (lmatch[i] >= MIN_LENGTH)
      {
        totl += lmatch[i];
        totdtheta += ldtheta[i];
      }
    }

  // 5. Result output
  strcpy (nom, OUT_FILE);
  pf = NULL;
  if ((pf = fopen (nom, "w")) == NULL)
  {
    fprintf (stderr, "Unable to create %s\n", nom);
    return (0);
  }
  fprintf (pf, "%lf %lf\n", totl, (totl != 0. ? totdtheta/totl : 0.));
  fclose (pf);
}
