// Compilation : cc -o oldtimes oldtimes.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define NB_MES 102
#define FICHIER_OLD "oldtimes.txt"
#define FICHIER_NEW "newtimes.txt"


int main (int argc, char *argv[])
{
  double vold[NB_MES];
  double vnew[NB_MES];
  double mold = 0.;
  double mnew = 0.;
  double sigold = 0.;
  double signew = 0.;
  double *val;
  int nbold = 0;
  int nbnew = 0;
  FILE *pfold = NULL;
  FILE *pfnew = NULL;

  if ((pfold = fopen (FICHIER_OLD, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_OLD);
    return (0);
  }
  val = vold;
  while (fscanf (pfold, "%lf", val) != -1)
  {
    mold += *val++;
    nbold ++;
  }
  fclose (pfold);

  if ((pfnew = fopen (FICHIER_NEW, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FICHIER_NEW);
    return (0);
  }
  val = vnew;
  while (fscanf (pfnew, "%lf", val) != -1)
  {
    mnew += *val++;
    nbnew ++;
  }
  fclose (pfnew);

  if (nbnew != nbold)
  {
    fprintf (stderr, "Different counts of measures\n");
    return (0);
  }

  // Computes means
  mold /= nbold;
  mnew /= nbnew;

  // Computes standard deviations
  for (int i = 0; i < NB_MES; i ++)
  {
    sigold += (vold[i] - mold) * (vold[i] - mold);
    signew += (vnew[i] - mnew) * (vnew[i] - mnew);
  }
  sigold = sqrt (sigold / (nbold - 1));
  signew = sqrt (signew / (nbnew - 1));

  fprintf (stdout, "Old : %lf ms (%lf)\n", mold * 10, sigold * 10);
  fprintf (stdout, "New : %lf ms (%lf)\n", mnew * 10, signew * 10);
  return (1);
}
