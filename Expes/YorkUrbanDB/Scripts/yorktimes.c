// Compilation : cc -o yorktimes yorktimes.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define EXPES_DIR "/home/even/Expes/"
#define FBSD_DIR "/home/even/tmp/FBSD/"
#define YORK_DIR "YorkUrbanDB/"
#define DATA_DIR "data/"
#define LSD_DIR "Timers/lsd/"
#define ED_DIR "Timers/edlines/"
#define CANNY_DIR "Timers/cannylines/"
#define IMAGE_NAMES "yorkImages.txt"
#define SCRIPT_NAME "exectimes.sh"
#define NBMAX 200


void tirage (int *ordre)
{
  int nb = 4, val;
  int choix[] = {0, 1, 2, 3};
  while (nb > 1)
  {
    val = rand () % nb;
    *ordre++ = choix[val];
    choix[val] = choix[--nb];
  }
  *ordre = choix[0];
}

int main (int argc, char *argv[])
{
  FILE *fim = NULL;
  FILE *fcom = NULL;
  char val[NBMAX];
  char *ims[NBMAX];
  int tir[4];
  int w, h;
  int nb = 0;
  int nstart = 0, nend = 0;

  srand (time (NULL));
  if (argc != 3)
  { 
    fprintf (stdout, "Usage : yorktimes <num_start> <num_end>\n");
    return (0);
  }
  sscanf (argv[1], "%d", &nstart);
  sscanf (argv[2], "%d", &nend);
  nstart --;
  nend --;
  if ((fim = fopen (IMAGE_NAMES, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", IMAGE_NAMES);
    return (0);
  }
  fscanf (fim, "%d", &w);
  fscanf (fim, "%d", &h);
  while (fscanf (fim, "%s", val) != -1)
  {
    ims[nb] = (char *) malloc (strlen (val) * sizeof (char) + 1);
    strcpy (ims[nb++], val);
  }
  fclose (fim);
  if ((fcom = fopen (SCRIPT_NAME, "w")) == NULL)
  {
    fprintf (stderr, "Unable to create %s\n", SCRIPT_NAME);
    return (0);
  }
  sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
  fprintf (fcom, "%s\n", val);
  for (int i = nstart; i <= nend; i++)
  {
    tirage (tir);
    for (int j = 0; j < 4; j++)
    {
      if (tir[j] == 0) // lsd
      {
        sprintf (val, "\\cp ImagesPGM/%s.pgm %s%stest.pgm",
                 ims[i], EXPES_DIR, LSD_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, LSD_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "\\rm lsdperf.txt");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "echo 'TIMING %d (%s) on lsd'", (i+1), ims[i]);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "lsd");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cat lsdperf.txt >> %s%s%slsdtimes.txt",
                 EXPES_DIR, YORK_DIR, DATA_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
        fprintf (fcom, "%s\n", val);
      }
      else if (tir[j] == 1) // ed
      {
        sprintf (val, "\\cp Images/%s/%s.jpg %s%stest.jpg",
                 ims[i], ims[i], EXPES_DIR, ED_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, ED_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "\\rm edperf.txt");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "echo 'TIMING %d (%s) on ed'", (i+1), ims[i]);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "EDLines");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cat edperf.txt >> %s%s%sedtimes.txt",
                 EXPES_DIR, YORK_DIR, DATA_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
        fprintf (fcom, "%s\n", val);
      }
      else if (tir[j] == 2) // canny
      {
        sprintf (val, "\\cp Images/%s/%s.jpg %s%stest.jpg",
                 ims[i], ims[i], EXPES_DIR, CANNY_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, CANNY_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "\\rm cannyperf.txt");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "echo 'TIMING %d (%s) on canny'", (i+1), ims[i]);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "CannyLine");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cat cannyperf.txt >> %s%s%scannytimes.txt",
                 EXPES_DIR, YORK_DIR, DATA_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
        fprintf (fcom, "%s\n", val);
      }
      else if (tir[j] == 3) // fbsd
      {
        sprintf (val, "\\cp Images/%s/%s.jpg %stest.jpg",
                 ims[i], ims[i], FBSD_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s", FBSD_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "echo 'TIMING %d (%s) on fbsd'", (i+1), ims[i]);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "./FBSD -yt");
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cat fbsdperf.txt >> %s%s%sfbsdtimes.txt",
                 EXPES_DIR, YORK_DIR, DATA_DIR);
        fprintf (fcom, "%s\n", val);
        sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
        fprintf (fcom, "%s\n", val);
      }
    }
  }
  sprintf (val, "cd %s", DATA_DIR);
  fprintf (fcom, "%s\n", val);
  fclose (fcom);
  return (0);
}
