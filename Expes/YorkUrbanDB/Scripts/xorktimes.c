// Compilation : cc -o xorktimes xorktimes.c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#define EXPES_DIR "/home/even/Expes/"
#define FBSD_DIR "/home/even/tmp/FBSD/"
#define YORK_DIR "YorkUrbanDB/"
#define DATA_DIR "data/"
#define IMAGE_NAMES "yorkImages.txt"
#define SCRIPT_NAME "exectimes.sh"
#define NBMAX 200


int main (int argc, char *argv[])
{
  FILE *fim = NULL;
  FILE *fcom = NULL;
  char val[NBMAX];
  char *ims[NBMAX];
  int w, h;
  int nb = 0;
  int nstart = 0, nend = 0;

  srand (time (NULL));
  int tirage = rand () % 2;
  if (argc != 3)
  { 
    fprintf (stdout, "Usage : xorktimes <num_start> <num_end>\n");
    return (0);
  }
  sscanf (argv[1], "%d", &nstart);
  sscanf (argv[2], "%d", &nend);
  nstart --;
  nend --;
  if ((fim = fopen (IMAGE_NAMES, "r")) == NULL)
  {
    fprintf (stderr, "No file %s\n", IMAGE_NAMES);
    return (0);
  }
  fscanf (fim, "%d", &w);
  fscanf (fim, "%d", &h);
  while (fscanf (fim, "%s", val) != -1)
  {
    ims[nb] = (char *) malloc (strlen (val) * sizeof (char) + 1);
    strcpy (ims[nb++], val);
  }
  fclose (fim);
  if ((fcom = fopen (SCRIPT_NAME, "w")) == NULL)
  {
    fprintf (stderr, "Unable to create %s\n", SCRIPT_NAME);
    return (0);
  }
  sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
  fprintf (fcom, "%s\n", val);
  for (int i = nstart; i <= nend; i++)
  {
    for (int j = 0; j < 2; j++)
    {
      sprintf (val, "\\cp Images/%s/%s.jpg %stest.jpg",
               ims[i], ims[i], FBSD_DIR);
      fprintf (fcom, "%s\n", val);
      sprintf (val, "cd %s", FBSD_DIR);
      fprintf (fcom, "%s\n", val);
      sprintf (val, "echo 'TIMING %d (%s) on fbsd'", (i+1), ims[i]);
      fprintf (fcom, "%s\n", val);
      if (tirage == 1) sprintf (val, "./FBSD -yt");
      else sprintf (val, "./FBSD -xt");
      fprintf (fcom, "%s\n", val);
      sprintf (val, "cat fbsdperf.txt >> %s%s%s%stimes.txt",
               EXPES_DIR, YORK_DIR, DATA_DIR, (tirage == 0 ? "old" : "new"));
      fprintf (fcom, "%s\n", val);
      sprintf (val, "cd %s%s", EXPES_DIR, YORK_DIR);
      fprintf (fcom, "%s\n", val);
      if (j == 0) tirage = (tirage == 0 ? 1 : 0);
    }
  }
  sprintf (val, "cd %s", DATA_DIR);
  fprintf (fcom, "%s\n", val);
  fclose (fcom);
  return (0);
}
