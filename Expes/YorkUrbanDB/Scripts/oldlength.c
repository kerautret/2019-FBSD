// Compilation : cc -o oldlength oldlength.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define OUTFILE "/home/even/Expes/YorkUrbanDB/data/oldlength.txt"
#define IMAGE_NAMES "/home/even/Expes/YorkUrbanDB/Scripts/yorkImages.txt"
#define MIN_LENGTH 10
#define NBMAX 200
#define OLD_DIR "/home/even/Expes/YorkUrbanDB/lines/oldlines/output/"
#define NAIVE_DIR "/home/even/Expes/YorkUrbanDB/lines/naivelines/output/"
#define NB_OLD 5
#define NB_NAIVE 5


int main (int argc, char *argv[])
{
  int w, h;
  double sx, sy, ex, ey, cmp, longueur;
  int oldnb[NBMAX];
  double oldml[NBMAX];
  double oldcuml[NBMAX];
  int naivenb[NBMAX];
  double naiveml[NBMAX];
  double naivecuml[NBMAX];
  double oldmcuml = 0., oldmnb = 0., oldmml = 0.;
  double oldscuml = 0., oldsnb = 0., oldsml = 0.;
  double oldmin = 50.;
  double naivemcuml = 0., naivemnb = 0., naivemml = 0.;
  double naivescuml = 0., naivesnb = 0., naivesml = 0.;
  int naivesmall = 0;
  int nbim = 0;
  char val[NBMAX];
  char *inames[NBMAX];
  FILE *pf = NULL;

  if ((pf = fopen (IMAGE_NAMES, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", IMAGE_NAMES);
    return (0);
  }
  fscanf (pf, "%d", &w);
  fscanf (pf, "%d", &h);
  while (fscanf (pf, "%s", val) != -1)
  {
    inames[nbim] = (char *) malloc (strlen (val) * sizeof (char) + 1);
    strcpy (inames[nbim++], val);
  }
  fclose (pf);

  for (int i = 0; i < nbim; i++)
  {
    strcpy (val, OLD_DIR);
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    oldnb[i] = 0;
    oldcuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_OLD; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        oldcuml[i] += longueur;
        oldnb[i] ++;
      }
      if (longueur < oldmin) oldmin = longueur;
    }
    fclose (pf);
    oldml[i] = oldcuml[i] / oldnb[i];

    strcpy (val, NAIVE_DIR);
    strcat (val, inames[i]);
    strcat (val, ".txt");
    if ((pf = fopen (val, "r")) == NULL)
    {
      fprintf (stderr, "Pas de fichier %s\n", val);
      return (0);
    }
    naivenb[i] = 0;
    naivecuml[i] = 0.;
    while (fscanf (pf, "%lf", &sx) != -1)
    {
      fscanf (pf, "%lf", &sy);
      fscanf (pf, "%lf", &ex);
      fscanf (pf, "%lf", &ey);
      for (int i = 4; i < NB_NAIVE; i++) fscanf (pf, "%lf", &cmp);
      longueur = sqrt ((ex - sx) * (ex - sx) + (ey - sy) * (ey - sy));
      if (longueur >= MIN_LENGTH)
      {
        naivecuml[i] += longueur;
        naivenb[i] ++;
      }
      else
      {
        naivesmall ++;
      }
    }
    fclose (pf);
    naiveml[i] = naivecuml[i] / naivenb[i];
  }
//fprintf (stdout, "%d segments FBSD < 10 pixels\n", naivesmall);
//fprintf (stdout, "Min lsd = %lf \n", lsdmin);
//fprintf (stdout, "Min ed = %lf \n", edmin);
//fprintf (stdout, "Min canny = %lf \n", cannymin);

  // Computes means
  for (int i = 0; i < nbim; i ++)
  {
    oldmcuml += oldcuml[i];
    oldmnb += (double) oldnb[i];
    oldmml += oldml[i];
    naivemcuml += naivecuml[i];
    naivemnb += (double) naivenb[i];
    naivemml += naiveml[i];
  }
  oldmcuml /= nbim;
  oldmnb /= nbim;
  oldmml /= nbim;
  naivemcuml /= nbim;
  naivemnb /= nbim;
  naivemml /= nbim;

  // Computes standard deviations
  for (int i = 0; i < nbim; i ++)
  {
    oldsnb += (oldnb[i] - oldmnb) * (oldnb[i] - oldmnb);
    oldscuml += (oldcuml[i] - oldmcuml) * (oldcuml[i] - oldmcuml);
    oldsml += (oldml[i] - oldmml) * (oldml[i] - oldmml);
    naivesnb += (naivenb[i] - naivemnb) * (naivenb[i] - naivemnb);
    naivescuml += (naivecuml[i] - naivemcuml) * (naivecuml[i] - naivemcuml);
    naivesml += (naiveml[i] - naivemml) * (naiveml[i] - naivemml);
  }
  oldsnb = sqrt (oldsnb / (nbim));
  oldscuml = sqrt (oldscuml / (nbim));
  oldsml = sqrt (oldsml / (nbim));
  naivesnb = sqrt (naivesnb / (nbim));
  naivescuml = sqrt (naivescuml / (nbim));
  naivesml = sqrt (naivesml / (nbim));

  if ((pf = fopen (OUTFILE, "w")) == NULL)
  {
    fprintf (stderr, "Pas de creation de %s\n", OUTFILE);
    return (0);
  }
  fprintf (pf, "         RESOLUTION %d x %d\tLONGUEUR MIN %d\n",
           w, h, MIN_LENGTH);
  fprintf (pf, "Old :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", oldmnb, oldsnb);
  fprintf (pf, "  L   = %lf (%lf)\n", oldmcuml, oldscuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", oldmml, oldsml);
  fprintf (pf, "Naive :\n");
  fprintf (pf, "  N   = %lf (%lf)\n", naivemnb, naivesnb);
  fprintf (pf, "  L   = %lf (%lf)\n", naivemcuml, naivescuml);
  fprintf (pf, "  L/N = %lf (%lf)\n", naivemml, naivesml);
  fclose (pf);
  return (1);
}
