// Compilation : cc -o pairing pairing.c -lm
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


#define MIN_DTHETA 8.0
#define MIN_SHIFT 3.0


int main (int argc, char* argv[])
{
  char *noms[] = {"lsd", "ed", "canny", "naive", "york"};
  int nbdata[] = {7, 4, 5, 5, 4};
  struct seg
  {
    double sx;
    double sy;
    double ex;
    double ey;
  };

  struct seg segs1[20000];
  struct seg segs2[20000];
  int match[20000];

  int nsegs1 = 0, nsegs2 = 0;
  int nblect, nbval = 0;
  double val;

  // 1. Reading first file type
  for (int k = 0; nbval == 0 && k < 5; k++)
    if (strcmp (argv[1], noms[k]) == 0) nbval = nbdata[k];
  if (nbval == 0)
  {
    fprintf (stdout, "Mauvais type de fichier : %s\n", argv[1]);
    return (0);
  }

  // 2. Reading first file
  FILE *pf = NULL;
  if ((pf = fopen (argv[2], "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", argv[2]);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % nbval == 0) segs1[nblect/nbval].sx = val;
    else if (nblect % nbval == 1) segs1[nblect/nbval].sy = val;
    else if (nblect % nbval == 2) segs1[nblect/nbval].ex = val;
    else if (nblect % nbval == 3) segs1[nblect/nbval].ey = val;
    if (nblect % nbval == nbval - 1) nsegs1 ++;
    nblect ++;
  }
  fclose (pf);

  // 3. Reading second file type
  nbval = 0;
  for (int k = 0; nbval == 0 && k < 5; k++)
    if (strcmp (argv[3], noms[k]) == 0) nbval = nbdata[k];
  if (nbval == 0)
  {
    fprintf (stdout, "Mauvais type de fichier : %s\n", argv[3]);
    return (0);
  }

  // 4. Reading second file
  pf = NULL;
  if ((pf = fopen (argv[4], "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", argv[4]);
    return (0);
  }
  nblect = 0;
  while (fscanf (pf, "%lf", &val) != -1)
  {
    if (nblect % nbval == 0) segs2[nblect/nbval].sx = val;
    else if (nblect % nbval == 1) segs2[nblect/nbval].sy = val;
    else if (nblect % nbval == 2) segs2[nblect/nbval].ex = val;
    else if (nblect % nbval == 3) segs2[nblect/nbval].ey = val;
    if (nblect % nbval == nbval - 1) nsegs2 ++;
    nblect ++;
  }
  fclose (pf);
  
  // 5. Matching
  int nbor = 0, nbec = 0, nbin = 0;
  int opposite = 0;
  double totl = 0., totdtheta = 0.;
  double cosang = 0., dtheta = 0., shift = 0., cx = 0., cy = 0.;
  double px = 0., py = 0., qx = 0., qy = 0., lgth = 0.;
  double matchlength = 0.;
  for (int i = 0; i < nsegs1; i++)
  {
    double vrefx = segs1[i].ex - segs1[i].sx;
    double vrefy = segs1[i].ey - segs1[i].sy;
    double vrefn = sqrt (vrefx * vrefx + vrefy * vrefy);
    double aref = segs1[i].ey - segs1[i].sy;
    double bref = segs1[i].sx - segs1[i].ex;
    double cref = aref * segs1[i].sx + bref * segs1[i].sy;
    double dref = sqrt (aref * aref + bref * bref);
    matchlength = 0.;
    match[i] = -1;

    for (int j = 0; j < nsegs2; j++)
    {
      opposite = 1;
      double vtestx = segs2[j].ex - segs2[j].sx;
      double vtesty = segs2[j].ey - segs2[j].sy;
      double vtestn = sqrt (vtestx * vtestx + vtesty * vtesty);
      cosang = (vtestx * vrefx + vtesty * vrefy) / (vrefn * vtestn);
      if (cosang < 0.)
      {
        opposite = 1;
        cosang = - cosang;
      }
      dtheta = (180. / M_PI) * acos (cosang);
      if (dtheta < MIN_DTHETA)
      {
        nbor ++;
        cx = (segs2[j].sx + segs2[j].ex) / 2;
        cy = (segs2[j].sy + segs2[j].ey) / 2;
        shift = (cref - aref * cx - bref * cy) / dref;
        if (shift < 0) shift = - shift;
        if (shift < MIN_SHIFT)
        {
          nbec ++;
          double vecacx = cx - segs1[i].sx;
          double vecacy = cy - segs1[i].sy;
          double vecbcx = cx - segs1[i].ex;
          double vecbcy = cy - segs1[i].ey;
          if (vrefx * vecacx + vrefy * vecacy >= 0.
              && vrefx * vecbcx + vrefy * vecbcy <= 0.)
          {
            nbin ++;
            if (opposite == 1)
            {
              px = segs2[j].ex;
              py = segs2[j].ey;
              qx = segs2[j].sx;
              qy = segs2[j].sy;
            }
            else
            {
              px = segs2[j].sx;
              py = segs2[j].sy;
              qx = segs2[j].ex;
              qy = segs2[j].ey;
            }
            double vecapx = px - segs1[i].sx;
            double vecapy = py - segs1[i].sy;
            if (vrefx * vecapx + vrefy * vecapy < 0.)
            {
              px = segs1[i].sx;
              py = segs1[i].sy;
            }
            double vecqbx = segs1[i].ex - qx;
            double vecqby = segs1[i].ey - qy;
            if (vrefx * vecqbx + vrefy * vecqby < 0.)
            {
              qx = segs1[i].ex;
              qy = segs1[i].ey;
            }
            lgth = sqrt ((qx - px) * (qx - px) + (qy - py) * (qy - py));
            if (lgth > matchlength)
            {
              matchlength = lgth;
              match[i] = j;
            }
            totl += lgth;
            totdtheta += dtheta * lgth;
          }
        }
      }
    }
  }
fprintf (stdout, "%d orientations communes, %d alignements, %d confusions\n",
         nbor, nbec, nbin);
  if ((pf = fopen ("pairs.txt", "w")) == NULL)
  {
    fprintf (stderr, "Pas d'ouverture de pairs.txt\n");
    return (0);
  }
  for (int i = 0; i < nsegs1; i++)
    if (match[i] != -1)
      fprintf (pf, "%s %d on %s %d\n", argv[1], i, argv[3], match[i]);
  fclose (pf);
}
