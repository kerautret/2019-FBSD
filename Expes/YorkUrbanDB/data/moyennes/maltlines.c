// Compilation : cc -o maltlines maltlines.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define ALT_LN "altln.txt"
#define FBSD_LN "fbsdln.txt"


int main (int argc, char *argv[])
{
  double nvals1[500];
  double lvals1[500];
  double lnvals1[500];
  double nvals2[500];
  double lvals2[500];
  double lnvals2[500];
  double *val = NULL;
  double nm1 = 0., lm1 = 0., lnm1 = 0.;
  double nm2 = 0., lm2 = 0., lnm2 = 0.;
  double nsig1 = 0., lsig1 = 0., lnsig1 = 0.;
  double nsig2 = 0., lsig2 = 0., lnsig2 = 0.;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (ALT_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ALT_LN);
    return (0);
  }
  val = nvals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals1 + nb / 3;
    else if (nb % 3 == 2) val = lnvals1 + nb / 3;
    else (val = nvals1 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ALT_LN);
    return (0);
  }

  if ((pf = fopen (FBSD_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FBSD_LN);
    return (0);
  }
  val = nvals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals2 + nb / 3;
    else if (nb % 3 == 2) val = lnvals2 + nb / 3;
    else (val = nvals2 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, FBSD_LN);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    nm1 += nvals1[i];
    lm1 += lvals1[i];
    lnm1 += lnvals1[i];
  }
  nm1 /= nb;
  lm1 /= nb;
  lnm1 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    nm2 += nvals2[i];
    lm2 += lvals2[i];
    lnm2 += lnvals2[i];
  }
  nm2 /= nb;
  lm2 /= nb;
  lnm2 /= nb;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    nsig1 += (nvals1[i] - nm1) * (nvals1[i] - nm1);
    lsig1 += (lvals1[i] - lm1) * (lvals1[i] - lm1);
    lnsig1 += (lnvals1[i] - lnm1) * (lnvals1[i] - lnm1);
  }
  nsig1 = sqrt (nsig1 / (nb -1));
  lsig1 = sqrt (lsig1 / (nb -1));
  lnsig1 = sqrt (lnsig1 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    nsig2 += (nvals2[i] - nm2) * (nvals2[i] - nm2);
    lsig2 += (lvals2[i] - lm2) * (lvals2[i] - lm2);
    lnsig2 += (lnvals2[i] - lnm2) * (lnvals2[i] - lnm2);
  }
  nsig2 = sqrt (nsig2 / (nb -1));
  lsig2 = sqrt (lsig2 / (nb -1));
  lnsig2 = sqrt (lnsig2 / (nb -1));

  fprintf (stdout, "Alt :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm1, nsig1);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm1, lsig1);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm1, lnsig1);
  fprintf (stdout, "FBSD :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm2, nsig2);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm2, lsig2);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm2, lnsig2);
  return (1);
}
