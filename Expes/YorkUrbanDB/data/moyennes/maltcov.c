// Compilation : cc -o maltcov maltcov.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define ALT_COVERT "altcov.txt"
#define FBSD_COVERT "fbsdcov.txt"


int main (int argc, char *argv[])
{
  double vals1[500];
  double vals2[500];
  double m1 = 0., m2 = 0.;
  double sig1 = 0., sig2 = 0.;
  double *val = NULL;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (ALT_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ALT_COVERT);
    return (0);
  }
  val = vals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val ++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ALT_COVERT);
    return (0);
  }

  if ((pf = fopen (FBSD_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FBSD_COVERT);
    return (0);
  }
  val = vals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val ++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, FBSD_COVERT);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    m1 += vals1[i];
    m2 += vals2[i];
  }
  m1 /= YORK_NB;
  m2 /= YORK_NB;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    sig1 += (vals1[i] - m1) * (vals1[i] - m1);
    sig2 += (vals2[i] - m2) * (vals2[i] - m2);
  }
  sig1 = sqrt (sig1 / (YORK_NB - 1));
  sig2 = sqrt (sig2 / (YORK_NB - 1));

  fprintf (stdout, "Ground truth cover :\n");
  fprintf (stdout, "Alt  : %lf pm %lf\n", m1, sig1);
  fprintf (stdout, "FBSD : %lf pm %lf\n", m2, sig2);
  return (1);
}
