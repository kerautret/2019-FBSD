// Compilation : cc -o mlines mlines.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define LSD_LN "lsdln.txt"
#define ED_LN "edln.txt"
#define CANNY_LN "cannyln.txt"
#define NAIVE_LN "naiveln.txt"
#define FBSD_LN "fbsdln.txt"


int main (int argc, char *argv[])
{
  double nvals1[500];
  double lvals1[500];
  double lnvals1[500];
  double nvals2[500];
  double lvals2[500];
  double lnvals2[500];
  double nvals3[500];
  double lvals3[500];
  double lnvals3[500];
  double nvals4[500];
  double lvals4[500];
  double lnvals4[500];
  double nvals5[500];
  double lvals5[500];
  double lnvals5[500];
  double *val = NULL;
  double nm1 = 0., lm1 = 0., lnm1 = 0.;
  double nm2 = 0., lm2 = 0., lnm2 = 0.;
  double nm3 = 0., lm3 = 0., lnm3 = 0.;
  double nm4 = 0., lm4 = 0., lnm4 = 0.;
  double nm5 = 0., lm5 = 0., lnm5 = 0.;
  double nsig1 = 0., lsig1 = 0., lnsig1 = 0.;
  double nsig2 = 0., lsig2 = 0., lnsig2 = 0.;
  double nsig3 = 0., lsig3 = 0., lnsig3 = 0.;
  double nsig4 = 0., lsig4 = 0., lnsig4 = 0.;
  double nsig5 = 0., lsig5 = 0., lnsig5 = 0.;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (LSD_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", LSD_LN);
    return (0);
  }
  val = nvals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals1 + nb / 3;
    else if (nb % 3 == 2) val = lnvals1 + nb / 3;
    else (val = nvals1 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, LSD_LN);
    return (0);
  }

  if ((pf = fopen (ED_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ED_LN);
    return (0);
  }
  val = nvals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals2 + nb / 3;
    else if (nb % 3 == 2) val = lnvals2 + nb / 3;
    else (val = nvals2 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ED_LN);
    return (0);
  }

  if ((pf = fopen (CANNY_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", CANNY_LN);
    return (0);
  }
  val = nvals3;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals3 + nb / 3;
    else if (nb % 3 == 2) val = lnvals3 + nb / 3;
    else (val = nvals3 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, CANNY_LN);
    return (0);
  }

  if ((pf = fopen (NAIVE_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", NAIVE_LN);
    return (0);
  }
  val = nvals4;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals4 + nb / 3;
    else if (nb % 3 == 2) val = lnvals4 + nb / 3;
    else (val = nvals4 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, NAIVE_LN);
    return (0);
  }

  if ((pf = fopen (FBSD_LN, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FBSD_LN);
    return (0);
  }
  val = nvals5;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 3 == 1) val = lvals5 + nb / 3;
    else if (nb % 3 == 2) val = lnvals5 + nb / 3;
    else (val = nvals5 + nb / 3);
  }
  fclose (pf);
  nb /= 3;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, FBSD_LN);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    nm1 += nvals1[i];
    lm1 += lvals1[i];
    lnm1 += lnvals1[i];
  }
  nm1 /= nb;
  lm1 /= nb;
  lnm1 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    nm2 += nvals2[i];
    lm2 += lvals2[i];
    lnm2 += lnvals2[i];
  }
  nm2 /= nb;
  lm2 /= nb;
  lnm2 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    nm3 += nvals3[i];
    lm3 += lvals3[i];
    lnm3 += lnvals3[i];
  }
  nm3 /= nb;
  lm3 /= nb;
  lnm3 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    nm4 += nvals4[i];
    lm4 += lvals4[i];
    lnm4 += lnvals4[i];
  }
  nm4 /= nb;
  lm4 /= nb;
  lnm4 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    nm5 += nvals5[i];
    lm5 += lvals5[i];
    lnm5 += lnvals5[i];
  }
  nm5 /= nb;
  lm5 /= nb;
  lnm5 /= nb;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    nsig1 += (nvals1[i] - nm1) * (nvals1[i] - nm1);
    lsig1 += (lvals1[i] - lm1) * (lvals1[i] - lm1);
    lnsig1 += (lnvals1[i] - lnm1) * (lnvals1[i] - lnm1);
  }
  nsig1 = sqrt (nsig1 / (nb -1));
  lsig1 = sqrt (lsig1 / (nb -1));
  lnsig1 = sqrt (lnsig1 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    nsig2 += (nvals2[i] - nm2) * (nvals2[i] - nm2);
    lsig2 += (lvals2[i] - lm2) * (lvals2[i] - lm2);
    lnsig2 += (lnvals2[i] - lnm2) * (lnvals2[i] - lnm2);
  }
  nsig2 = sqrt (nsig2 / (nb -1));
  lsig2 = sqrt (lsig2 / (nb -1));
  lnsig2 = sqrt (lnsig2 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    nsig3 += (nvals3[i] - nm3) * (nvals3[i] - nm3);
    lsig3 += (lvals3[i] - lm3) * (lvals3[i] - lm3);
    lnsig3 += (lnvals3[i] - lnm3) * (lnvals3[i] - lnm3);
  }
  nsig3 = sqrt (nsig3 / (nb -1));
  lsig3 = sqrt (lsig3 / (nb -1));
  lnsig3 = sqrt (lnsig3 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    nsig4 += (nvals4[i] - nm4) * (nvals4[i] - nm4);
    lsig4 += (lvals4[i] - lm4) * (lvals4[i] - lm4);
    lnsig4 += (lnvals4[i] - lnm4) * (lnvals4[i] - lnm4);
  }
  nsig4 = sqrt (nsig4 / (nb -1));
  lsig4 = sqrt (lsig4 / (nb -1));
  lnsig4 = sqrt (lnsig4 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    nsig5 += (nvals5[i] - nm5) * (nvals5[i] - nm5);
    lsig5 += (lvals5[i] - lm5) * (lvals5[i] - lm5);
    lnsig5 += (lnvals5[i] - lnm5) * (lnvals5[i] - lnm5);
  }
  nsig5 = sqrt (nsig5 / (nb -1));
  lsig5 = sqrt (lsig5 / (nb -1));
  lnsig5 = sqrt (lnsig5 / (nb -1));

  fprintf (stdout, "LSD :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm1, nsig1);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm1, lsig1);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm1, lnsig1);
  fprintf (stdout, "ED :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm2, nsig2);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm2, lsig2);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm2, lnsig2);
  fprintf (stdout, "Canny :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm3, nsig3);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm3, lsig3);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm3, lnsig3);
  fprintf (stdout, "NAIVE :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm4, nsig4);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm4, lsig4);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm4, lnsig4);
  fprintf (stdout, "FBSD  :\n");
  fprintf (stdout, "  N   = %lf (%lf)\n", nm5, nsig5);
  fprintf (stdout, "  L   = %lf (%lf)\n", lm5, lsig5);
  fprintf (stdout, "  L/N = %lf (%lf)\n", lnm5, lnsig5);
  return (1);
}
