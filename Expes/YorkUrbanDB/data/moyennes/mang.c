// Compilation : cc -o mang mang.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define LSD_ANG "lsdang.txt"
#define ED_ANG "edang.txt"
#define CANNY_ANG "cannyang.txt"
#define NAIVE_ANG "naiveang.txt"


int main (int argc, char *argv[])
{
  double vals1[500];
  double vals2[500];
  double vals3[500];
  double vals4[500];
  double vang1[500];
  double vang2[500];
  double vang3[500];
  double vang4[500];
  double m1 = 0., m2 = 0., m3 = 0., m4 = 0.;
  double a1 = 0., a2 = 0., a3 = 0., a4 = 0.;
  double sig1 = 0., sig2 = 0., sig3 = 0., sig4 = 0.;
  double asig1 = 0., asig2 = 0., asig3 = 0., asig4 = 0.;
  double totl1 = 0., totl2 = 0., totl3 = 0., totl4 = 0.;
  double totang1 = 0., totang2 = 0., totang3 = 0., totang4 = 0.;
  double *val = NULL;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (LSD_ANG, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", LSD_ANG);
    return (0);
  }
  val = vals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 0)
    {
      val = vals1 + nb / 2;
    }
    else val = vang1 + nb / 2;
  }
  fclose (pf);
  if (nb != 2 * YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, LSD_ANG);
    return (0);
  }

  if ((pf = fopen (ED_ANG, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ED_ANG);
    return (0);
  }
  val = vals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 0)
    {
      val = vals2 + nb / 2;
    }
    else val = vang2 + nb / 2;
  }
  fclose (pf);
  if (nb != YORK_NB * 2)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ED_ANG);
    return (0);
  }

  if ((pf = fopen (CANNY_ANG, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", CANNY_ANG);
    return (0);
  }
  val = vals3;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 0)
    {
      val = vals3 + nb / 2;
    }
    else val = vang3 + nb / 2;
  }
  fclose (pf);
  if (nb != YORK_NB * 2)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, CANNY_ANG);
    return (0);
  }

  if ((pf = fopen (NAIVE_ANG, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", NAIVE_ANG);
    return (0);
  }
  val = vals4;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 0)
    {
      val = vals4 + nb / 2;
    }
    else val = vang4 + nb / 2;
  }
  fclose (pf);
  if (nb != YORK_NB * 2)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, NAIVE_ANG);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    m1 += vals1[i];
    m2 += vals2[i];
    m3 += vals3[i];
    m4 += vals4[i];
    a1 += vang1[i];
    a2 += vang2[i];
    a3 += vang3[i];
    a4 += vang4[i];
    totl1 += vals1[i];
    totang1 += vang1[i] * vals1[i];
    totl2 += vals2[i];
    totang2 += vang2[i] * vals2[i];
    totl3 += vals3[i];
    totang3 += vang3[i] * vals3[i];
    totl4 += vals4[i];
    totang4 += vang4[i] * vals4[i];
  }
  m1 /= YORK_NB;
  m2 /= YORK_NB;
  m3 /= YORK_NB;
  m4 /= YORK_NB;
  a1 /= YORK_NB;
  a2 /= YORK_NB;
  a3 /= YORK_NB;
  a4 /= YORK_NB;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    sig1 += (vals1[i] - m1) * (vals1[i] - m1);
    sig2 += (vals2[i] - m2) * (vals2[i] - m2);
    sig3 += (vals3[i] - m3) * (vals3[i] - m3);
    sig4 += (vals4[i] - m4) * (vals4[i] - m4);
    asig1 += (vang1[i] - a1) * (vang1[i] - a1);
    asig2 += (vang2[i] - a2) * (vang2[i] - a2);
    asig3 += (vang3[i] - a3) * (vang3[i] - a3);
    asig4 += (vang4[i] - a4) * (vang4[i] - a4);
  }
  sig1 = sqrt (sig1 / (YORK_NB - 1));
  sig2 = sqrt (sig2 / (YORK_NB - 1));
  sig3 = sqrt (sig3 / (YORK_NB - 1));
  sig4 = sqrt (sig4 / (YORK_NB - 1));
  asig1 = sqrt (asig1 / (YORK_NB - 1));
  asig2 = sqrt (asig2 / (YORK_NB - 1));
  asig3 = sqrt (asig3 / (YORK_NB - 1));
  asig4 = sqrt (asig4 / (YORK_NB - 1));

  fprintf (stdout, "Match length :\n");
  fprintf (stdout, "LSD   : %lf pm %lf\n", m1, sig1);
  fprintf (stdout, "ED    : %lf pm %lf\n", m2, sig2);
  fprintf (stdout, "Canny : %lf pm %lf\n", m3, sig3);
  fprintf (stdout, "Naive : %lf pm %lf\n", m4, sig4);
  fprintf (stdout, "Mean angular deviation :\n");
  fprintf (stdout, "LSD   : %lf pm %lf\n", a1, asig1);
  fprintf (stdout, "ED    : %lf pm %lf\n", a2, asig2);
  fprintf (stdout, "Canny : %lf pm %lf\n", a3, asig3);
  fprintf (stdout, "Naive : %lf pm %lf\n", a4, asig4);
  fprintf (stdout, "Global angular deviation :\n");
  fprintf (stdout, "LSD   : %lf\n", totang1 / totl1);
  fprintf (stdout, "ED    : %lf\n", totang2 / totl2);
  fprintf (stdout, "Canny : %lf\n", totang3 / totl3);
  fprintf (stdout, "Naive : %lf\n", totang4 / totl4);
  return (1);
}
