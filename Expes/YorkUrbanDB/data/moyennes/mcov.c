// Compilation : cc -o mcov mcov.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define LSD_COVERT "lsdcov.txt"
#define ED_COVERT "edcov.txt"
#define CANNY_COVERT "cannycov.txt"
#define NAIVE_COVERT "naivecov.txt"
#define FBSD_COVERT "fbsdcov.txt"


int main (int argc, char *argv[])
{
  double vals1[500];
  double vals2[500];
  double vals3[500];
  double vals4[500];
  double vals5[500];
  double m1 = 0., m2 = 0., m3 = 0., m4 = 0., m5 = 0.;
  double a1 = 0., a2 = 0., a3 = 0., a4 = 0.;
  double sig1 = 0., sig2 = 0., sig3 = 0., sig4 = 0., sig5 = 0.;
  double asig1 = 0., asig2 = 0., asig3 = 0., asig4 = 0.;
  double *val = NULL;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (LSD_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", LSD_COVERT);
    return (0);
  }
  val = vals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, LSD_COVERT);
    return (0);
  }

  if ((pf = fopen (ED_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ED_COVERT);
    return (0);
  }
  val = vals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ED_COVERT);
    return (0);
  }

  if ((pf = fopen (CANNY_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", CANNY_COVERT);
    return (0);
  }
  val = vals3;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val ++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, CANNY_COVERT);
    return (0);
  }

  if ((pf = fopen (NAIVE_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", NAIVE_COVERT);
    return (0);
  }
  val = vals4;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val ++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, NAIVE_COVERT);
    return (0);
  }

  if ((pf = fopen (FBSD_COVERT, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FBSD_COVERT);
    return (0);
  }
  val = vals5;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    val ++;
  }
  fclose (pf);
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, FBSD_COVERT);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    m1 += vals1[i];
    m2 += vals2[i];
    m3 += vals3[i];
    m4 += vals4[i];
    m5 += vals5[i];
  }
  m1 /= YORK_NB;
  m2 /= YORK_NB;
  m3 /= YORK_NB;
  m4 /= YORK_NB;
  m5 /= YORK_NB;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    sig1 += (vals1[i] - m1) * (vals1[i] - m1);
    sig2 += (vals2[i] - m2) * (vals2[i] - m2);
    sig3 += (vals3[i] - m3) * (vals3[i] - m3);
    sig4 += (vals4[i] - m4) * (vals4[i] - m4);
    sig5 += (vals5[i] - m5) * (vals5[i] - m5);
  }
  sig1 = sqrt (sig1 / (YORK_NB - 1));
  sig2 = sqrt (sig2 / (YORK_NB - 1));
  sig3 = sqrt (sig3 / (YORK_NB - 1));
  sig4 = sqrt (sig4 / (YORK_NB - 1));
  sig5 = sqrt (sig5 / (YORK_NB - 1));

  fprintf (stdout, "Ground truth cover :\n");
  fprintf (stdout, "LSD   : %lf pm %lf\n", m1, sig1);
  fprintf (stdout, "ED    : %lf pm %lf\n", m2, sig2);
  fprintf (stdout, "Canny : %lf pm %lf\n", m3, sig3);
  fprintf (stdout, "Naive : %lf pm %lf\n", m4, sig4);
  fprintf (stdout, "FBSD  : %lf pm %lf\n", m5, sig5);
  return (1);
}
