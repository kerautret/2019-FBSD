// Compilation : cc -o maltth maltth.c -lm

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

#define YORK_NB 102
#define ALT_THICK "altth.txt"
#define FBSD_THICK "fbsdth.txt"


int main (int argc, char *argv[])
{
  double ltvals1[500];
  double tvals1[500];
  double ltvals2[500];
  double tvals2[500];
  double *val = NULL;
  double ltm1 = 0., tm1 = 0.;
  double ltm2 = 0., tm2 = 0.;
  double ltsig1 = 0., tsig1 = 0.;
  double ltsig2 = 0., tsig2 = 0.;
  int nb = 0;
  FILE *pf = NULL;

  if ((pf = fopen (ALT_THICK, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", ALT_THICK);
    return (0);
  }
  val = ltvals1;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 1) val = tvals1 + nb / 2;
    else (val = ltvals1 + nb / 2);
  }
  fclose (pf);
  nb /= 2;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, ALT_THICK);
    return (0);
  }

  if ((pf = fopen (FBSD_THICK, "r")) == NULL)
  {
    fprintf (stderr, "Pas de fichier %s\n", FBSD_THICK);
    return (0);
  }
  val = ltvals2;
  nb = 0;
  while (fscanf (pf, "%lf", val) != -1)
  {
    nb ++;
    if (nb % 2 == 1) val = tvals2 + nb / 2;
    else (val = ltvals2 + nb / 2);
  }
  fclose (pf);
  nb /= 2;
  if (nb != YORK_NB)
  {
    fprintf (stderr, "Seulement %d donnees dans %s\n", nb, FBSD_THICK);
    return (0);
  }

  // Computes means
  for (int i = 0; i < nb; i ++)
  {
    ltm1 += ltvals1[i];
    tm1 += tvals1[i];
  }
  ltm1 /= nb;
  tm1 /= nb;
  for (int i = 0; i < nb; i ++)
  {
    ltm2 += ltvals2[i];
    tm2 += tvals2[i];
  }
  ltm2 /= nb;
  tm2 /= nb;

  // Computes standard deviations
  for (int i = 0; i < nb; i ++)
  {
    ltsig1 += (ltvals1[i] - ltm1) * (ltvals1[i] - ltm1);
    tsig1 += (tvals1[i] - tm1) * (tvals1[i] - tm1);
  }
  ltsig1 = sqrt (ltsig1 / (nb -1));
  tsig1 = sqrt (tsig1 / (nb -1));
  for (int i = 0; i < nb; i ++)
  {
    ltsig2 += (ltvals2[i] - ltm2) * (ltvals2[i] - ltm2);
    tsig2 += (tvals2[i] - tm2) * (tvals2[i] - tm2);
  }
  ltsig2 = sqrt (ltsig2 / (nb -1));
  tsig2 = sqrt (tsig2 / (nb -1));

  fprintf (stdout, "Alt :\n");
  fprintf (stdout, "  Epaisseur Lineaire  = %lf (%lf)\n", ltm1, ltsig1);
  fprintf (stdout, "  Epaisseur Moyenne   = %lf (%lf)\n", tm1, tsig1);
  fprintf (stdout, "FBSD :\n");
  fprintf (stdout, "  Epaisseur Lineaire  = %lf (%lf)\n", ltm2, ltsig2);
  fprintf (stdout, "  Epaisseur Moyenne   = %lf (%lf)\n", tm2, tsig2);
  return (1);
}
