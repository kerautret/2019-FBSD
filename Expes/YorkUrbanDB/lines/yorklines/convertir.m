path = '.' ; % répertoire courant
D = dir(path) ; % récupère un tableau de structure
D = D(cell2mat({D(:).isdir})) ; % filter pour ne garder que les noms de répetoire
Liste = {D(:).name} ; % transformer en un tableau de cellules texte

for dirname = Liste
    %disp(dirname)
    if(length(char(dirname))>5)
        %disp(dirname)
        filename = strcat(dirname,"/",dirname,"LinesAndVP.mat");
        %disp(filename)
        A = matfile(filename);
        B = A.lines;
        filename_save = strcat(dirname,"Lines.txt");
        %save 'test.txt' B -ascii
        save (filename_save, 'B', '-ascii');
    end
end    