#include "EDLib.h"
#include <iostream>
#include <fstream>
#include <ctime>

using namespace cv;
using namespace std;

int main(int argc, char ** argv)
{
    if(argc<3)
    {
        cout<<"EDlines in out"<<endl;
        return -1;
    }
	//***************************** ED Edge Segment Detection *****************************
	//Detection of edge segments from an input image	
	Mat testImg = imread(argv[1], 0);

	//Call ED constructor
	ED testED = ED(testImg, SOBEL_OPERATOR, 36, 8, 1, 10, 1.0, true); // apply ED algorithm
	
	//Show resulting edge image
	Mat edgeImg = testED.getEdgeImage();
	
	//Get edges in segment form (getSortedSegments() gives segments sorted w.r.t. legnths) 
	//std::vector< std::vector<Point> > segments = testED.getSegments();
    
	//***************************** EDLINES Line Segment Detection *****************************
	//Detection of line segments from the same image
	EDLines testEDLines = EDLines(testImg);
	//Mat lineImg = testEDLines.getLineImage();	//draws on an empty image
	//imshow("Line Image 1 - PRESS ANY KEY TO CONTINUE", lineImg);

	//Detection of lines segments from edge segments instead of input image
	//Therefore, redundant detection of edge segmens can be avoided
	testEDLines = EDLines(testED);
	//lineImg = testEDLines.drawOnImage();	//draws on the input image
	//imshow("Line Image 2  - PRESS ANY KEY TO CONTINUE", lineImg);

	//Acquiring line information, i.e. start & end points
	vector<LS> lines = testEDLines.getLines();
	//int noLines = testEDLines.getLinesNo();
	//std::cout << "Number of line segments: " << noLines << std::endl;

    /* Phuc */
    ofstream myfile;
    myfile.open (argv[2]);
    for(size_t it=0; it<lines.size(); it++)
    {
        LS l = lines.at(it);
        //cout<<"Lines "<<l.start<<" to "<<l.end<<endl;
        myfile << l.start.x<<" "<<l.start.y<<" "<<l.end.x<<" "<<l.end.y<<endl;
        
    }
    myfile.close();
    /* Phuc */
	//waitKey();
    return 0;
}



