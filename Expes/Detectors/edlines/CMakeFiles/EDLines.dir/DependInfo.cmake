# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/even/Expes/Detectors/edlines/ED.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/ED.cpp.o"
  "/home/even/Expes/Detectors/edlines/EDCircles.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/EDCircles.cpp.o"
  "/home/even/Expes/Detectors/edlines/EDColor.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/EDColor.cpp.o"
  "/home/even/Expes/Detectors/edlines/EDLines.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/EDLines.cpp.o"
  "/home/even/Expes/Detectors/edlines/EDPF.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/EDPF.cpp.o"
  "/home/even/Expes/Detectors/edlines/NFA.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/NFA.cpp.o"
  "/home/even/Expes/Detectors/edlines/test.cpp" "/home/even/Expes/Detectors/edlines/CMakeFiles/EDLines.dir/test.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/include"
  "/usr/local/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
